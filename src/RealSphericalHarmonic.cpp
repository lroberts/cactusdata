#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <set>
#include <algorithm>

#include "boost/math/special_functions/spherical_harmonic.hpp"

#include "RealSphericalHarmonic.hpp" 
#include "jacobi_rule.hpp" 

double RealSphericalHarmonic::Integrate(const std::vector<std::vector<double>>& val) {
  double xx = 0.0;
  for (int i=0; i<val.size(); ++i) {
    for (int j=0; j<val[i].size(); ++j) {
      xx += val[i][j]*Ylm[i][j];
    }
  } 
  return xx; 
} 

RealSphericalHarmonic::RealSphericalHarmonic(int L, int m, std::vector<double> mus, 
    std::vector<double> phis) : mL(L), mMus(mus), mPhis(phis) { 
  // Initialize quadrature 
  cgqf(NGLPTS, 1, 0.0, 0.0, -1.0, 1.0, mXt.data(), mWt.data()); 
   
  double total  = 0.0;
  double total2 = 0.0;
  double norm = 0.0;
  double musum = 0.0; 
  for (int i=0; i<mMus.size(); ++i) {
    std::vector<double> arr;
    double dmu, mu_min, mu_max;
    if (i>0 && i<mMus.size()-1) {
      mu_min = 0.5 * (mMus[i-1] + mMus[i]);
      mu_max = 0.5 * (mMus[i] + mMus[i+1]);
    } else if (i>0) { 
      mu_min = 0.5 * (mMus[i] + mMus[i-1]);
      mu_max = 1.0;
    } else { 
      mu_min = -1.0; 
      mu_max = 0.5 * (mMus[i] + mMus[i+1]);
      //dmu = (mMus[i+1]+mMus[i])/2.0 + 1.0; 
    }
    dmu = mu_max - mu_min; 
    musum += dmu;
    double theta = acos(mMus[i]); 
    for (int j=0; j<mPhis.size(); ++j) { 
      double val;
      double dphi, phi_min, phi_max;
      if (j>0 && j<mPhis.size()-1) {
        phi_min = 0.5 * (mPhis[j-1] + mPhis[j]);
        phi_max = 0.5 * (mPhis[j] + mPhis[j+1]);
        //dphi = (mPhis[j+1] - mPhis[j-1])/2.0; 
      } else if (j>0) { 
        phi_min = 0.5 * (mPhis[j-1] + mPhis[j]);
        phi_max = 2.0*3.14159265359;
        //dphi = 2.0*3.14159265359 - (mPhis[j] + mPhis[j-1])/2.0; 
      } else { 
        phi_min = 0.0;
        phi_max = 0.5 * (mPhis[j] + mPhis[j+1]);
        //dphi = (mPhis[j]+mPhis[j+1])/2.0; 
      }
      dphi = phi_max - phi_min; 
       
      //if (m<0) { 
      //  val = boost::math::spherical_harmonic_i<double, double>(L,-m, theta, mPhis[j]); 
      //} else { 
      //  val = boost::math::spherical_harmonic_r<double, double>(L, m, theta, mPhis[j]); 
      //}
      //if (m!=0) val *= sqrt(2.0); 
      //total += val*dmu*dphi;
      //total2 += val*val*dmu*dphi;
      //norm += dmu*dphi;
      //arr.push_back(val*dmu*dphi); 
      
      val = IntegratePatch(L, m, mu_min, mu_max, phi_min, phi_max);
      arr.push_back(val);
      total += val; 
      
    }
    Ylm.push_back(arr);
  }
}

double RealSphericalHarmonic::IntegratePatch(int L, int m, double mu_min, 
    double mu_max, double phi_min, double phi_max) const { 
  double mu_c = 0.5 * (mu_min + mu_max);
  double dmu = mu_max - mu_min;
  double integral = 0.0; 
  for (int i=0; i<NGLPTS; ++i) { 
    double mu = dmu * (mXt[i] + 1.0) * 0.5 + mu_min; 
    integral += mWt[i] * IntegratePhi(L, m, mu, phi_min, phi_max);
  }
  integral *= dmu * 0.5; 
  return integral; 
}

double RealSphericalHarmonic::IntegratePhi(int L, int m, double mu, 
    double phi_min, double phi_max) const {
  double phi_c = 0.5 * (phi_max + phi_min);
  double theta = acos(mu);
  if (m<0) {
    m = abs(m);
    double fac = sqrt(2.0) * (cos((double) m * phi_min) - cos((double) m * phi_max)) / 
        ((double) m * sin((double) m * phi_c));
    return fac *  
        boost::math::spherical_harmonic_i<double, double>(L,m, theta, phi_c); 
  } else if (m>0) {
    double fac = sqrt(2.0) * (sin((double) m * phi_max) - sin((double) m * phi_min)) / 
        ((double) m * cos((double) m * phi_c));
    return fac *  
        boost::math::spherical_harmonic_r<double, double>(L, m, theta, phi_c); 
  } else { 
    return (phi_max - phi_min) *  
        boost::math::spherical_harmonic_r<double, double>(L, m, theta, phi_c); 
  }
} 

