#include <algorithm>
#include <stdexcept>
#include <string>
#include <cstring>
#include <iostream>

#include "epsThermal/epsThermal.hpp"
//#include "epsThermal.hpp"

namespace {
  extern "C" {
    void  thermal_energy_(double* rho, 
			  double* temp, 
			  double* ye,
			  double* abar,
			  double* etherm);
    void readtable_electron_table_(char* eos_file, int* nchar);
  }
  // Taken from Stack Overflow "Passing char arrays from c to fortran"                                                                                   
  void ConvertToFortran(const char* cstring, char* fstring,
			std::size_t fstring_len) {
    std::size_t inlen = std::strlen(cstring);
    std::size_t cpylen = std::min(inlen, fstring_len);
    if (inlen > fstring_len) throw std::length_error("Bad string size.");
    std::copy(cstring, cstring + cpylen, fstring);
    std::fill(fstring + cpylen, fstring + fstring_len, ' ');
  }


}

double epsThermal::GetThermalEnergy(double rho,
				  double temp,
				  double ye,
				  double abar) {

  double etherm = 0.0;

  thermal_energy_(&rho,&temp,&ye,&abar,&etherm);

  return etherm;
}

void epsThermal::ReadTable(std::string eos_file) {
  char cs[eos_file.size()+1];
  ConvertToFortran(eos_file.c_str(), cs, eos_file.size() + 1);
  int nchar = eos_file.size()+1;
  readtable_electron_table_(cs, &nchar);
}
