#ifndef EPSTHERMAL_HPP_
#define EPSTHERMAL_HPP_


namespace epsThermal {
  double GetThermalEnergy(double rho, 
			  double temp, 
			  double ye,
			  double Abar);
  void ReadTable(std::string eos_file);
}
#endif // EPSTHERMAL_HPP_        
