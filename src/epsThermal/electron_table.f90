module electron_table

  implicit none
  integer :: nrhoye
  integer :: ntemp
  real*8, allocatable :: logrhoye(:)
  real*8, allocatable :: logtemp(:)
  real*8, allocatable :: ep_eps(:,:)
  real*8 :: dlry,dlryi
  real*8 :: dltemp,dltempi

end module electron_table

subroutine get_eps_electron_table(rho,ye,temp,eps)

  use electron_table
  implicit none

  real*8, intent(in)  :: rho, ye, temp
  real*8, intent(out) :: eps

  real*8 :: xry, xtemp
  integer :: iry, it

  real*8 :: eps1, eps2

  eps = 0.0

  xry = log10(rho*ye)
  xtemp = log10(temp)

  ! get nearest integers
  iry = 1 + INT((xry - logrhoye(1)) * dlryi)
  it = 1 + INT((xtemp - logtemp(1)) * dltempi)

  if (it .ge. ntemp) then
     stop "temp > temp max"
  else if(it .lt. 1) then
     stop "temp < temp in"
  endif

  if (iry .ge. nrhoye) then
     stop "rho*ye > rho*ye max"
  else if (iry .lt. 1) then
     stop "rho*ye < rho*ye min"
  endif

!  write(6,*) dlry,dltemp
!  write(6,*) nrhoye,ntemp
!  write(6,*) iry,it
!  write(6,"(1P10E15.6)") logrhoye(iry),logtemp(it)
!  write(6,"(1P10E15.6)") xry,xtemp
!  write(6,"(1P10E15.6)") logrhoye(iry+1),logtemp(it+1)

  
  ! first interpolate in density * ye
  eps1 = (ep_eps(iry+1,it) - ep_eps(iry,it)) * dlryi * (xry - logrhoye(iry)) &
       + ep_eps(iry,it)

  eps2 = (ep_eps(iry+1,it+1) - ep_eps(iry,it+1)) * dlryi * (xry - logrhoye(iry)) &
       + ep_eps(iry,it+1)

  ! now interpolate in temperature
  eps = (eps2-eps1) * dltempi * (xtemp-logtemp(it)) + eps1
  
  ! go to ergs per gram, table is ergs per cm^3
  eps = eps / rho 

end subroutine get_eps_electron_table

subroutine readtable_electron_table(h5_filename,nchar)

  use electron_table
  use hdf5
  implicit none
  
  integer(HID_T) file_id,dset_id,dspace_id
  integer(HSIZE_T) dims1(1), dims2(2)
  integer error,rank,accerr
  integer :: nchar
  character(LEN=nchar) :: h5_filename
  integer :: i,j

  call h5open_f(error)
  call h5fopen_f (trim(adjustl(h5_filename)), H5F_ACC_RDONLY_F, file_id, error)
  
  dims1(1)=1
  call h5dopen_f(file_id, "pointsrhoye", dset_id, error)
  call h5dread_f(dset_id, H5T_NATIVE_INTEGER, nrhoye, dims1, error)
  call h5dclose_f(dset_id,error)
  
  dims1(1)=1
  call h5dopen_f(file_id, "pointstemp", dset_id, error)
  call h5dread_f(dset_id, H5T_NATIVE_INTEGER, ntemp, dims1, error)
  call h5dclose_f(dset_id,error)
  
  allocate(ep_eps(nrhoye,ntemp))
  dims2(1)=nrhoye
  dims2(2)=ntemp
  call h5dopen_f(file_id, "e", dset_id, error)
  call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, ep_eps(:,:), dims2, error)
  call h5dclose_f(dset_id,error)
  
  allocate(logrhoye(nrhoye))
  dims1(1)=nrhoye
  call h5dopen_f(file_id, "logrhoye", dset_id, error)
  call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, logrhoye, dims1, error)
  call h5dclose_f(dset_id,error)
  
  allocate(logtemp(ntemp))
  dims1(1)=ntemp
  call h5dopen_f(file_id, "logtemp", dset_id, error)
  call h5dread_f(dset_id, H5T_NATIVE_DOUBLE, logtemp, dims1, error)
  call h5dclose_f(dset_id,error)

  dltemp = logtemp(2) - logtemp(1)
  dltempi = 1.0d0/dltemp

  dlry = logrhoye(2) - logrhoye(1)
  dlryi = 1.0d0/dlry


end subroutine readtable_electron_table


