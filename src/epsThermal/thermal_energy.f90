program e_thermal

  use thermal_energy_mod, only : thermal_energy

  implicit none

  double precision :: rho, temp, ye, abar
  double precision :: e_th


  rho = 3.804074E+09
  temp = 1.445440E+00
  ye = 5.000000E-01
  abar = 1.32d0
  
  call thermal_energy(rho,temp,ye,abar,e_th)

  write (*,"(A10,1ES20.12)") 'rho  = ', rho
  write (*,"(A10,1ES20.12)") 'temp = ', temp
  write (*,"(A10,1ES20.12)") 'ye   = ', ye
  write (*,"(A10,1ES20.12)") 'Abar = ', abar
  write (*,"(A10,1ES20.12)") 'E_th = ', e_th

end program e_thermal
