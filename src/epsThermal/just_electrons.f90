!module thermal_energy_mod

!  use wrap, only : wrap_timmes
!  use physical_constants_mod, &
!       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
!       m_n_cgs, neutron_mass_MeV
  
!  implicit none

!end module thermal_energy_mod

subroutine just_electrons(rhoye,temp,eps)

  use wrap, only : wrap_timmes
  use physical_constants_mod, &
       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
       m_n_cgs, neutron_mass_MeV

  implicit none

  double precision, intent(in) :: rhoye,temp
  double precision, intent(out) :: eps
  
  double precision :: temp_cgs, amu_cgs
  double precision :: e_timmes, e_baryons, e_photons
  double precision :: A, Z
  double precision :: etot, ptot, stot
  double precision :: dedT, dpdT, dsdT
  double precision :: dedr, dpdr, dsdr
  double precision :: dedy, dpdy, dsdy
  double precision :: gamma, etaele, sound
  
  double precision, parameter :: amu = 931.49432d0
  integer :: do_rad

  do_rad = 0

!   convert temp to cgs
  temp_cgs = temp*temp_mev_to_kelvin

!   convert amu to cgs
  amu_cgs = m_n_cgs*amu/neutron_mass_MeV

!   timmes eos already include photons
!    e_photons = (4.d0*avo)*Temp**4./rho

  A = 1.0d0
  Z = 1.0d0

  call wrap_timmes(A,Z,rhoye,temp_cgs,etot,ptot,stot, &
       dedT,dpdT,dsdT,dedr,dpdr,dsdr,dedy,dpdy,dsdy,&
       gamma,etaele,sound,do_rad)
  
  eps = etot 

end subroutine just_electrons

subroutine just_electrons2(rho,ye,temp,eps)

  use wrap, only : wrap_timmes
  use physical_constants_mod, &
       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
       m_n_cgs, neutron_mass_MeV

  implicit none

  double precision, intent(in) :: rho,ye,temp
  double precision, intent(out) :: eps
  
  double precision :: temp_cgs, amu_cgs
  double precision :: e_timmes, e_baryons, e_photons
  double precision :: A, Z
  double precision :: etot, ptot, stot
  double precision :: dedT, dpdT, dsdT
  double precision :: dedr, dpdr, dsdr
  double precision :: dedy, dpdy, dsdy
  double precision :: gamma, etaele, sound
  
  double precision, parameter :: amu = 931.49432d0
  integer :: do_rad

  do_rad = 0

!   convert temp to cgs
  temp_cgs = temp*temp_mev_to_kelvin

!   convert amu to cgs
  amu_cgs = m_n_cgs*amu/neutron_mass_MeV

!   timmes eos already include photons
!    e_photons = (4.d0*avo)*Temp**4./rho

  A = 1.0d0
  Z = A*ye

  call wrap_timmes(A,Z,rho,temp_cgs,etot,ptot,stot, &
       dedT,dpdT,dsdT,dedr,dpdr,dsdr,dedy,dpdy,dsdy,&
       gamma,etaele,sound,do_rad)
  
  eps = etot

end subroutine just_electrons2


