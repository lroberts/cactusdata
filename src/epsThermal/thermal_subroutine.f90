!module thermal_energy_mod

!  use wrap, only : wrap_timmes
!  use physical_constants_mod, &
!       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
!       m_n_cgs, neutron_mass_MeV
  
!  implicit none

!end module thermal_energy_mod

subroutine thermal_energy(rho,temp,ye,abar,e_thermal)

  use wrap, only : wrap_timmes
  use physical_constants_mod, &
       only : kb_erg, avo, clite, temp_mev_to_kelvin, &
       m_n_cgs, neutron_mass_MeV
  use electron_table

  implicit none

  real*8, intent(in) :: rho, temp, ye, abar
  real*8, intent(out) :: e_thermal
  
  real*8 :: temp_cgs, amu_cgs
  real*8 :: e_timmes, e_baryons, e_photons
  real*8 :: A, Z, eps
  real*8 :: etot, ptot, stot
  real*8 :: dedT, dpdT, dsdT
  real*8 :: dedr, dpdr, dsdr
  real*8 :: dedy, dpdy, dsdy
  real*8 :: gamma, etaele, sound
  
  real*8, parameter :: amu = 931.49432d0
  real*8, parameter :: clight = 2.99792458d10
  real*8, parameter :: ssol = 5.67051d-5
  real*8, parameter :: asol = 4.0d0 * ssol / clight
  integer :: do_rad
  logical :: use_electron_table 

  use_electron_table = .true.
  do_rad = 1
 

!   convert temp to cgs
  temp_cgs = temp*temp_mev_to_kelvin

!   convert amu to cgs
  amu_cgs = m_n_cgs*amu/neutron_mass_MeV

!   timmes eos already include photons
!    e_photons = (4.d0*avo)*Temp**4./rho

  e_baryons = 3.d0/2.d0*kb_erg*Temp_cgs/(Abar*amu_cgs)

  if(use_electron_table) then
     
     call get_eps_electron_table(rho,ye,temp,eps)
     e_timmes = eps
     
     ! now add photons
     e_photons = asol * temp_cgs**4.0d0 / rho  

     e_thermal = e_baryons + e_timmes + e_photons 

!     write(6,"(1P10E15.6)") e_photons, e_timmes

  else 

     A = abar
     Z = ye*abar
     
     call wrap_timmes(A,Z,rho,temp_cgs,etot,ptot,stot, &
          dedT,dpdT,dsdT,dedr,dpdr,dsdr,dedy,dpdy,dsdy,&
          gamma,etaele,sound,do_rad)
     
     e_timmes = etot
  

!     write(6,"(1P10E15.6)") e_timmes
     e_thermal = e_baryons + e_timmes !+ e_photons
  endif

!  write (*,"(3ES20.12)") e_baryons, e_timmes !, e_photons

end subroutine thermal_energy


