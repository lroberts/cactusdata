/// \file EOSDriver.cpp
/// \author lroberts
/// \since Mar 17, 2016
///
/// \brief
///
///
#include <algorithm> 
#include <stdexcept>
#include <string>
#include <cstring> 
#include <iostream> 
 
#include "EOSDriver/EOSDriver.hpp"

namespace {
  extern "C" {
    void readtable_(char* eos_file, int* nchar);
    void nuc_eos_full_(double* rho, double* temp, double* ye, double* enr,
        double* prs, double* ent, double* cs2, double* dedt, double* dpderho,
        double* dpdrhoe, double* xa, double* xh, double* xn, double* xp,
        double* xabar, double* xzbar, double* mu_e, double* mu_n, double* mu_p,
        double* muhat, int* keytemp, int* keyerr, double* rfeps);
  }
  
  // Taken from Stack Overflow "Passing char arrays from c to fortran"  
  void ConvertToFortran(const char* cstring, char* fstring, 
      std::size_t fstring_len) {
    std::size_t inlen = std::strlen(cstring); 
    std::size_t cpylen = std::min(inlen, fstring_len);
    std::cout << inlen << " " << cpylen << std::endl;
    if (inlen > fstring_len) throw std::length_error("Bad string size.");
    std::copy(cstring, cstring + cpylen, fstring); 
    std::fill(fstring + cpylen, fstring + fstring_len, ' '); 
  }   

}

void EOSDriver::ReadTable(std::string eos_file) {
  std::cout << eos_file << std::endl;
  char cs[eos_file.size()+1];  
  ConvertToFortran(eos_file.c_str(), cs, eos_file.size() + 1);
  int nchar = eos_file.size()+1;
  readtable_(cs, &nchar); 
} 

double EOSDriver::GetPressureFromEntropy(double rho, double ye, double s) {
  double enr, prs, ent, cs2, dedt, dpderho, dpdrhoe, xa, xh, xn, xp, xabar,
      xzbar, mu_e, mu_n, mu_p, muhat;
  double rfeps = 1.e-8; 
  int keyerr; 
  int keytemp = 2;
  double temp = 1.0; 
  nuc_eos_full_(&rho, &temp, &ye, &enr, &prs, &s, &cs2, &dedt, &dpderho, &dpdrhoe, 
      &xa, &xh, &xn, &xp, &xabar, &xzbar, &mu_e, &mu_n, &mu_p, &muhat, &keytemp, 
      &keyerr, &rfeps); 
  
  if (keyerr != 0) throw std::runtime_error("Bad EOS call.");
  
  return prs;  
}

double EOSDriver::GetPressureFromTemperature(double rho, double ye, double t) {
  double enr, prs, ent, cs2, dedt, dpderho, dpdrhoe, xa, xh, xn, xp, xabar,
      xzbar, mu_e, mu_n, mu_p, muhat;
  double rfeps = 1.e-8; 
  int keyerr; 
  int keytemp = 1;
  nuc_eos_full_(&rho, &t, &ye, &enr, &prs, &ent, &cs2, &dedt, &dpderho, &dpdrhoe, 
      &xa, &xh, &xn, &xp, &xabar, &xzbar, &mu_e, &mu_n, &mu_p, &muhat, &keytemp, 
      &keyerr, &rfeps); 
  
  if (keyerr != 0) throw std::runtime_error("Bad EOS call.");
  
  return prs;  
}

double EOSDriver::GetEpsFromTemperature(double rho, double ye, double t) {
  double enr, prs, ent, cs2, dedt, dpderho, dpdrhoe, xa, xh, xn, xp, xabar,
      xzbar, mu_e, mu_n, mu_p, muhat;
  double rfeps = 1.e-8; 
  int keyerr; 
  int keytemp = 1;
  nuc_eos_full_(&rho, &t, &ye, &enr, &prs, &ent, &cs2, &dedt, &dpderho, &dpdrhoe, 
      &xa, &xh, &xn, &xp, &xabar, &xzbar, &mu_e, &mu_n, &mu_p, &muhat, &keytemp, 
      &keyerr, &rfeps); 
  
  if (keyerr != 0) throw std::runtime_error("Bad EOS call.");
  
  return enr;  
}

void EOSDriver::GetPressureEpsFromTemperature(double rho, double ye, double t, double* press,
					   double *eps) {
  double enr, prs, ent, cs2, dedt, dpderho, dpdrhoe, xa, xh, xn, xp, xabar,
      xzbar, mu_e, mu_n, mu_p, muhat;
  double rfeps = 1.e-8; 
  int keyerr; 
  int keytemp = 1;
  nuc_eos_full_(&rho, &t, &ye, eps, press, &ent, &cs2, &dedt, &dpderho, &dpdrhoe, 
      &xa, &xh, &xn, &xp, &xabar, &xzbar, &mu_e, &mu_n, &mu_p, &muhat, &keytemp, 
      &keyerr, &rfeps); 
  
  if (keyerr != 0) throw std::runtime_error("Bad EOS call.");
  
}

void EOSDriver::GetPressureEpsAbarFromTemperature(double rho, double ye, double t, double* press,
						  double *eps, double *abar) {
  double enr, prs, ent, cs2, dedt, dpderho, dpdrhoe, xa, xh, xn, xp, xabar,
      xzbar, mu_e, mu_n, mu_p, muhat;
  double rfeps = 1.e-8; 
  int keyerr; 
  int keytemp = 1;
  nuc_eos_full_(&rho, &t, &ye, eps, press, &ent, &cs2, &dedt, &dpderho, &dpdrhoe, 
      &xa, &xh, &xn, &xp, &xabar, &xzbar, &mu_e, &mu_n, &mu_p, &muhat, &keytemp, 
      &keyerr, &rfeps); 

  const double ytot1 = xn/1.0e0 + xp/1.0e0 + xa/4.0e0 + xh/xabar;
  *abar = 1.0/ytot1;
  
  if (keyerr != 0) throw std::runtime_error("Bad EOS call.");
  
}

