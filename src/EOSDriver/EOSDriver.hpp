/// \file EOSDriver.hpp
/// \author lroberts
/// \since Mar 17, 2016
///
/// \brief
///
///

#ifndef EOSDRIVER_HPP_
#define EOSDRIVER_HPP_

#include <string> 
namespace EOSDriver {
  void ReadTable(std::string eos_file);
  double GetPressureFromEntropy(double rho, double ye, double s);
  double GetPressureFromTemperature(double rho, double ye, double t);
  double GetEpsFromTemperature(double rho, double ye, double t);
  void GetPressureEpsFromTemperature(double rho, double ye, double t,
				     double *press, double *eps);
  void GetPressureEpsAbarFromTemperature(double rho, double ye, double t,
					 double *press, double *eps,
					 double *abar);
}
#endif // EOSDRIVER_HPP_
