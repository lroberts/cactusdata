/// \file CactusDataset.hpp
/// \author lroberts
/// \since Sep 09, 2015
///
/// \brief
///
///

#ifndef CACTUSDATASET_HPP_
#define CACTUSDATASET_HPP_

#include <memory> 
#include <vector> 
#include <array> 
#include <utility> 
#include <H5Cpp.h> 
#include <exception>
#include <NDArray.hpp> 
#include <cmath>
#include <cstdlib>

template <int Nd>
class CactusDataset {
public:
  CactusDataset(const H5::DataSet& h5Dset, int cl=0, 
      std::vector<bool> reflect = std::vector<bool>(Nd, false));
  CactusDataset(const std::vector<CactusDataset<Nd>>& subsets);
  CactusDataset(const CactusDataset<Nd> &CDS, const bool make_zero ); 
  
  void WriteInfo() const;

  std::shared_ptr<H5::DataSet> 
  writeToH5(const H5::CommonFG& group) const;
  
  int getTimestep() const {return mTimestep;}
  
  int getTime() const {return mTime;}
  
  int getReflevel() const {return mLevel;}
  
  int getC() const {return mC;} 

  std::string getName() const {return mName;} 

  std::array<double,Nd> getDelta() const {return mDelta;}

  std::vector<double> getOrigin() const {return std::vector<double>(mOrigin);} 
  
  std::vector<double> getMaxExtent() const {
    std::vector<double> extent;
    for (int i=0; i<Nd; ++i) 
        extent.push_back((mData.dimSize(i)-1)*mDelta[i] + mOrigin[i]); 
    return extent;
  }
  
  double& operator()(std::array<std::size_t, Nd> idxs) {
    return mData(idxs);
  }     
  
  double operator()(std::array<std::size_t, Nd> idxs) const { 
    return mData(idxs);
  } 

  double operator()(std::size_t idx) const { 
    return mData[idx];
  } 

  std::vector<double> interpolate(std::vector<std::array<double, Nd>> xarr) const;
  
  double maskedSum(double min_rad, double max_rad) const;
  double maskedSumPos(double min_rad, double max_rad) const;
  double maskedSumMultMask(const CactusDataset<Nd> &mask,
			   const double min_rad, const double max_rad) const;
  double maskedSumMultMaskGreaterVal(const CactusDataset<Nd> &mask,
				     const double min_rad, const double max_rad, 
				     const double val) const;
  double maskedSumMultMaskPos(const CactusDataset<Nd> &mask,
			      const double min_rad, const double max_rad) const;

protected:
  
  std::string mName; 
  H5::StrType mNameType; 
   
  int mTimestep; 
  int mLevel, mTimeLevel, mCarpetLevel;
  int mC; 
  double mTime;
   
  NDArray<double, Nd> mData;
  std::array<double, Nd> mDelta;
  std::array<double, Nd> mOrigin;
  std::array<int, Nd> mIorigin, mIoffset, mIoffsetdenom;
  std::array<int, Nd> mNGhost;
  std::array<int, 2*Nd> mBbox;
  std::array<bool, Nd> mReflectBoundary;
};

// Have to include implementation in header file
template<int Nd>
std::vector<double> 
CactusDataset<Nd>::interpolate(std::vector<std::array<double, Nd>> xarr) const { 
   
  std::vector<double> val(xarr.size()); 
  std::array<std::size_t, Nd> arr_size; 
  for (int i=0; i<Nd; ++i) arr_size[i] = mData.dimSize(i);

#pragma omp parallel for
  for (int iarr=0; iarr<xarr.size(); ++iarr) { 
    
    std::array<std::size_t, Nd> idx;
    for (std::size_t i=0; i<Nd; ++i) {
      double xx = xarr[iarr][i];
      if (mReflectBoundary[i]) xx = std::abs(xx);
      idx[i] = (std::size_t) (xx - mOrigin[i] + 0.5*mDelta[i])/mDelta[i];
      if (idx[i] < 0) idx[i]=0; 
      if (idx[i] >= arr_size[i]) idx[i] = arr_size[i]-1; 
    }
    //    std::cout << xarr[iarr][0] << " " << xarr[iarr][1] << " " << xarr[iarr][2];

    //    std::cout << ": "<< idx[0] << " " << idx[1] << " " << idx[2] << std::endl;
    std::array<std::size_t, Nd> idx2;
    idx2[0] = idx[2];
    idx2[1] = idx[1];
    idx2[2] = idx[0];

    val[iarr] = mData(idx2); 
  }
  return val;
}

template<int Nd>
double
CactusDataset<Nd>::maskedSum(double min_rad, double max_rad) const { 
  double sum = 0.0;
  int npts = 1;
  for (int j=0; j<Nd; ++j) 
    npts *= mData.dimSize(j);
#pragma omp parallel for reduction(+:sum)
  for (int i=0; i<npts; ++i) {
    auto idx = mData.GetIdxInverse(i);
    // Calculate the radius of this point
    double r = 0.0;
    double weight = 1.0; 
    for (int j=0; j<Nd; ++j) {
        r += pow( ((double)idx[j])*mDelta[j] + mOrigin[j], 2);
        if (idx[j]<mNGhost[j]) weight = 0.0;
	if (idx[j]==mNGhost[j]) weight *= 0.5;
        if (idx[j]>=mData.dimSize(j) - mNGhost[j]) weight = 0.0;
        if (idx[j]==mData.dimSize(j) - mNGhost[j] - 1) weight *= 0.5;
    }
    r = sqrt(r);
    if (r >= min_rad  && r <= max_rad) sum += mData[i]*weight; 
  }
  return sum;
}

template<int Nd>
double
CactusDataset<Nd>::maskedSumMultMaskGreaterVal(const CactusDataset<Nd> &mask, 
					       const double min_rad,
					       const double max_rad,
					       const double val) const { 
  double sum = 0.0;
  int npts = 1;
  for (int j=0; j<Nd; ++j) 
    npts *= mData.dimSize(j);
#pragma omp parallel for reduction(+:sum)
  for (int i=0; i<npts; ++i) {
    auto idx = mData.GetIdxInverse(i);
    // Calculate the radius of this point
    double r = 0.0;
    double weight = 1.0; 
    for (int j=0; j<Nd; ++j) {
        r += pow( ((double)idx[j])*mDelta[j] + mOrigin[j], 2);
        if (idx[j]<mNGhost[j]) weight = 0.0;
	if (idx[j]==mNGhost[j]) weight *= 0.5;
        if (idx[j]>=mData.dimSize(j) - mNGhost[j]) weight = 0.0;
        if (idx[j]==mData.dimSize(j) - mNGhost[j] - 1) weight *= 0.5;
    }
    r = sqrt(r);
    if (r >= min_rad  && r <= max_rad && mData[i] > val) 
      sum += mData[i]*weight*mask(i); 
  }
  return sum;
}

template<int Nd>
double
CactusDataset<Nd>::maskedSumMultMaskPos(const CactusDataset<Nd> &mask, 
					const double min_rad,
					const double max_rad) const { 
  double sum = 0.0;
  int npts = 1;
  for (int j=0; j<Nd; ++j) 
    npts *= mData.dimSize(j);
#pragma omp parallel for reduction(+:sum)
  for (int i=0; i<npts; ++i) {
    auto idx = mData.GetIdxInverse(i);
    // Calculate the radius of this point
    double r = 0.0;
    double weight = 1.0; 
    for (int j=0; j<Nd; ++j) {
        r += pow( ((double)idx[j])*mDelta[j] + mOrigin[j], 2);
        if (idx[j]<mNGhost[j]) weight = 0.0;
	if (idx[j]==mNGhost[j]) weight *= 0.5;
        if (idx[j]>=mData.dimSize(j) - mNGhost[j]) weight = 0.0;
        if (idx[j]==mData.dimSize(j) - mNGhost[j] - 1) weight *= 0.5;
    }
    r = sqrt(r);
    if (r >= min_rad  && r <= max_rad && mData[i] > 0.0) 
      sum += mData[i]*weight*mask(i); 
  }
  return sum;
}

template<int Nd>
double
CactusDataset<Nd>::maskedSumMultMask(const CactusDataset<Nd> &mask, 
				     const double min_rad,
				     const double max_rad) const { 
  double sum = 0.0;
  int npts = 1;
  for (int j=0; j<Nd; ++j) 
    npts *= mData.dimSize(j);
#pragma omp parallel for reduction(+:sum)
  for (int i=0; i<npts; ++i) {
    auto idx = mData.GetIdxInverse(i);
    // Calculate the radius of this point
    double r = 0.0;
    double weight = 1.0; 
    for (int j=0; j<Nd; ++j) {
        r += pow( ((double)idx[j])*mDelta[j] + mOrigin[j], 2);
        if (idx[j]<mNGhost[j]) weight = 0.0;
	if (idx[j]==mNGhost[j]) weight *= 0.5;
        if (idx[j]>=mData.dimSize(j) - mNGhost[j]) weight = 0.0;
        if (idx[j]==mData.dimSize(j) - mNGhost[j] - 1) weight *= 0.5;
    }
    r = sqrt(r);
    if (r >= min_rad  && r <= max_rad) 
      sum += mData[i]*weight*mask(i); 
  }
  return sum;
}


template<int Nd>
double
CactusDataset<Nd>::maskedSumPos(double min_rad, double max_rad) const { 
  double sum = 0.0;
  int npts = 1;
  for (int j=0; j<Nd; ++j) 
    npts *= mData.dimSize(j);
#pragma omp parallel for reduction(+:sum)
  for (int i=0; i<npts; ++i) {
    auto idx = mData.GetIdxInverse(i);
    // Calculate the radius of this point
    double r = 0.0;
    double weight = 1.0; 
    for (int j=0; j<Nd; ++j) {
        r += pow( ((double)idx[j])*mDelta[j] + mOrigin[j], 2);
        if (idx[j]<mNGhost[j]) weight = 0.0;
	if (idx[j]==mNGhost[j]) weight *= 0.5;
        if (idx[j]>=mData.dimSize(j) - mNGhost[j]) weight = 0.0;
        if (idx[j]==mData.dimSize(j) - mNGhost[j] - 1) weight *= 0.5;
    }
    r = sqrt(r);
    if (r >= min_rad  && r <= max_rad && mData[i] > 0.0) 
      sum += mData[i]*weight; 
  }
  return sum;
}
 
template <int Nd>
CactusDataset<Nd>::CactusDataset(const H5::DataSet& h5Dset, int cl, 
    std::vector<bool> reflect) 
    :  mData(NDArray<double,Nd>::ReadFromH5(h5Dset)), mC(cl) {
  
  for (int i=0; i<Nd; ++i) mReflectBoundary[i] = reflect[i];  
  
  for (unsigned int i=0; i<h5Dset.getNumAttrs(); ++i) {
    auto att = h5Dset.openAttribute(i);
    //std::cout << att.getName() << std::endl;
    if (att.getName() == "iorigin") 
      att.read(GetH5DataType<int>(), mIorigin.data());
    else if (att.getName() == "ioffset") 
      att.read(GetH5DataType<int>(), mIoffset.data());
    else if (att.getName() == "ioffsetdenom") 
      att.read(GetH5DataType<int>(), mIoffsetdenom.data());
    else if (att.getName() == "cctk_nghostzones") 
      att.read(GetH5DataType<int>(), mNGhost.data());
    else if (att.getName() == "cctk_bbox") 
      att.read(GetH5DataType<int>(), mBbox.data());
    else if (att.getName() == "origin") 
      att.read(GetH5DataType<double>(), mOrigin.data());
    else if (att.getName() == "delta") 
      att.read(GetH5DataType<double>(), mDelta.data());
    else if (att.getName() == "timestep") 
      att.read(GetH5DataType<int>(), &mTimestep);
    else if (att.getName() == "level") 
      att.read(GetH5DataType<int>(), &mLevel);
    else if (att.getName() == "carpet_mglevel") 
      att.read(GetH5DataType<int>(), &mCarpetLevel);
    else if (att.getName() == "group_timelevel") 
      att.read(GetH5DataType<int>(), &mTimeLevel);
    else if (att.getName() == "time") 
      att.read(GetH5DataType<double>(), &mTime);
    else if (att.getName() == "name") {
      mNameType = att.getStrType();
      att.read(mNameType, mName);
    }
  }
}

template <int Nd>
CactusDataset<Nd>::CactusDataset(const std::vector<CactusDataset>& subsets) {
  
  if (subsets.size()==0) 
    throw std::length_error("Must have at least one subset");
     
  // Set the properties of the dataset
  mReflectBoundary = subsets[0].mReflectBoundary; 
  mName = subsets[0].mName; 
  mNameType = subsets[0].mNameType; 

  mTimestep = subsets[0].mTimestep;
  mLevel = subsets[0].mLevel; 
  mTimeLevel = subsets[0].mTimeLevel; 
  mCarpetLevel = subsets[0].mCarpetLevel; 
  mTime = subsets[0].mTime;
  
  mDelta = subsets[0].mDelta;
  mIoffset = subsets[0].mIoffset;
  mIoffsetdenom = subsets[0].mIoffsetdenom; 
  mNGhost = subsets[0].mNGhost;
  mBbox = subsets[0].mBbox;

  // Check that all the datasets are on the same refinement level, step, etc. 
  for (auto& sset : subsets) { 
    assert(sset.mName == mName); 
    assert(sset.mTimestep == mTimestep);
    assert(sset.mLevel == mLevel);
    assert(sset.mTimeLevel == mTimeLevel);
  }
   
  // Find the minimum and maximum extents of the subsets
  // we need to reverse the arrays since the dimensions are 
  // ordered the opposite of what one might expect
  std::array<double, Nd> min, max;
  std::array<int, Nd> Imin, Imax;
   
  for (int i=0; i<Nd; ++i) {
    min[Nd-i-1] = subsets[0].mOrigin[i]; 
    max[Nd-i-1] = subsets[0].mOrigin[i] 
        + subsets[0].mData.dimSize(Nd-i-1)*subsets[0].mDelta[i]; 
    Imin[Nd-i-1] = subsets[0].mIorigin[i]; 
    Imax[Nd-i-1] = subsets[0].mIorigin[i] + subsets[0].mData.dimSize(Nd-i-1); 
  } 
  
  for (auto& sset : subsets) {
    for (int i=0; i<Nd; ++i) {
      min[Nd-i-1] = std::min(min[Nd-i-1], sset.mOrigin[i]); 
      max[Nd-i-1] = std::max(max[Nd-i-1], sset.mOrigin[i]
          + sset.mData.dimSize(Nd-i-1)*sset.mDelta[Nd-i-1]); 
      Imin[Nd-i-1] = std::min(Imin[Nd-i-1], sset.mIorigin[i]); 
      Imax[Nd-i-1] = std::max(Imax[Nd-i-1], sset.mIorigin[i] 
          + (int) sset.mData.dimSize(Nd-i-1));
    } 
  } 
  mOrigin = min;
  mIorigin = Imin;

  // Allocate the correct size dataset
  std::array<size_t, Nd> arrSize; 
  for (int i=0; i<Nd; ++i) arrSize[i] = (size_t)(Imax[i] - Imin[i]);
  mData = NDArray<double, Nd>(arrSize);

  // Copy over subsets to main dataset 
  for (auto& sset : subsets) { 
    std::array<std::size_t, Nd> offset; 
    for (unsigned int i=0; i<Nd; ++i) 
        offset[i] = (std::size_t)
        ((sset.mOrigin[Nd-i-1] - mOrigin[Nd-i-1])/mDelta[Nd-i-1]);
    //mData.copyBlock(sset.mData, offset); 
    for (unsigned int i=0; i<sset.mData.size(); ++i) {
      // Calculate the n-dimensional index 
      std::array<size_t, Nd> sidx;
      unsigned int delta = 0;  
      for (int j=Nd-1; j>=0; --j) {
        int base = 1;
        for (int k=0; k<j; ++k) base *= sset.mData.dimSize(k); 
        sidx[j] = (i - delta)/base;
        delta += base*sidx[j]; 
      }
      std::array<size_t, Nd> idx; 
      for (unsigned int j=0; j<Nd; ++j) idx[j] = sidx[j] + offset[j];
      mData(idx) = sset.mData(sidx); 
    }
  }   
}
  
// copy constructor
template <int Nd>
CactusDataset<Nd>::CactusDataset(const CactusDataset<Nd> &CDS, 
				 const bool make_zero ) {


 // Set the properties of the dataset
  mReflectBoundary = CDS.mReflectBoundary; 
  mName = CDS.mName; 
  mNameType = CDS.mNameType; 

  mTimestep = CDS.mTimestep;
  mLevel = CDS.mLevel; 
  mTimeLevel = CDS.mTimeLevel; 
  mCarpetLevel = CDS.mCarpetLevel; 
  mTime = CDS.mTime;
  
  mDelta = CDS.mDelta;
  mIoffset = CDS.mIoffset;
  mIoffsetdenom = CDS.mIoffsetdenom; 
  mNGhost = CDS.mNGhost;
  mBbox = CDS.mBbox;

  mOrigin = CDS.mOrigin;
  mIorigin = CDS.mIorigin;

  // Allocate the correct size dataset
  std::array<size_t, Nd> arrSize; 
  for (int i=0; i<Nd; ++i) arrSize[i] = CDS.mData.dimSize(i);
  mData = NDArray<double, Nd>(arrSize);

  // initialize with zeros
  if (make_zero) {
#pragma omp parallel for
    for (unsigned int i=0; i < mData.size(); ++i) {
      mData[i] = 0.0;
    }
  } else {
    // or copy
#pragma omp parallel for
    for (unsigned int i=0; i<mData.size(); ++i) {
      mData[i] = CDS.mData[i];
    }
  }

}



template <int Nd>
std::shared_ptr<H5::DataSet> 
CactusDataset<Nd>::writeToH5(const H5::CommonFG& group) const {

  // Create a dataset and write out the data 
  std::string dsetName = mName 
      + " it=" + std::to_string(mTimestep) 
      + " tl=" + std::to_string(mTimeLevel)
      + " rl=" + std::to_string(mLevel)
      + " c="  + std::to_string(0);
   
  H5E_auto2_t old_func;
  void *old_client_data;
  H5Eget_auto(H5E_DEFAULT, &old_func, &old_client_data);
  H5Eset_auto(H5E_DEFAULT, NULL, NULL);
  try {
    
    auto temp = group.openDataSet(dsetName); 
    group.move(dsetName, dsetName + " deprecated");
  } catch(...) {}
  H5Eset_auto(H5E_DEFAULT, old_func, old_client_data);
  
  auto dset = mData.WriteToH5(group, dsetName); 
  
  // Write required attributes to the dataset 
  hsize_t asize[1];
  asize[0] = Nd*2;
  dset->createAttribute("cctk_bbox", GetH5DataType<int>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<int>(), mBbox.data());  

  asize[0] = Nd;
  dset->createAttribute("iorigin", GetH5DataType<int>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<int>(), mIorigin.data());  
  dset->createAttribute("ioffset", GetH5DataType<int>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<int>(), mIoffset.data());  
  dset->createAttribute("ioffsetdenom", GetH5DataType<int>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<int>(), mIoffsetdenom.data());  
  dset->createAttribute("cctk_nghostzones", GetH5DataType<int>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<int>(), mNGhost.data());  
  dset->createAttribute("origin", GetH5DataType<double>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<double>(), mOrigin.data());  
  dset->createAttribute("delta", GetH5DataType<double>(), H5::DataSpace(1,asize)) 
      .write(GetH5DataType<double>(), mDelta.data());
       
  dset->createAttribute("time", GetH5DataType<double>(), H5::DataSpace()) 
      .write(GetH5DataType<double>(), &mTime); 
  dset->createAttribute("level", GetH5DataType<int>(), H5::DataSpace()) 
      .write(GetH5DataType<int>(), &mLevel); 
  dset->createAttribute("timestep", GetH5DataType<int>(), H5::DataSpace()) 
      .write(GetH5DataType<int>(), &mTimestep); 
  dset->createAttribute("carpet_mglevel", GetH5DataType<int>(), H5::DataSpace()) 
      .write(GetH5DataType<int>(), &mCarpetLevel); 
  dset->createAttribute("group_timelevel", GetH5DataType<int>(), H5::DataSpace()) 
      .write(GetH5DataType<int>(), &mTimeLevel); 
  dset->createAttribute("name", mNameType, H5::DataSpace()) 
      .write(mNameType, mName); 
  
  return dset;
}

template <int Nd>
void CactusDataset<Nd>::WriteInfo() const { 
  std::cout << "Timestep: " << mTimestep << std::endl;
  std::cout << "Reflevel: " << mLevel << std::endl;
  
  std::cout << "Ghost zones: "; 
  for (int i=0; i<Nd; ++i) std::cout << mNGhost[i] << " ";
  std::cout << std::endl; 

  std::cout << "Iorigin: ";
  for (int i=0; i<Nd; ++i) std::cout << mIorigin[i] << " ";
  std::cout << std::endl; 

  std::cout << "Ioffset: ";
  for (int i=0; i<Nd; ++i) std::cout << mIoffset[i] << " ";
  std::cout << std::endl; 

  std::cout << "Ioffsetdenom: "; 
  for (int i=0; i<Nd; ++i) std::cout << mIoffsetdenom[i] << " ";
  std::cout << std::endl; 

  std::cout << "Delta: ";
  for (int i=0; i<Nd; ++i) std::cout << mDelta[i] << " ";
  std::cout << std::endl; 

  std::cout << "Origin: "; 
  for (int i=0; i<Nd; ++i) std::cout << mOrigin[i] << " ";
  std::cout << std::endl; 
}

#endif // CACTUSDATASET_HPP_
