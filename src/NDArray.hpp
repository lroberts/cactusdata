/// \file NDArray.hpp
/// \author lroberts
/// \since Sep 09, 2015
///
/// \brief
///
///

#ifndef EOS_NDARRAY_HPP_
#define EOS_NDARRAY_HPP_

#include <memory> 
#include <vector> 
#include <utility>
#include <string>
#include <numeric>
#include <algorithm> 
#include <H5Cpp.h> 
#include <iostream> 
#include <cassert>

template<class T> 
H5::DataType GetH5DataType() {
  if (std::is_same<T, int>::value) {
    return H5::DataType(H5::PredType::NATIVE_INT);
  }
  else if (std::is_same<T, float>::value) {
    return H5::DataType(H5::PredType::NATIVE_FLOAT);
  }
  else if (std::is_same<T, double>::value) {
    return H5::DataType(H5::PredType::NATIVE_DOUBLE);
  }
  else if (std::is_same<T, long>::value) {
    return H5::DataType(H5::PredType::NATIVE_LONG);
  }
  else if (std::is_same<T, char>::value) {
    return H5::DataType(H5::PredType::NATIVE_CHAR);
  }
  else if (std::is_same<T, std::string>::value) {
    return H5::StrType(H5::PredType::C_S1);
  } else {
    throw std::invalid_argument("Unknown type");
  }
}


template <class T, int Nd>
class NDArray {
public:
  NDArray(): mNTot(1) { mData = new T[mNTot];}
   
  NDArray(std::array<std::size_t, Nd> n) {
    mN = n;
    mNTot = std::accumulate(mN.begin(), mN.end(), 1, 
        std::multiplies<std::size_t>());
    mData = new T[mNTot];
  } 
  
  NDArray(const NDArray<T,Nd>& old) {
    mNTot = old.mNTot;
    mN = old.mN;
    mData = new T[mNTot]; 
    for (unsigned int i=0; i<mNTot; ++i) mData[i] = old.mData[i];
  } 
  
  NDArray<T,Nd>& operator=(const NDArray<T,Nd>& old) {
    if (this == &old) return *this; 
    delete [] mData; 
    mNTot = old.mNTot;
    mN = old.mN;
    mData = new T[mNTot]; 
    for (unsigned int i=0; i<mNTot; ++i) mData[i] = old.mData[i];
    return *this;
  }
  
  ~NDArray() {
    delete [] mData;
   }  
  
  static NDArray<T, Nd> ReadFromH5(const H5::DataSet& h5Dset);
  
  T& operator()(std::array<std::size_t, Nd> idxs) {
    return mData[GetIdx(idxs)];
  }     
  
  T operator()(std::array<std::size_t, Nd> idxs) const { 
    return mData[GetIdx(idxs)];
  } 
  
  T& operator[](std::size_t idx) {
    if (idx >= mNTot) 
      throw std::length_error("Index out of allocated memory region.");
    return mData[idx];
  }     
  
  T operator[](std::size_t idx) const {
    if (idx >= mNTot) 
      throw std::length_error("Index out of allocated memory region.");
    return mData[idx];
  }    
   
  void copyBlock(const NDArray<T, Nd> inData, std::array<std::size_t, Nd> origin);
  
  std::size_t size() const { return mNTot; }
  
  std::size_t dimSize(int dim) const { return mN[dim];} 
    
  std::shared_ptr<H5::DataSet> 
  WriteToH5(const H5::CommonFG& group, const std::string& dsetName) const;

  std::size_t GetIdx(const std::array<std::size_t, Nd>& idxs) const;
  std::array<size_t, Nd> GetIdxInverse(const std::size_t idx) const;

  // Cactus versions: column major
  std::size_t GetIdxCactus(const std::array<std::size_t, Nd>& idxs) const;
  std::array<size_t, Nd> GetIdxInverseCactus(const std::size_t idx) const;

protected:
  T* mData;
  std::size_t mNTot; 
  std::array<std::size_t, Nd> mN;
    
};

template <class T, int Nd>
void NDArray<T,Nd>::copyBlock(const NDArray<T, Nd> inData, 
    std::array<std::size_t, Nd> origin) {
  
  int npt = 1;
  for (int i=0; i<Nd-1; ++i) npt *= inData.mN[i];
  
  for (int p=0; p<npt; ++p) {
    int toffset = inData.mN[Nd-1]*p;
    auto tidxs = inData.GetIdxInverse(toffset);
    std::array<std::size_t, Nd> midxs = tidxs;
    for (int i=0; i<Nd; ++i) midxs[i] += origin[i];
    std::size_t moffset = GetIdx(midxs);
    // Innermost loop should vectorize easily
    // this needs to be consistent with GetIdx
    T* dpt = &mData[moffset];
    T* dpo = &inData.mData[toffset];
    if (dpt + inData.mN[Nd-1] < dpo || dpo + inData.mN[Nd-1] < dpt) 
    //#pragma GCC ivdep
    for (int j=0; j<inData.mN[Nd-1]; ++j) dpt[j] = dpo[j]; 
    else for (int j=0; j<inData.mN[Nd-1]; ++j) dpt[j] = dpo[j]; 
  }
}

// Implementations have to be in header for template classes
template <class T, int Nd>
NDArray<T, Nd> NDArray<T,Nd>::ReadFromH5(const H5::DataSet& h5Dset) {
  H5::DataSpace dspace = h5Dset.getSpace(); 
  int ndim = dspace.getSimpleExtentNdims(); 
  if (ndim>Nd) 
      throw std::range_error("Too many dimensions in H5 dataset for NDArray");
  hsize_t dimSize[ndim];
  dspace.getSimpleExtentDims(dimSize); 
  std::array<std::size_t, Nd> dimSizeArr;
  for (int i=0; i<Nd; ++i) dimSizeArr[i] = dimSize[i];
  NDArray<T, Nd> arr(dimSizeArr);
  // Read in data here
  H5::DataType h5DType = GetH5DataType<T>();
  h5Dset.read(arr.mData, h5DType);
  return arr;
}

template <class T, int Nd>
std::size_t NDArray<T, Nd>::GetIdxCactus(const std::array<std::size_t, Nd>& idxs) const {
  std::size_t idx = 0;
  
  // Storage needs to be in this direction to work with HDF5
  for (int i=Nd-1; i>0; --i) {
    if (idxs[i] >= mN[i]) 
      throw std::length_error("Trying to access element in matrix beyond size.");
    idx += idxs[i];
    idx *= mN[i-1];
  } 
  idx += idxs[0];
  if (idx >= mNTot) 
    throw std::length_error("Index out of allocated memory region.");
  
  return idx;
}

// this is a version for row-major arrays
template <class T, int Nd>
std::size_t NDArray<T, Nd>::GetIdx(const std::array<std::size_t, Nd>& idxs) const {
  std::size_t idx = 0;
  
  // Storage needs to be in this direction to work with HDF5
  for (int i=0; i<Nd-1; ++i) {
    if (idxs[i] >= mN[i]) 
      throw std::length_error("Trying to access element in matrix beyond size.");
    idx += idxs[i];
    idx *= mN[i+1];
  } 
  idx += idxs[Nd-1];
  if (idx >= mNTot) 
    throw std::length_error("Index out of allocated memory region.");
  
  return idx;
}


template <class T, int Nd>
std::array<std::size_t, Nd> NDArray<T, Nd>::GetIdxInverseCactus(
    std::size_t idx) const {
  // Cactus arrays are column major (like Fortran).
  // So in (x,y,z), x will be varying fastest.
  std::array<std::size_t, Nd> idxs; 
  std::size_t idx_loc = idx;
  for (int i=Nd-1; i>0; --i) {
    int base = 1;
    for(int j=0;j<i;++j) {
      base *= mN[j];
    }
    idxs[i] = idx_loc / base;
    idx_loc -= base*idxs[i];
  }
  idxs[0] = idx_loc;

  //  idxs[2] = idx / (mN[0]*mN[1]);
  // idxs[1] = (idx - mN[0]*mN[1]*idxs[2]) / (mN[0]);
  // idxs[0] = idx - idxs[1]*mN[0] - idxs[2]*mN[0]*mN[1];
  return idxs;
}


// this is code for row major arrays; it's what was here
template <class T, int Nd>
std::array<std::size_t, Nd> NDArray<T, Nd>::GetIdxInverse(
    std::size_t idx) const {
  std::array<std::size_t, Nd> idxs; 
  std::size_t idx_loc = idx;
  for (int i=Nd-1; i>0; --i) {
    int base = 1;
    idxs[i] = idx_loc;
    for (int j=i; j>=1; --j) {
      base = 1;
      for (int k=j; k>=1; --k) base *= mN[k];
      idxs[i] = idxs[i] % base;
    }  
    //for (int j=i; j>=1; --j) base *= mN[j];
    //std::cout << i << " " << idx << " " << idx % base 
    //  << " " << base << std::endl; 
    //idxs[i] = idx % base;
    idx_loc = (idx_loc - idxs[i]) / mN[i];
  }
  idxs[0] = idx_loc;
  return idxs;
}


template <class T, int Nd>
std::shared_ptr<H5::DataSet> NDArray<T, Nd>::WriteToH5(
    const H5::CommonFG& group, const std::string& dsetName) const {
  H5::DataType h5DType = GetH5DataType<T>();
  hsize_t dims[Nd]; 
  for (int i=0; i<Nd; ++i) dims[i] = mN[i]; 
  H5::DataSpace h5DSpace(Nd, dims); 
  std::shared_ptr<H5::DataSet> dset = std::make_shared<H5::DataSet>( 
      group.createDataSet(dsetName.c_str(), h5DType, h5DSpace));
  dset->write(mData, h5DType); 
  return dset;
}

#endif // EOS_NDARRAY_HPP_
