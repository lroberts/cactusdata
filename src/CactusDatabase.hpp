/// \file CactusDatabase.hpp
/// \author lroberts
/// \since Sep 09, 2015
///
/// \brief
///
///

#ifndef CACTUSDATABASE_HPP_
#define CACTUSDATABASE_HPP_

#include <memory> 
#include <vector> 
#include <array> 
#include <utility> 
#include <set>
#include <algorithm>
#include <H5Cpp.h> 

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"

#include "CactusDataset.hpp"

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

template<int Nd> 
class CactusDatabase {
public:
  CactusDatabase(std::string basepath, std::string basename, 
      std::string dirbase = "", 
      std::vector<bool> reflect = std::vector<bool>(Nd, false));  
  
  std::vector<CactusDataset<Nd>> getDatasets(std::string var, int tstep, int reflevel) const;
   
  void printDsets() const { 
    for (auto dset : mDsets) std::cout << dset.h5file << ": " << dset.key << std::endl;
  }

  std::set<int> getTsteps() const {return mTsteps;}
  std::set<std::string> getVariables() const {return mDvars;}
   
protected:
  struct dsetHeader {
    dsetHeader(std::string k, std::string f, std::string v, int ts, int rl, 
        int c, double t) : key(k), h5file(f), var(v), tstep(ts), rlevel(rl), 
        time(t), c(c) {}
    std::string key; 
    std::string h5file;
    std::string var; 
    int tstep; 
    int rlevel;
    int c;
    double time;
  }; 
  
  std::vector<dsetHeader> mDsets; 
  std::set<std::string> mDvars; 
  std::set<int> mTsteps, mReflevels; 
  std::set<double> mTimes;
  std::vector<bool> mReflectBoundary; 
  
}; 

template<int Nd>
std::vector<CactusDataset<Nd>> 
CactusDatabase<Nd>::getDatasets(std::string var, int tstep, int reflevel) const{
  std::vector<CactusDataset<Nd>> out; 
  bool found = false;
  for (auto dhead : mDsets) {
    if ((var.compare(dhead.var)==0) && tstep==dhead.tstep && reflevel==dhead.rlevel) {
      found = true;
      H5::H5File h5File(dhead.h5file, H5F_ACC_RDONLY); 
      out.push_back(CactusDataset<Nd>(h5File.openDataSet(dhead.key), dhead.c, 
          mReflectBoundary));
      h5File.close();
    }
  }  
  if(!found) {
    std::cerr << "Could not find dataset for " << var 
	      << " timestep " << tstep << " rl " << reflevel << std::endl;
    abort();
  }

  return out; 
}

template<int Nd>
CactusDatabase<Nd>::CactusDatabase(std::string basepath, std::string basename, 
    std::string dirbase, std::vector<bool> reflect) : mReflectBoundary(reflect){
   
  // Find all of the matching directories
  std::vector<std::string> dirs;
  if (!(dirbase.compare("")==0)) {
    bfs::directory_iterator end_itr; // Default ctor is past-the-end
    boost::regex filter(dirbase);
    for (bfs::directory_iterator i(basepath); i != end_itr; ++i) {
      if (!bfs::is_directory(i->status())) continue;
      
      std::string fname =  i->path().filename().string();
      boost::smatch what; 
      if (boost::regex_match(fname, what, filter)) {
         dirs.push_back(basepath + "/" + fname);  
      }
    }
  } else {
    dirs.push_back(basepath);
  }
  
  // Find all of the matching files in directories
  std::vector<std::string> files;
  for (auto dir : dirs) {
    std::cout << dir << std::endl;
    bfs::directory_iterator end_itr; // Default ctor is past-the-end
    boost::regex filter(basename);
    for (bfs::directory_iterator i(dir); i != end_itr; ++i) {
      if (bfs::is_directory(i->status())) continue;
      
      std::string fname =  i->path().filename().string();
      boost::smatch what; 
      if (boost::regex_match(fname, what, filter)) {
         //std::cout << basename << " " << fname << std::endl;
         files.push_back(dir + "/" + fname);  
      }
    }
  } 
   
  // Iterate over files and build up the headers
  for (auto file : files) {
    std::cout << file << std::endl;
    H5::H5File h5File(file, H5F_ACC_RDONLY);  
    for (unsigned int i=0; i < h5File.getNumObjs(); ++i) { 
      if (h5File.getObjTypeByIdx(i) == H5G_obj_t::H5G_DATASET) {
        std::string key = h5File.getObjnameByIdx(i);
        std::string dname = key.substr(0, key.find(" "));
        int it = -1; int tl = -1; int rl = -1; int c = -1;
        double t = 0.0;
        sscanf(key.c_str(), "%*s it=%d tl=%d rl=%d c=%d", &it, &tl, &rl, &c);
        if (it>=0 && tl>=0 && rl>=0 && c>=0) {
          mDsets.push_back(dsetHeader(key, file, dname, it, rl, c, t));
          mDvars.insert(dname); 
          mTsteps.insert(it);
          mReflevels.insert(rl);
          mTimes.insert(t); 
        }
      }
    }
  }
  std::sort(mDsets.begin(), mDsets.end(), [](dsetHeader a, dsetHeader b) {
    return (a.key < b.key);
  });
}

#endif // CACTUSDATABASE_HPP_
