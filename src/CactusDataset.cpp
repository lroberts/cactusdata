/// \file CactusDataset.cpp
/// \author lroberts
/// \since Sep 09, 2015
///
/// \brief
///
///

#include <iostream> 
#include <cassert>
#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 

// This file does nothing but allows us to make cmake work
