/// \file RealSphericalHarmonic.hpp
/// \author lroberts
/// \since Sep 09, 2015
///
/// \brief
///
///

#ifndef REALSPHERICALHARMONIC_HPP_
#define REALSPHERICALHARMONIC_HPP_

#include <vector>

#define NGLPTS 8

class RealSphericalHarmonic {
public: 
  RealSphericalHarmonic(int L, int m, std::vector<double> mus, 
      std::vector<double> phis); 
  double Integrate(const std::vector<std::vector<double>>& val); 
  
  const std::vector<std::vector<double>>& GetYlm() const {return Ylm;} 

protected: 
  double IntegratePhi(int L, int m, double mu, double phi_min, double phi_max) const;
  double IntegratePatch(int L, int m, double mu_min, double mu_max, 
      double phi_min, double phi_max) const;

  int mL;
  std::vector<double> mMus; 
  std::vector<double> mPhis; 
  std::vector<std::vector<double>> Ylm;

  std::array<double, NGLPTS> mXt; 
  std::array<double, NGLPTS> mWt;
};

#endif // REALSPHERICALHARMONIC_HPP_
