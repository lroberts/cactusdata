#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 

#include "EOSDriver/EOSDriver.hpp" 

int main() {
  
  EOSDriver::ReadTable("/work/00386/ux455321/stampede2/tables/SFHo.h5"); 
  
  double rho = 3.804074E+09;
  double temp = 1.445440E+00;
  double ye = 0.5;
  double press,eps;

  EOSDriver::GetPressureEpsFromTemperature(rho,ye,
					   temp,
					   &press,&eps);

  double temp0 = 0.01;
  double eps0;
  EOSDriver::GetPressureEpsFromTemperature(rho,ye,
					   temp0,
					   &press,&eps0);
  fprintf(stderr,"%15.6E %15.6E %15.6E %15.6E %15.6E %15.6E\n",rho,ye,temp,eps,eps0,eps-eps0);
  
  return 0;
}  
