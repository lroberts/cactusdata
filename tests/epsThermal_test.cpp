#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 

#include "epsThermal/epsThermal.hpp" 

int main() {
  const double temp_mev_to_kelvin = 1.1604447522806e10;

  double rho = 1.256385E+11;
  double temp = 4.589303E+10/temp_mev_to_kelvin;
  const double abar = 1.041346E+00;
  const double zbar = 1.606538E-01;
  double ye = zbar / abar;

  double press,eps;

  epsThermal::ReadTable("electron_table.h5");

#if 0
  int n = 10000;
  double rhox[10000];
  rhox[0] = rho;
  for(int i=1;i<n;i++) {
    rhox[i] = rhox[i-1]*1.00001e0;
  }

#pragma omp parallel for shared(rhox,temp,abar,ye)
  for(int i=0;i<n;i++) {

    double eps_thermal = epsThermal::GetThermalEnergy(rhox[i],temp,
						      ye,abar);


  }
#endif

  double eps_thermal = epsThermal::GetThermalEnergy(rho,temp,ye,abar);
  fprintf(stderr,"%15.6E %15.6E %15.6E %15.6E\n",rho,ye,temp,eps_thermal);

  return 0;
}  
