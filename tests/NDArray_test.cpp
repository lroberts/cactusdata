#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 

#include "NDArray.hpp"

#include <H5Cpp.h> 

int main() {
  NDArray<double, 2> arr({2, 2});
  arr({0,0}) = 0; 
  arr({0,1}) = 1; 
  arr({1,0}) = 2; 
  arr({1,1}) = 3; 
  // Create an array in an HDF5 file 
  {
    H5::H5File h5File("temporary_file.h5", H5F_ACC_TRUNC);
    arr.WriteToH5(h5File, "dset1");
    h5File.close(); 
  }
  
  // Read the array back in
  H5::H5File h5File("temporary_file.h5", H5F_ACC_RDONLY);
  NDArray<double, 2> arr2 = 
      NDArray<double, 2>::ReadFromH5(h5File.openDataSet("dset1"));
  std::cout << arr({0,1}) << " " << arr2({0,1}) << " " 
      << arr.size() << std::endl;

  if (arr({0,1}) != arr2({0,1}) || arr({0,1}) != 1) return 1;
  if (remove("temporary_file.h5")) return 1;
  
  {
    NDArray<double, 3> arr({6,6,6});
    std::size_t idx = 75;
    auto idxs = arr.GetIdxInverse(idx);
    auto idx2 = arr.GetIdx(idxs);
    if (idx != idx2) {
      std::cout << "Index inverse does not work " << idx << " " << idx2 << std::endl;
      return 1;
    }
  }
  
  return 0;
}  
