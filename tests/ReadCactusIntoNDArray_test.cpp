#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 

#include "NDArray.hpp"

#include <H5Cpp.h> 

int main() {
  H5::H5File h5File("hydrobase::temperature.xyz.file_100.h5", H5F_ACC_RDONLY);
  std::vector<std::string> dsetNames;
  for (unsigned int i=0; i < h5File.getNumObjs(); ++i) {
    if (h5File.getObjTypeByIdx(i) == H5G_obj_t::H5G_DATASET) {
      dsetNames.push_back(h5File.getObjnameByIdx(i));
    }
  }
  
  std::cout << dsetNames[0] << std::endl; 
  auto arr = NDArray<double, 3>::ReadFromH5(h5File.openDataSet(dsetNames[0]));
  std::cout << arr.size() << " " << arr({0,1,0}) << std::endl;
  h5File.close();
  return 0;
}  
