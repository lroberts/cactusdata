#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp" 

#include <H5Cpp.h> 

int main() {
  
  std::string path("/home/lroberts/Work/CactusData/test_data/");
  static const boost::regex filter("hydrobase::temperature.xyz.file_.*\.h5");
   
  std::vector<std::string> files; 
  
  // Find all matching files 
  boost::filesystem::directory_iterator end_itr; // Default ctor is past-the-end
  for (boost::filesystem::directory_iterator i(path); i != end_itr; ++i) {
    if (!boost::filesystem::is_regular_file(i->status())) continue;
    boost::smatch what; 
    if (!boost::regex_match(i->path().filename().string(), what, filter)) continue;
    files.push_back(i->path().filename().string());  
  } 
  
  // Read all of the data from all of the files 
  std::vector<CactusDataset<3>> dsets; 
  std::set<int> timeSteps, reflevels;
  std::set<double> xorigins;
  dsets.reserve(50*files.size()); 
  for (auto file : files) {
    H5::H5File h5File(path + file, H5F_ACC_RDONLY);  
    for (unsigned int i=0; i < h5File.getNumObjs(); ++i) {
      if (h5File.getObjTypeByIdx(i) == H5G_obj_t::H5G_DATASET) {
        std::string dname = h5File.getObjnameByIdx(i);
        auto arr = CactusDataset<3>(h5File.openDataSet(dname));
        if (arr.getReflevel()==5) {
          timeSteps.insert(arr.getTimestep()); 
          reflevels.insert(arr.getReflevel()); 
          dsets.push_back(arr);
        }
      }
    }
    h5File.close(); 
  }
  
  // Write out the combined dataset to file
  H5::H5File h5File("temp.h5", H5F_ACC_TRUNC);  
  for (auto tstep : timeSteps) {
    for (auto refl : reflevels) { 
      std::cout << tstep << " " << refl << std::endl; 
      std::vector<CactusDataset<3>> ds;
      for (auto& dset : dsets) { 
        if (dset.getReflevel()==refl && dset.getTimestep()==tstep) {
            std::cout << dset.getName() << std::endl;
            ds.push_back(dset);
        } 
      }
      std::cout << ds.size() << std::endl;
      auto full = CactusDataset<3>(ds);
      full.writeToH5(h5File);
    }
  }
  h5File.close(); 
  
  return 0;
}  
