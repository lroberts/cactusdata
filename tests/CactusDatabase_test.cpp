#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 

#include "CactusDatabase.hpp" 
#include "CactusDataset.hpp" 

#include <H5Cpp.h> 

int main() {

  CactusDatabase<3> dbase("/home/lroberts/Work/CactusData/test_data/", "hydrobase::.*file.*"); 
  
  H5::H5File h5Out("tmp.h5", H5F_ACC_TRUNC);
  
  for (auto tstep : dbase.getTsteps()) { 
    // Find the datasets for a single timestep
    std::cout << tstep << std::endl;
    auto darr = dbase.getDatasets("HYDROBASE::temperature", tstep, 5);
    std::cout << darr.size() << std::endl;
    try {  
      auto full = CactusDataset<3>(darr);
      full.writeToH5(h5Out); 
    } catch(...) {}
  }
  
  h5Out.close(); 

  return 0;
}  
