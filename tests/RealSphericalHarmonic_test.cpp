#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <vector> 
#include <iostream> 

#include "RealSphericalHarmonic.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"

int main() {
  std::vector<double> mus; 
  std::vector<double> phis; 

  for (double mu = -0.99; mu < 1.0; mu += 0.01) mus.push_back(mu);
  for (double phi = 0.01; phi < 2.0*3.14159; phi += 0.01) phis.push_back(phi);

  RealSphericalHarmonic y00(0, 0, mus, phis); 
  //RealSphericalHarmonic y11(1, 1, mus, phis); 
  //RealSphericalHarmonic y1m1(1, -1, mus, phis);
  
  std::vector<std::vector<double>> 
      ones(mus.size(), std::vector<double>(phis.size(), 1.0));
  
  std::vector<std::vector<double>> 
      func(mus.size(), std::vector<double>(phis.size()));
  for (int i=0; i<mus.size(); ++i) { 
    double theta = acos(mus[i]);
    for (int j=0; j<phis.size(); ++j) { 
      func[i][j] =
          boost::math::spherical_harmonic_r<double, double>(30, 0, theta, phis[j]); 
    }
  }    

  for (int i=1; i<80; ++i) {  
    RealSphericalHarmonic ylm(i, 0, mus, phis); 
    std::cout << i << " " << ylm.Integrate(func) << std::endl;
  } 

  return 0;
}  
