Some Notes on Usage:

Example for combining Y_e on reflevel 4 in one directory:

/work/00386/ux455321/stampede2/cactusdata/build/exe/CombineCactusData --basepath /scratch/00386/ux455321/M1/s40WH07_full --directory R0087 --basename "hydrobase-y_e.xyz.file_[0-9]+.h5" --outfile guenni_ye.h5 -R 4

Note the regular expression [0-9]+.

