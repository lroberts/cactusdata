#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp" 

#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>(), "String describing the base path")
      ("directory,D", po::value<std::string>(), "Regular expression describing the directories")
      ("basename,N", po::value<std::string>(), "Regular expression describing the base name")
      ("outfile,O", po::value<std::string>(), "Output filename")
      ("careful,C", "Carefully check file names in each directory (slow)")
      ("reflevel,R", po::value<int>(), "Specify refinement level (by default all are written to file)");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  bool carefulFileCheck = false;
  if (vm.count("careful")) carefulFileCheck = true; 
     
  std::string basepath; 
  if (!vm.count("basepath")) {
    std::cout << "No basepath provided, assuming current directory." << std::endl;
    basepath = "\./";
  } else {
    basepath = vm["basepath"].as<std::string>(); 
  }
   
  std::string directory; 
  if (!vm.count("directory")) {
    std::cout << "No directory provided, assuming basepath." << std::endl;
    directory = "\./";
  } else {
    directory = vm["directory"].as<std::string>(); 
  }
   
  std::string outfile; 
  if (!vm.count("outfile")) {
    outfile = "out.h5";
  } else {
    outfile = vm["outfile"].as<std::string>(); 
  }

  int reflevel; 
  if (!vm.count("reflevel")) {
    reflevel = -1;
  } else {
    reflevel = vm["reflevel"].as<int>(); 
  }

  if (!vm.count("basename")) {
    std::cerr << "basename is required but none was provided. Exiting." << std::endl;
    return 1;
  }
  std::cout << std::endl; 
  std::cout << "Base: "; 
  std::cout << vm["basename"].as<std::string>() << std::endl; 
  
  // Find all matching paths
  std::vector<std::string> dirs;
  {
    bfs::directory_iterator end_itr; // Default ctor is past-the-end
    //std::string path("./");
    std::string path(basepath);
    static const boost::regex filter(directory);
    for (bfs::directory_iterator i(path); i != end_itr; ++i) {
      if (!bfs::is_directory(i->status())) continue;
      boost::smatch what; 
      if (!boost::regex_match(i->path().filename().string(), what, filter)) continue;
      dirs.push_back(basepath + "/" + i->path().filename().string());  
    } 
  }
  
  if (dirs.size()<1) dirs.push_back(basepath); 
  std::sort(dirs.begin(), dirs.end());
  
  std::cout << "Folders: " << std::endl;
  for (auto dir : dirs) std::cout << dir << std::endl;
  std::cout << std::endl; 
   
  std::cout << "Starting recombination..." << std::endl;

  // Open output hdf5 file
  H5::H5File h5Out;
  if (!boost::filesystem::exists(outfile)) {
    h5Out = H5::H5File(outfile, H5F_ACC_TRUNC); 
  } else { 
    h5Out = H5::H5File(outfile, H5F_ACC_RDWR); 
  }
  
  std::vector<std::string> files; 
  static const boost::regex filter(vm["basename"].as<std::string>());
  // If we are assuming every directory has the same set of files
  if (!carefulFileCheck) {
    std::cout << "Iterating over files in first directory " << dirs[0] << ".\n"; 
    bfs::directory_iterator end_itr; // Default ctor is past-the-end
    for (bfs::directory_iterator i(dirs[0]); i != end_itr; ++i) {
      if (!bfs::is_regular_file(i->status())) continue;
      boost::smatch what; 
      if (!boost::regex_match(i->path().filename().string(), what, filter)) continue;
      files.push_back(i->path().filename().string());  
    }
    std::cout << "Found " << files.size() << " files in directory " << dirs[0] << ".\n"; 
  } 

  // Iterate over directories 
  std::vector<std::string> failed_dirs;
  for (auto path : dirs) {  
    try { 
      // Find all matching files
      if (carefulFileCheck) { 
        std::cout << "Iterating over files in directory " << path << ".\n"; 
        files = std::vector<std::string>();
        bfs::directory_iterator end_itr; // Default ctor is past-the-end
        for (bfs::directory_iterator i(path); i != end_itr; ++i) {
          if (!bfs::is_regular_file(i->status())) continue;
          boost::smatch what; 
          if (!boost::regex_match(i->path().filename().string(), what, filter)) continue;
          files.push_back(i->path().filename().string());  
        } 
      }
      
      std::cout << "Reading " << path << "...";
      std::cout.flush();

      // Read all of the data from all of the files 
      std::vector<CactusDataset<3>> dsets; 
      std::set<int> timeSteps, reflevels;
      std::set<double> xorigins;
      std::set<std::string> names;
      dsets.reserve(50*files.size()); 
      for (auto file : files) {
        H5::H5File h5File(path + "/" + file, H5F_ACC_RDONLY);  
        for (unsigned int i=0; i < h5File.getNumObjs(); ++i) {
          if (h5File.getObjTypeByIdx(i) == H5G_obj_t::H5G_DATASET) {
            std::string dname = h5File.getObjnameByIdx(i);
            int rl=-1; 
            sscanf(dname.c_str(),"%*s %*s %*s rl=%d", &rl);
            if (rl == reflevel || reflevel < 0) {
              auto arr = CactusDataset<3>(h5File.openDataSet(dname));
              timeSteps.insert(arr.getTimestep()); 
              reflevels.insert(arr.getReflevel()); 
              names.insert(arr.getName()); 
              dsets.push_back(arr);
            }
          }
        }
        h5File.close(); 
      }
      std::cout << " done." << std::endl; 
      
      for (auto name:names) {
        std::cout << "Dset : " << name << std::endl;    
      }  
      
      // Write out the combined dataset to file
      for (auto tstep : timeSteps) {
        for (auto refl : reflevels) { 
          for (auto name : names) { 
            std::cout << name << " Timestep: " << tstep 
                << " Refinement Level: " << refl << std::endl; 
            std::vector<CactusDataset<3>> ds;
            for (auto& dset : dsets) { 
              if (dset.getReflevel()==refl && dset.getTimestep()==tstep 
                  && dset.getName()==name) 
                  ds.push_back(dset); 
            }
            
            auto full = CactusDataset<3>(ds);
            
            try {
              full.writeToH5(h5Out);
            } catch(H5::Exception& e) {
              std::cerr << "H5 write failed with message : " << e.getDetailMsg() 
                  << std::endl;
            }
          } 
        }
      }
      std::cout << std::endl;
    } catch(...) {
      // Probably some hdf5 error, just move on to next file
      failed_dirs.push_back(path);
    }  
  } // End loop over files
  std::cout << "recombination done." << std::endl;
  h5Out.close();
  
  if (failed_dirs.size()>0) {
    std::cout << "Failed on directories: " << std::endl;
    for (auto dir : failed_dirs) std::cout << dir << std::endl;
  }
 
  
  return 0;
}  
