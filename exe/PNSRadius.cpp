#include <iostream>
#include <fstream>
#include <ostream>
#include <math.h>
#include <vector>
#include <memory>
#include <stdio.h>
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp"
#include "CactusDatabase.hpp"
#include "RealSphericalHarmonic.hpp"
#include "GainRegionAnalysis.hpp"
#include "DiagnosticEnergy.hpp"
#include "GravBaryMass.hpp"
#include <H5Cpp.h>

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

double find_rad(const std::vector<double> &rad, 
		const std::vector<double> &dens, 
		const double rhocut) {

  int i = 0;
  while(i < rad.size() && dens[i] > rhocut) {
    i++;
  }

  if( i >= rad.size() ) {
    return 0.0;
  } else {
    return rad[i];
  }

}

int main(int argc, char** argv) {

  // constants
  const double clite = 2.99792458e10;
  const double msun  =  1.9884e33;
  const double inv_time_gf = 4.92513293223396E-06;
  const double inv_rho_gf = 6.17714470405638E+17;
  const double inv_length_gf = 1.47651770773117E+05;
  const double time_gf = 1.0/inv_time_gf;

  // Set up the cl interface
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
    ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
    ("basename-rho,NR", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for rho")
    ("basename-vel,NV", po::value<std::string>()->default_value("hydrobase-vel.*\.h5"), "Regular expression describing the base name velocity")
    ("basename-metric,NM", po::value<std::string>()->default_value("admbase-metric.*\.h5"), "Regular expression describing the base name metric")
    ("outfile,O", po::value<std::string>()->default_value("PNSRadius.out"), "Output filename")
    ("octant", "Assume the data posesses reflecting octant symmetry")
    ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level")
    ("skip-iteration,S", po::value<std::vector<int> >()->multitoken(), "Skip this iteration [can be used multiple times]");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  int reflevel = vm["reflevel"].as<int>();

  // by specifying -S <iteration> -S <iteration> -S <iteration> and so forth,
  // one can skip iterations one doesn't want to analyze. That's useful if
  // some data are missing.
  std::vector<int> skipits(0);
  if(vm.count("skip-iteration")) {
    skipits = vm["skip-iteration"].as<std::vector<int>>();
  }

  //Build the database
  std::vector<bool> symmetry(3, false);
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true);

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(),
			  vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_vel(vm["basepath"].as<std::string>(),
			  vm["basename-vel"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_metric(vm["basepath"].as<std::string>(),
			  vm["basename-metric"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);


  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "WARNING! Some constants are hardcoded! Read the code!!!" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "Done reading data structures!" << std::endl;

  std::string vname_rho   = "HYDROBASE::rho";
  std::string vname_velx  = "HYDROBASE::vel[0]";
  std::string vname_vely  = "HYDROBASE::vel[1]";
  std::string vname_velz  = "HYDROBASE::vel[2]";
  std::string vname_gxx   = "ADMBASE::gxx";
  std::string vname_gyy   = "ADMBASE::gyy";
  std::string vname_gzz   = "ADMBASE::gzz";

  // Set up the spherical grid                                                                                                                     
  std::vector<double> phis, mus;
  const int nmu = 100;
  const int nphi = 200;
  const double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);

  const int nrad = 600;
  std::vector<double> radii(nrad);
  const double r0 = 1.0;

  // coordinates
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size()*nrad);
  // integer shock mask
  std::vector<int> mshock(mus.size()*phis.size()*nrad);

  // shock radius as a function of angle
  std::vector<std::vector<double>>
    shock_sphere(mus.size(),std::vector<double>(phis.size()));

  // open output file and write header
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);
  ofile << "#[1] Time (Msun) " << std::endl;
  ofile << "#[2] PNS radius sdetg*W*rho > 1.0e10 [cm]" << std::endl;
  ofile << "#[3] PNS radius sdetg*W*rho > 1.0e11 [cm]" << std::endl;
  ofile << "#[4] PNS radius sdetg*W*rho > 1.0e12 [cm]" << std::endl;
  ofile << "#[5] PNS radius sdetg*W*rho > 1.0e13 [cm]" << std::endl;

  // Iterate over timesteps
  for (auto tstep : dbase_rho.getTsteps()) {  
    
    // check if we need to skip the iteration
    bool found = false;
    for(std::vector<int>::const_iterator it = skipits.begin(); it < skipits.end(); ++it) {
      if( tstep == * it ) {
	std::cout << "Skipping iteration " << *it << " !" << std::endl;
	found = true;
      }
    }
    if (found) continue;

    std::cout << "*** Working on iteration " << (int) tstep << std::endl;
    auto rho = dbase_rho.getDatasets(vname_rho, tstep, reflevel);
    std::cout << "*** rho done " << std::endl;

    auto velx = dbase_vel.getDatasets(vname_velx, tstep, reflevel);
    std::cout << "*** velx done " << std::endl;
    auto vely = dbase_vel.getDatasets(vname_vely, tstep, reflevel);
    std::cout << "*** vely done " << std::endl;
    auto velz = dbase_vel.getDatasets(vname_velz, tstep, reflevel);
    std::cout << "*** velz done " << std::endl;

    auto gxx   = dbase_metric.getDatasets(vname_gxx, tstep, reflevel);
    std::cout << "*** gxx done " << std::endl;
    auto gyy   = dbase_metric.getDatasets(vname_gyy, tstep, reflevel);
    std::cout << "*** gyy done " << std::endl;
    auto gzz   = dbase_metric.getDatasets(vname_gzz, tstep, reflevel);
    std::cout << "*** gzz done " << std::endl;


    std::cout << "*** Read Datasets complete " << (int) tstep << std::endl;

    LorentzFactor<3> wlorentz(rho[0],true);
    wlorentz.compute_w_lorentz(gxx[0],gyy[0],gzz[0],velx[0],vely[0],velz[0]);

    GravBaryMass<3> dens(rho[0],true);
    dens.compute_bary_mass(gxx[0],gyy[0],
			       gzz[0],wlorentz,rho[0],1.0);

    /*********************** Start work on angular grid *****************************/

    std::vector<double> extent = rho[0].getMaxExtent();
    auto mxel = std::max_element(extent.begin(), extent.end(),
				 [](double x, double y){ return std::abs(x)<std::abs(y); });

    double rmax = *mxel;
    // set up radius array
    for (int i=0; i<nrad; ++i)
      radii[i] = r0 + (rmax-r0)*i/((double)nrad-1.0);
    
    // set up coordinates
    int idx=0;
    for (double mu : mus) {
      for (double phi : phis) {
	for (double radius : radii) {
	  const double x = sqrt(1.0 - mu*mu) * cos(phi) * radius;
	  const double y = sqrt(1.0 - mu*mu) * sin(phi) * radius;
	  const double z = mu*radius;
	  xarr[idx] = {x, y, z};
	  ++idx;
	}
      }
    }

    // get radial density
    auto dens_interp = dens.interpolate(xarr);
    const int n = mus.size()*phis.size()*nrad;

    std::vector<double> av_dens(nrad);

    for (int k=0;k<nrad;k++) {   
      av_dens[k] = 0.0;
    }

    for (int i=0; i < mus.size(); ++i) 
      for (int j=0; j < phis.size(); ++j) 
	for (int k=0; k < radii.size(); ++k) {
	  int idx = k + radii.size()*(j + phis.size()*i);
	  av_dens[k] += dens_interp[idx];
	}

    const double ifac = 1.0 / ((double) mus.size()*phis.size());
    for (int k=0;k<nrad;k++) {
      // convert to real units
      av_dens[k] *= ifac * inv_rho_gf;
    }

    const double rad10 = find_rad(radii,av_dens,1.0e10)*inv_length_gf;
    const double rad11 = find_rad(radii,av_dens,1.0e11)*inv_length_gf;
    const double rad12 = find_rad(radii,av_dens,1.0e12)*inv_length_gf;
    const double rad13 = find_rad(radii,av_dens,1.0e13)*inv_length_gf;

    std::cout << "rad 10: " << rad10 << std::endl;
    std::cout << "rad 11: " << rad11 << std::endl;
    std::cout << "rad 12: " << rad12 << std::endl;
    std::cout << "rad 13: " << rad13 << std::endl;

    ofile << boost::format("% 15.6E ") % rho[0].getTime();
    ofile << boost::format("% 15.6E ") % rad10;
    ofile << boost::format("% 15.6E ") % rad11;
    ofile << boost::format("% 15.6E ") % rad12;
    ofile << boost::format("% 15.6E ") % rad13;
    ofile << std::endl;
        
  }
  
  ofile.close();


  return 0;
}
