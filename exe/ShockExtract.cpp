#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

double detect_shock(const std::vector<double>& radii, 
  const std::vector<double>& entropy, double entropy_max) {
  for (int i=radii.size()-1; i > -1; --i)
    if (entropy[i] > entropy_max) return i; 
}

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename,N", po::value<std::string>()->default_value("hydrobase::entropy.*\.h5"), "Regular expression describing the base name")
      ("outfile,O", po::value<std::string>()->default_value("Shock.out"), "Output filename")
      ("modeoutfile,M", po::value<std::string>()->default_value("Shock-lm.out"), "Individual Mode Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("startit,I", po::value<int>()->default_value(-1), "Start iteration")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 
  int startit = vm["startit"].as<int>(); 
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase(vm["basepath"].as<std::string>(), 
      vm["basename"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);
   
  std::cout << "Starting shock extraction..." << std::endl;
  
  // Set up the angular grid and build spherical harmonics
  std::vector<double> phis, mus;
  int nmu = 100;
  int nphi = 200;
  double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);
  int lmax = 15;
  int lmax_fullout = 4;
  std::vector<std::vector<RealSphericalHarmonic>> Ylm(lmax+1); 
  for (int l=0; l<=lmax; ++l) {
    for (int m=-l; m<=l; ++m) {
      Ylm[l].push_back(RealSphericalHarmonic(l,m,mus,phis));
    }
  }

  
  std::string vname;  
  for (auto name : dbase.getVariables()) vname = name; 
  
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[1] Time (Msun) " << std::endl; 
  ofile << "#[2] Radius (Msun) " << std::endl; 
  ofile << "#[3] Min Radius (Msun) " << std::endl; 
  ofile << "#[4] Max Radius (Msun) " << std::endl; 
  ofile << "#[5] Sigma Radius (Msun) " << std::endl; 
  for (int l=0; l<=lmax; ++l) {
    ofile << "#[" << l+6 << "] A" << l << " (Msun)" << std::endl; 
  }

  std::ofstream mofile(vm["modeoutfile"].as<std::string>(), std::ofstream::out);  
  mofile << "#[1] Time (Msun) " << std::endl; 
  int count = 2;
  for (int l=0; l<=lmax_fullout; ++l) {
    for(int m=-l;m<=l; ++m) {
      mofile << "#[" << count++ << "] a" << l << "," << m << " (Msun)" << std::endl; 
    }
  }

  std::string frmt;
  for (int ii=0; ii<5+lmax+1; ++ii) frmt.append("% 12.5e "); 
  
  int nrad = 600; 
  double r0 = 30.0;
  double rmax = 30.0;
  std::vector<double> radii(nrad);
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size()*nrad);
   
  // Iterate over timesteps 
  for (auto tstep : dbase.getTsteps()) {

    if (tstep < startit) continue;
    
    std::cout << "Working on timestep " << tstep << std::endl;
    
    auto darr = dbase.getDatasets(vname, tstep, reflevel); 
    std::vector<std::vector<double>> 
        shock_sphere(mus.size(),std::vector<double>(phis.size()));
     
    for (auto& arr : darr) {
  
      // Build radial grid that goes to the maximum radius
      std::vector<double> extent = arr.getMaxExtent();
      auto mxel = std::max_element(extent.begin(), extent.end(),
         [](double x, double y){ return std::abs(x)<std::abs(y); });
      if (*mxel > rmax) {
        double rmax = *mxel;
        for (int i=0; i<nrad; ++i) 
            radii[i] = r0 + (rmax-r0)*i/((double)nrad-1.0);
        
        int idx=0;
        for (double mu : mus) {
          for (double phi : phis) { 
            for (double radius : radii) {
              double x = sqrt(1.0 - mu*mu) * cos(phi) * radius; 
              double y = sqrt(1.0 - mu*mu) * sin(phi) * radius; 
              double z = mu*radius; 
              xarr[idx] = {x, y, z};
              ++idx;
            }
          }
        }
      } 
       
      auto data = arr.interpolate(xarr);
      double shock_rad = 0.0;
      double shock_sq = 0.0;
      double shock_max = 0.0;
      double shock_min = 1.e10;
      for (int i=0; i < mus.size(); ++i) {
        for (int j=0; j < phis.size(); ++j) { 
          double shock_c;
          double sedge = data[radii.size()*(1 + j + phis.size()*i) - 1];
          for (int k=radii.size()-1; k>-1; --k) {
            int idx = k + radii.size()*(j + phis.size()*i);
            if (data[idx] > fmax(4.0, 1.1*sedge)) {
              shock_c = radii[k];
              break;
            }
          }
          shock_sphere[i][j] = shock_c;
          shock_rad += shock_c;
          shock_sq  += shock_c*shock_c;
          shock_max  = fmax(shock_max, shock_c);
          shock_min  = fmin(shock_min, shock_c);
        }
      }
      
      std::vector<std::vector<double>> alm(lmax+1);
      std::vector<double> Al(lmax+1); 
      for (int l=0; l<=lmax; ++l) {
        Al[l] = 0.0;
        for (int m=-l; m<=l; ++m) { 
          double y = Ylm[l][m+l].Integrate(shock_sphere);
          Al[l] += y*y;
          alm[l].push_back(y);
        }
        Al[l] = sqrt(Al[l]);
      } 
      
      shock_rad /= (double) mus.size()*phis.size();
      shock_sq /= (double) mus.size()*phis.size();
      shock_sq = sqrt(shock_sq - shock_rad*shock_rad);
      
      ofile << boost::format("% 12.5e ") % arr.getTime(); 
      ofile << boost::format("% 12.5e ") % shock_rad; 
      ofile << boost::format("% 12.5e ") % shock_min; 
      ofile << boost::format("% 12.5e ") % shock_max; 
      ofile << boost::format("% 12.5e ") % shock_sq; 
      for (int l=0; l<=lmax; ++l)  
        ofile << boost::format("% 12.5e ") % Al[l];
      ofile << std::endl; 

      mofile << boost::format("%15.6E ") % arr.getTime();
      for (int l=0; l<=lmax_fullout; ++l) {
	for(int m=0;m < (2*l + 1); ++m) {
	  mofile << boost::format("%15.6E ") % alm[l][m]; 
	}
      }
      mofile << std::endl;


       
    }       
  } 
  ofile.close(); 
  mofile.close();

  return 0;
}  
