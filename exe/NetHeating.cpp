#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename,N", po::value<std::string>()->default_value("zelmanim1::heatcoolanalysis.*\.h5"), "Regular expression describing the base name")
      ("outfile,O", po::value<std::string>()->default_value("NetHeating.out"), "Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase(vm["basepath"].as<std::string>(), 
      vm["basename"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);
   
  std::cout << "Starting heating rate extraction..." << std::endl;
  
  // Set up the angular grid and build spherical harmonics
  
  std::string vname = "ZELMANIM1::netheat";  
  //for (auto name : dbase.getVariables()) vname = name; 
   
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[1] Time (Msun) " << std::endl; 
  ofile << "#[2] Net Heating Rate (25) " << std::endl; 
  ofile << "#[3] Net Heating Rate (50) " << std::endl; 
  ofile << "#[3] Net Heating Rate (75) " << std::endl; 
  ofile << "#[4] Net Heating Rate (Full)" << std::endl; 
  
  // Iterate over timesteps 
  for (auto tstep : dbase.getTsteps()) {
    auto darr = dbase.getDatasets(vname, tstep, reflevel); 
    double netheat25 = darr[0].maskedSum(25.0, 400.0); 
    double netheat50 = darr[0].maskedSum(50.0, 400.0); 
    double netheat75 = darr[0].maskedSum(75.0, 400.0); 
    double netheat_all = darr[0].maskedSum(0.0, 100000.0); 
    
    std::cout << darr[0].getTime() << " " << netheat50 << " " << netheat_all << std::endl; 
    ofile << boost::format("% 12.5e ") % darr[0].getTime(); 
    ofile << boost::format("% 12.5e ") % netheat25; 
    ofile << boost::format("% 12.5e ") % netheat50; 
    ofile << boost::format("% 12.5e ") % netheat75; 
    ofile << boost::format("% 12.5e ") % netheat_all; 
    ofile << std::endl; 
     
  } 
  ofile.close(); 
   
  return 0;
}  
