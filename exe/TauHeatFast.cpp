#include <iostream>
#include <fstream>
#include <ostream>
#include <math.h>
#include <vector>
#include <memory>
#include <stdio.h>
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp"
#include "CactusDatabase.hpp"
#include "RealSphericalHarmonic.hpp"
#include "GainRegionAnalysis.hpp"
#include "DiagnosticEnergy.hpp"
#include "GravBaryMass.hpp"
#include <H5Cpp.h>

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {

  // constants
  const double clite = 2.99792458e10;
  const double msun  =  1.9884e33;
  const double inv_time_gf = 4.92513293223396E-06;
  const double time_gf = 1.0/inv_time_gf;

  // Set up the cl interface
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
    ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
    ("basename-entropy,NE", po::value<std::string>()->default_value("hydrobase-entropy.*\.h5"), "Regular expression describing the base name for entropy")
    ("basename-rho,NR", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for rho")
    ("basename-temp,NR", po::value<std::string>()->default_value("hydrobase-temperature.*\.h5"), "Regular expression describing the base name for temperature")
    ("basename-ye,NY", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for Y_e")
    ("basename-vel,NV", po::value<std::string>()->default_value("hydrobase-vel.*\.h5"), "Regular expression describing the base name velocity")
    ("basename-lapse,NL", po::value<std::string>()->default_value("admbase-lapse.*\.h5"), "Regular expression describing the base name lapse")
    ("basename-metric,NM", po::value<std::string>()->default_value("admbase-metric.*\.h5"), "Regular expression describing the base name metric")
    ("basename-heatcool,NH", po::value<std::string>()->default_value("zelmanim1-heatcoolanalysis.*\.h5"), "Regular expression describing the base name heatcool data")
    ("outfile,O", po::value<std::string>()->default_value("TauHeatFast.out"), "Output filename")
    ("octant", "Assume the data posesses reflecting octant symmetry")
    ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level")
    ("skip-iteration,S", po::value<std::vector<int> >()->multitoken(), "Skip this iteration [can be used multiple times]");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  int reflevel = vm["reflevel"].as<int>();

  // by specifying -S <iteration> -S <iteration> -S <iteration> and so forth,
  // one can skip iterations one doesn't want to analyze. That's useful if
  // some data are missing.
  std::vector<int> skipits(0);
  if(vm.count("skip-iteration")) {
    skipits = vm["skip-iteration"].as<std::vector<int>>();
  }

  //Build the database
  std::vector<bool> symmetry(3, false);
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true);

  CactusDatabase<3> dbase_entropy(vm["basepath"].as<std::string>(),
			  vm["basename-entropy"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(),
			  vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_temp(vm["basepath"].as<std::string>(),
			       vm["basename-temp"].as<std::string>(), vm["directory"].as<std::string>(),
			       symmetry);

  CactusDatabase<3> dbase_ye(vm["basepath"].as<std::string>(),
			  vm["basename-ye"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_vel(vm["basepath"].as<std::string>(),
			  vm["basename-vel"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_lapse(vm["basepath"].as<std::string>(),
			  vm["basename-lapse"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_metric(vm["basepath"].as<std::string>(),
			  vm["basename-metric"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_heatcool(vm["basepath"].as<std::string>(),
			  vm["basename-heatcool"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "WARNING! Some constants are hardcoded! Read the code!!!" << std::endl;
  std::cout << "Path to EOS table is hardcoded!!!                      " << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "Done reading data structures!" << std::endl;

  // first read the EOS table, table is currently hardcoded for simplicity
  EOSDriver::ReadTable("/work/00386/ux455321/stampede2/tables/SFHo.h5");

  std::string vname_rho   = "HYDROBASE::rho";
  std::string vname_entropy  = "HYDROBASE::entropy";
  std::string vname_temp  = "HYDROBASE::temperature";
  std::string vname_ye    = "HYDROBASE::Y_e";
  std::string vname_velx  = "HYDROBASE::vel[0]";
  std::string vname_vely  = "HYDROBASE::vel[1]";
  std::string vname_velz  = "HYDROBASE::vel[2]";
  std::string vname_lapse = "ADMBASE::alp";
  std::string vname_gxx   = "ADMBASE::gxx";
  std::string vname_gyy   = "ADMBASE::gyy";
  std::string vname_gzz   = "ADMBASE::gzz";
  std::string vname_heat  = "ZELMANIM1::netheat";

  // change this!!!
  const double r0 = 25.0;

  // open output file and write header
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);
  ofile << "#[1] Time (Msun) " << std::endl;
  ofile << "#[2] Qheat Net Heating (masked: r>25M,inside shock) [erg/s]" << std::endl;
  ofile << "#[3] Egain (masked: r>25, inside gain & shock) [erg]" << std::endl;
  ofile << "#[4] Ediag (masked: r>25, >0, in shock) [erg]" << std::endl;
  ofile << "#[5] tau_heat = |Egain|/Qheat [ms]" << std::endl;

  // Iterate over timesteps
  //  std::vector<double> tsteps = {400384,401408,402432,403456,404480};  
  //int tstep;
  
  for (auto tstep : dbase_rho.getTsteps()) {  
  //for(int ii=0;ii<tsteps.size();++ii) {
  //tstep = tsteps[ii];

    // check if we need to skip the iteration
    bool found = false;
    for(std::vector<int>::const_iterator it = skipits.begin(); it < skipits.end(); ++it) {
      if( tstep == * it ) {
	std::cout << "Skipping iteration " << *it << " !" << std::endl;
	found = true;
      }
    }
    if (found) continue;

    std::cout << "*** Working on iteration " << (int) tstep << std::endl;
    auto rho = dbase_rho.getDatasets(vname_rho, tstep, reflevel);
    std::cout << "*** rho done " << std::endl;
    auto entropy = dbase_entropy.getDatasets(vname_entropy, tstep, reflevel);
    std::cout << "*** entropy done " << std::endl;

    auto temp = dbase_temp.getDatasets(vname_temp, tstep, reflevel);
    std::cout << "*** temperature done " << std::endl;
    auto ye = dbase_ye.getDatasets(vname_ye, tstep, reflevel);
    std::cout << "*** ye done " << std::endl;

    auto velx = dbase_vel.getDatasets(vname_velx, tstep, reflevel);
    std::cout << "*** velx done " << std::endl;
    auto vely = dbase_vel.getDatasets(vname_vely, tstep, reflevel);
    std::cout << "*** vely done " << std::endl;
    auto velz = dbase_vel.getDatasets(vname_velz, tstep, reflevel);
    std::cout << "*** velz done " << std::endl;

    auto lapse = dbase_lapse.getDatasets(vname_lapse, tstep, reflevel);
    std::cout << "*** lapse done " << std::endl;
    auto gxx   = dbase_metric.getDatasets(vname_gxx, tstep, reflevel);
    std::cout << "*** gxx done " << std::endl;

    auto gyy   = dbase_metric.getDatasets(vname_gyy, tstep, reflevel);
    std::cout << "*** gyy done " << std::endl;
    auto gzz   = dbase_metric.getDatasets(vname_gzz, tstep, reflevel);
    std::cout << "*** gzz done " << std::endl;

    auto netheat  = dbase_heatcool.getDatasets(vname_heat, tstep, reflevel);
    std::cout << "*** netheat done " << std::endl;

    std::cout << "*** Read Datasets complete " << (int) tstep << std::endl;

    LorentzFactor<3> wlorentz(rho[0],true); // by default set to 1
    wlorentz.compute_w_lorentz(gxx[0],gyy[0],gzz[0],velx[0],vely[0],velz[0]);

    // mask that marks the region inside the shock, including the PNS.
    // the way this is works that it uses an entropy criterion and if
    // the entropy is higher than that value (4 by default), then the
    // mask is set to 1. It's also by default set to one inside r0.
    DoubleMask<3> shock_mask(rho[0],true);
    shock_mask.set_shock_mask(entropy[0],r0,4.0);

    // mask that makrs the gain region, excluding the PNS (r0). Inside
    // r0 it is set to 0.
    DoubleMask<3> shock_heating_mask(rho[0],true);
    shock_heating_mask.set_shock_and_heating_mask(entropy[0],netheat[0],r0,0.0,4.0,0.0);
    
    // grid spacing dx on the current refinement level
    auto mydelta = rho[0].getDelta();
    
    // NOTE THAT NETGAIN IS ALREADY MULTIPLIED BY ***COARSE GRID*** dx*dy*dz in ZelmaniM1!!!
    // Here we must correct for this
    double volume_factor = 1.0 /  pow(2,(reflevel)*3);

    // we need to multiply the netgain with the sqrt(detg) in order to get the
    // integral right
    Densitized<3> densitized_netgain(netheat[0],false,gxx[0],gyy[0],gzz[0]);
    double dens_intnetgain = densitized_netgain.maskedSumMultMask(shock_mask,r0,100000.0);
    dens_intnetgain *= msun * clite * clite * time_gf * volume_factor;

    std::cout << "masked integrated densitized net heating " << dens_intnetgain << std::endl;
    
    // *********************** Energy in the gain region
    // using Eqs. 6 and 2. of Mueller et al. 2012a

    DiagnosticEnergy<3> diag_energy(rho[0],true);
    diag_energy.compute_diag_enr(lapse[0],rho[0],temp[0],ye[0],
				 wlorentz,gxx[0],gyy[0],gzz[0]);

    DiagnosticEnergy<3> gain_energy(rho[0],true);
    gain_energy.compute_egain(lapse[0],rho[0],temp[0],ye[0],
			      wlorentz,gxx[0],gyy[0],gzz[0]);


    double Egain = gain_energy.maskedSumMultMask(shock_heating_mask,r0,100000.0);
    Egain *= msun * clite * clite *  pow(mydelta[0],3);

    double Ediag = diag_energy.maskedSumMultMaskPos(shock_mask,r0,100000.0);
    Ediag *= msun * clite * clite *  pow(mydelta[0],3);

    std::cout << "Egain:  " << Egain << std::endl;
    std::cout << "Diagnostic:  " << Ediag << std::endl;

    const double tau_heat = std::abs(Egain) / dens_intnetgain * 1000;
    std::cout << "tau_heat = Egain/Qheat: " << tau_heat << std::endl;

    ofile << boost::format("% 15.6E ") % rho[0].getTime();
    ofile << boost::format("% 15.6E ") % dens_intnetgain;
    ofile << boost::format("% 15.6E ") % Egain;
    ofile << boost::format("% 15.6E ") % Ediag;
    ofile << boost::format("% 15.6E ") % tau_heat;
    ofile << std::endl;

  }
  
  ofile.close();


  return 0;
}
