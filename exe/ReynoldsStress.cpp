#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>
#include <string> 

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include "EOSDriver/EOSDriver.hpp" 
#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("outfile,O", po::value<std::string>()->default_value("Reynolds."), "Output filename")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 
     
  EOSDriver::ReadTable("LS220_234r_136t_50y_analmu_20091212_SVNr26.h5"); 
  
  //Build the database 
  CactusDatabase<3> vel_dbase(vm["basepath"].as<std::string>(), 
      "hydrobase::vel.xyz.*\.h5", vm["directory"].as<std::string>());
   
  CactusDatabase<3> rho_dbase(vm["basepath"].as<std::string>(), 
      "hydrobase::rho.*\.h5", vm["directory"].as<std::string>());
  
  CactusDatabase<3> entropy_dbase(vm["basepath"].as<std::string>(), 
      "hydrobase::entropy.*\.h5", vm["directory"].as<std::string>());
  
  CactusDatabase<3> ye_dbase(vm["basepath"].as<std::string>(), 
      "hydrobase::y_e.*\.h5", vm["directory"].as<std::string>());
  
  std::cout << "Starting Reynolds extraction..." << std::endl;
  
  // Set up the spherical grid
  std::vector<double> phis, mus;
  int nmu = 200;
  int nphi = 400;
  double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);
  
  int nrad = 600; 
  double r0 = 1.0;
  double rmax = 0.0;
  std::vector<double> radii(nrad);
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size()*nrad);
  
  // Build spherical harmonics
  int lmax = 20;
  std::vector<std::vector<RealSphericalHarmonic>> Ylm(lmax+1); 
  for (int l=0; l<=lmax; ++l) {
    for (int m=-l; m<=l; ++m) {
      Ylm[l].push_back(RealSphericalHarmonic(l,m,mus,phis));
    }
  }
  
  std::string vname = "ZELMANIM1::netheat";  
  //for (auto name : dbase.getVariables()) vname = name; 
   
  // Iterate over timesteps 
  for (auto tstep : rho_dbase.getTsteps()) {
    
    auto rho_data = rho_dbase.getDatasets("HYDROBASE::rho", tstep, reflevel); 
    auto ye_data = ye_dbase.getDatasets("HYDROBASE::Y_e", tstep, reflevel); 
    auto entropy_data = entropy_dbase.getDatasets("HYDROBASE::entropy", tstep, reflevel); 
    // note: Christian changed vel vector ordering to 0-1-2 from 2-1-0 to
    // account for the changes in array ordering on NDarray.hpp
    auto velx_data = vel_dbase.getDatasets("HYDROBASE::vel[0]", tstep, reflevel); 
    auto vely_data = vel_dbase.getDatasets("HYDROBASE::vel[1]", tstep, reflevel); 
    auto velz_data = vel_dbase.getDatasets("HYDROBASE::vel[2]", tstep, reflevel); 
    if (rho_data.size() == 0 || velx_data.size()==0 || vely_data.size() == 0 ||
        velz_data.size() == 0) continue;
      
    // Build radial grid that goes to the maximum radius
    std::vector<double> extent = rho_data[0].getMaxExtent();
    auto mxel = std::max_element(extent.begin(), extent.end(),
       [](double x, double y){ return std::abs(x)<std::abs(y); });
    if (*mxel > rmax) {
      double rmax = *mxel;
      for (int i=0; i<nrad; ++i) 
          radii[i] = r0 + (rmax-r0)*i/((double)nrad-1.0);
      
      int idx=0;
      for (int i=0; i < mus.size(); ++i) {
        for (int j=0; j < phis.size(); ++j) { 
          for (int k=0; k<radii.size(); ++k) {
            int idx = k + radii.size()*(j + phis.size()*i);
            double mu = mus[i];
            double phi = phis[j];
            double radius = radii[k];
            double x = sqrt(1.0 - mu*mu) * cos(phi) * radius; 
            double y = sqrt(1.0 - mu*mu) * sin(phi) * radius; 
            double z = mu*radius; 
            xarr[idx] = {x, y, z};
          }
        }
      }
    } 
         
    std::ofstream ofile(vm["outfile"].as<std::string>() + std::to_string(tstep) 
        + ".out", std::ofstream::out);  
  
    ofile << "# Time: " << rho_data[0].getTime() << " (Msun)" << std::endl; 
    ofile << "#[1] Radius (Msun) " << std::endl; 
    ofile << "#[2] Density (code units) " << std::endl; 
    ofile << "#[3] <vr> (code units) " << std::endl; 
    ofile << "#[4] Rrr " << std::endl; 
    ofile << "#[5] Rtt " << std::endl; 
    ofile << "#[6] Rpp " << std::endl; 
    ofile << "#[7] Rrr (Murphy version) " << std::endl; 
    ofile << "#[8] Rtt (Murphy version) " << std::endl; 
    ofile << "#[9] Rpp (Murphy version) " << std::endl; 
    ofile << "#[10] FK*r^2 (Code units)" << std::endl; 
    ofile << "#[11] <rho vr> (Code units)" << std::endl; 
    ofile << "#[12] <P> (erg/cm^3)" << std::endl; 
    ofile << "#[13] <v'P'> (erg/cm^3)" << std::endl; 
    ofile << "#[14] dvr/dr (Code units)" << std::endl; 
    
    std::cout << tstep << " " << rho_data.size() << " " << velx_data.size() << std::endl;
    
    // Interpolate the data and transform to spherical coordinate system
    auto rho  = rho_data[0].interpolate(xarr);
    auto entropy  = entropy_data[0].interpolate(xarr);
    auto ye  = ye_data[0].interpolate(xarr);
     
    std::vector<double> velr(rho.size());
    std::vector<double> velt(rho.size());
    std::vector<double> velp(rho.size());
    std::vector<double> pres(rho.size());
    std::vector<std::vector<int>> 
        max_rad(mus.size(),std::vector<int>(phis.size()));
    double rshock_max = 0.0;
    double rshock_min = 1.e50;
    {
      auto velx = velx_data[0].interpolate(xarr);
      auto vely = vely_data[0].interpolate(xarr);
      auto velz = velz_data[0].interpolate(xarr);
      for (int i=0; i < mus.size(); ++i) {
        for (int j=0; j < phis.size(); ++j) { 
          for (int k=0; k<radii.size(); ++k) {
            int idx = k + radii.size()*(j + phis.size()*i);
            double st = sqrt(1.0 - mus[i]*mus[i]); 
            double ct = mus[i];
            double sp = sin(phis[j]);
            double cp = cos(phis[j]);
            velr[idx] = st*cp*velx[idx] + st*sp*vely[idx] + ct*velz[idx]; 
            velt[idx] = ct*cp*velx[idx] + ct*sp*vely[idx] - st*velz[idx]; 
            velp[idx] = -sp*velx[idx]   + cp*vely[idx]; 
            pres[idx] = EOSDriver::GetPressureFromEntropy(
                rho[idx]*6.18735016707159e17, ye[idx], entropy[idx]); 
          }
        }    
      }
    
       
     
      // Find the shock radius 
      for (int i=0; i < mus.size(); ++i) {
        for (int j=0; j < phis.size(); ++j) { 
          int idx = radii.size()*(j + phis.size()*i);
          // Find the minimum velocity  
          auto mm = std::min_element(velr.begin() + idx, 
              velr.begin() + idx + radii.size());
          max_rad[i][j] = std::min(std::max(
            (int) std::distance(velr.begin() + idx, mm) - 15, 0), (int) radii.size()-1);
          rshock_max = std::max(rshock_max, radii[max_rad[i][j]]);
          rshock_min = std::min(rshock_min, radii[max_rad[i][j]]);
        }
      }
      
      // Calculate the kinetic energy spectrum
      std::vector<std::vector<double>> 
          epsk(mus.size(),std::vector<double>(phis.size()));
      for (int i=0; i < mus.size(); ++i) {
        double st = sqrt(1.0 - mus[i]*mus[i]); 
        double ct = mus[i];
        for (int j=0; j < phis.size(); ++j) { 
          double sp = sin(phis[j]);
          double cp = cos(phis[j]);
          epsk[i][j] = 0.0;
          for (int k=0; k<radii.size(); ++k) {
            if (radii[k]<0.7 * rshock_min || radii[k] > 0.85 * rshock_min) continue;
            int idx = k + radii.size()*(j + phis.size()*i);
            velr[idx] = st*cp*velx[idx] + st*sp*vely[idx] + ct*velz[idx]; 
            velt[idx] = ct*cp*velx[idx] + ct*sp*vely[idx] - st*velz[idx]; 
            velp[idx] = -sp*velx[idx]   + cp*vely[idx]; 
            epsk[i][j] += radii[k]*radii[k] 
                * sqrt(rho[idx]*(velt[idx]*velt[idx] + velp[idx]*velp[idx]));
          }
        }    
      }
      std::ofstream kfile("KSpectrum" + std::to_string(tstep) 
          + ".out", std::ofstream::out);  
      for (int l=0; l<=lmax; ++l) { 
        double Al = 0.0;
        for (int m=-l; m<=l; ++m) { 
          double y = Ylm[l][m+l].Integrate(epsk);
          Al += y*y; 
        }
        kfile << l << " " << Al << std::endl;  
      }
      kfile.close(); 
    }
     
    ofile << "# Minimum shock radius: " << rshock_min << " (code units)" << std::endl; 
    ofile << "# Maximum shock radius: " << rshock_max << " (code units)" << std::endl; 

    for (int k=0; k<radii.size(); ++k) {
      double tFK = 0.0; 
      double tRrr = 0.0, tRrt = 0.0, tRrp = 0.0;
      double tRtt = 0.0, tRtp = 0.0, tRpp = 0.0;
      double tRr = 0.0, tRt = 0.0, tRp = 0.0;
      double avgrho = 1.e-100, avgvt = 0.0, avgvp = 0.0; 
      double norm = 1.e-100;
      double avgp = 0.0, tFp = 0.0; 
      double dvrdr = 0.0; 
      double avgvr = 0.0; 
      double avgvrup = 0.0; 
      double avgvrlo = 0.0; 
      double norm_deriv = 0.0;          
      double norm_lo = 0.0;
      double norm_up = 0.0;

      for (int i=0; i < mus.size(); ++i) {
        for (int j=0; j < phis.size(); ++j) {
          // max_rad[i][j] = radii.size()-1;
          // Average over a small region in radius
          for (int l=std::max(0, k-10); l<=std::min((int)max_rad[i][j]+2, k+10); ++l) { 
            int idx = l + radii.size()*(j + phis.size()*i);
            
            double d = rho[idx]; 
            double p = pres[idx]; 
            double vr = velr[idx]; 
            double vt = velt[idx]; 
            double vp = velp[idx]; 
            
            if (k > max_rad[i][j] + 1) continue; // Don't include in average if outside shock
            int looff = -1;
            if (l==0) looff = 0;
            avgvrlo += velr[idx+looff]; 
            norm_lo += 1.0; 

            if (k > max_rad[i][j]) continue; // Don't include in average if outside shock
            tFK += d*vr*(vr*vr + vt*vt + vp*vp);  
            tFp += p*vr; 
            avgp += p; 
             
            tRrr += d*vr*vr; 
            tRrt += d*vr*vt; 
            tRrp += d*vr*vp; 
            tRtt += d*vt*vt; 
            tRtp += d*vt*vp; 
            tRpp += d*vp*vp; 
             
            tRr += d*vr;
            tRt += d*vt;
            tRp += d*vp;
            int hioff = 1;
            if (l==max_rad[i][j]+4) hioff = 0;
            if (k < max_rad[i][j]) {  
              dvrdr += (velr[idx+hioff] - velr[idx+looff])
                  /(radii[l+hioff] - radii[l+looff]);
              avgvrup += velr[idx+hioff]; 
              norm_up += 1.0;
            }
            avgrho += d;
            avgvr += vr;
            avgvt += vt;
            avgvp += vp;
            norm += 1.0;
          }
        }
      }
      avgrho /= norm; 
      avgvr /= norm; 
      avgvt /= norm; 
      avgvp /= norm;
      avgp /= norm;
      dvrdr /= norm_up;
      avgvrlo /= norm_lo;
      avgvrup /= norm_up;
      tFp /=norm; 
      
      dvrdr = (avgvrup - avgvrlo)/(radii[2] - radii[0]);
       
      tRrr /= norm*avgrho;
      tRrt /= norm*avgrho;
      tRrp /= norm*avgrho;
      tRtt /= norm*avgrho;
      tRtp /= norm*avgrho;
      tRpp /= norm*avgrho;
       
      tRr  /= norm*avgrho; 
      tRt  /= norm*avgrho;  
      tRp  /= norm*avgrho; 
      
      double Rrr = tRrr + avgvr*avgvr - 2.0*avgvr*tRr;
      double Rtt = tRtt + avgvt*avgvt - 2.0*avgvt*tRt;
      double Rpp = tRpp + avgvp*avgvp - 2.0*avgvp*tRp;
      double Rr = tRr - avgvr;

      double FK = tFK/norm - avgrho*avgvr*(3.0*Rrr + Rtt + Rpp) 
          - 3.0*avgvr*avgvr*avgrho*Rr - avgrho*pow(avgvr,3); 

      ofile << boost::format("% 12.5e ") % radii[k]; 
      ofile << boost::format("% 12.5e ") % avgrho; 
      ofile << boost::format("% 12.5e ") % avgvr; 
      ofile << boost::format("% 12.5e ") % Rrr; 
      ofile << boost::format("% 12.5e ") % Rtt; 
      ofile << boost::format("% 12.5e ") % Rpp; 
      ofile << boost::format("% 12.5e ") % (tRrr - tRr*avgvr); 
      ofile << boost::format("% 12.5e ") % (tRtt - tRt*avgvt); 
      ofile << boost::format("% 12.5e ") % (tRpp - tRp*avgvp); 
      ofile << boost::format("% 12.5e ") % (FK*radii[k]*radii[k]);
      if (radii[k] >= 15.0) { 
        ofile << boost::format("% 12.5e ") % (tRr*avgrho - avgvr*avgrho); 
      } else {
        // Get lots of noise here, makes plotting harder 
        ofile << boost::format("% 12.5e ") % 0.0; 
      }
      ofile << boost::format("% 12.5e ") % avgp;
      ofile << boost::format("% 12.5e ") % (tFp-avgvr*avgp);
      ofile << boost::format("% 12.5e ") % dvrdr;
      ofile << boost::format("% 12.5e ") % avgvrlo;
      ofile << boost::format("% 12.5e ") % avgvrup;
      ofile << std::endl; 
    }
     
    ofile.close(); 
  } 
   
  return 0;
}  
