#include <iostream>
#include <fstream>
#include <ostream>
#include <math.h>
#include <vector>
#include <memory>
#include <stdio.h>
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp"
#include "CactusDatabase.hpp"
#include "RealSphericalHarmonic.hpp"
#include "GainRegionAnalysis.hpp"
#include "DiagnosticEnergy.hpp"
#include "GravBaryMass.hpp"
#include "epsThermal/epsThermal.hpp"
#include <H5Cpp.h>

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {

  // constants
  const double clite = 2.99792458e10;
  const double msun  =  1.9884e33;
  const double inv_time_gf = 4.92513293223396E-06;
  const double time_gf = 1.0/inv_time_gf;

  // Set up the cl interface
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
    ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
    ("basename-entropy,NE", po::value<std::string>()->default_value("hydrobase-entropy.*\.h5"), "Regular expression describing the base name for entropy")
    ("basename-rho,NR", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for rho")
    ("basename-temp,NR", po::value<std::string>()->default_value("hydrobase-temperature.*\.h5"), "Regular expression describing the base name for temperature")
    ("basename-ye,NY", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for Y_e")
    ("basename-vel,NV", po::value<std::string>()->default_value("hydrobase-vel.*\.h5"), "Regular expression describing the base name velocity")
    ("basename-lapse,NL", po::value<std::string>()->default_value("admbase-lapse.*\.h5"), "Regular expression describing the base name lapse")
    ("basename-metric,NM", po::value<std::string>()->default_value("admbase-metric.*\.h5"), "Regular expression describing the base name metric")
    ("basename-heatcool,NH", po::value<std::string>()->default_value("zelmanim1-heatcoolanalysis.*\.h5"), "Regular expression describing the base name heatcool data")
    ("outfile,O", po::value<std::string>()->default_value("Timescales_Spherical.out"), "Output filename")
    ("octant", "Assume the data posesses reflecting octant symmetry")
    ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level")
    ("startit,I", po::value<int>()->default_value(-1), "Start at iteration greater than")
    ("skip-iteration,S", po::value<std::vector<int> >()->multitoken(), "Skip this iteration [can be used multiple times]");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  int reflevel = vm["reflevel"].as<int>();

  int startit = vm["startit"].as<int>();

  // by specifying -S <iteration> -S <iteration> -S <iteration> and so forth,
  // one can skip iterations one doesn't want to analyze. That's useful if
  // some data are missing.
  std::vector<int> skipits(0);
  if(vm.count("skip-iteration")) {
    skipits = vm["skip-iteration"].as<std::vector<int>>();
  }

  //Build the database
  std::vector<bool> symmetry(3, false);
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true);

  CactusDatabase<3> dbase_entropy(vm["basepath"].as<std::string>(),
			  vm["basename-entropy"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(),
			  vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_temp(vm["basepath"].as<std::string>(),
			       vm["basename-temp"].as<std::string>(), vm["directory"].as<std::string>(),
			       symmetry);

  CactusDatabase<3> dbase_ye(vm["basepath"].as<std::string>(),
			  vm["basename-ye"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_vel(vm["basepath"].as<std::string>(),
			  vm["basename-vel"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_lapse(vm["basepath"].as<std::string>(),
			  vm["basename-lapse"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_metric(vm["basepath"].as<std::string>(),
			  vm["basename-metric"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_heatcool(vm["basepath"].as<std::string>(),
			  vm["basename-heatcool"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "WARNING! Some constants are hardcoded! Read the code!!!" << std::endl;
  std::cout << "Path to EOS table is hardcoded!!!                      " << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "Done reading data structures!" << std::endl;

  // first read the EOS table, table is currently hardcoded for simplicity
  EOSDriver::ReadTable("/work/00386/ux455321/stampede2/tables/SFHo.h5");

  std::string vname_rho   = "HYDROBASE::rho";
  std::string vname_entropy  = "HYDROBASE::entropy";
  std::string vname_temp  = "HYDROBASE::temperature";
  std::string vname_ye    = "HYDROBASE::Y_e";
  std::string vname_velx  = "HYDROBASE::vel[0]";
  std::string vname_vely  = "HYDROBASE::vel[1]";
  std::string vname_velz  = "HYDROBASE::vel[2]";
  std::string vname_lapse = "ADMBASE::alp";
  std::string vname_gxx   = "ADMBASE::gxx";
  std::string vname_gyy   = "ADMBASE::gyy";
  std::string vname_gzz   = "ADMBASE::gzz";
  std::string vname_heatcool  = "ZELMANIM1::heatcool";

  // Set up the spherical grid                                                                                                                     
  std::vector<double> phis, mus;
  const int nmu = 100;
  const int nphi = 200;
  const double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);

  const int nrad = 600;
  std::vector<double> radii(nrad);

  // change this!!!
  const double r0 = 25.0;

  // coordinates
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size()*nrad);
  // integer shock mask
  std::vector<int> mshock(mus.size()*phis.size()*nrad);

  // shock radius as a function of angle
  std::vector<std::vector<double>>
    shock_sphere(mus.size(),std::vector<double>(phis.size()));

  // open output file and write header
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);
  ofile << "#[1] Time (Msun) " << std::endl;
  ofile << "#[2] Baryonic Mass in Gain Region [Msun]" << std::endl;
  //  ofile << "#[3] Bound Baryonic Mass inside shock [Msun]" << std::endl;
  ofile << "#[3] Qheat Net Heating (masked: r>25M,inside shock) [erg/s]" << std::endl;
  ofile << "#[4] Egain (masked: r>25, inside gain & shock) [erg]" << std::endl;
  ofile << "#[5] Accretion rate through shock [Msun/s]" << std::endl;
  ofile << "#[6] tau_adv = Mgain/Mdot [ms]" << std::endl;
  ofile << "#[7] tau_heat = |Egain|/Qheat [ms]" << std::endl;

  // Iterate over timesteps
  // std::vector<double> tsteps = {400384,401408,402432,403456,404480};


  
  for (auto tstep : dbase_rho.getTsteps()) {  
    //int lvc = 0;
    //while (lvc < tsteps.size()) {
    //const int tstep = tsteps[lvc];
    //lvc++;

    // check if we need to skip the iteration
    bool found = false;
    for(std::vector<int>::const_iterator it = skipits.begin(); it < skipits.end(); ++it) {
      if( tstep == * it ) {
	std::cout << "Skipping iteration " << *it << " !" << std::endl;
	found = true;
      }
    }
    if (found) continue;

    //    if (tstep != 684032) continue;

    if (tstep < startit) continue;


    std::cout << "*** Working on iteration " << (int) tstep << std::endl;
    auto rho = dbase_rho.getDatasets(vname_rho, tstep, reflevel);
    std::cout << "*** rho done " << std::endl;
    auto entropy = dbase_entropy.getDatasets(vname_entropy, tstep, reflevel);
    std::cout << "*** entropy done " << std::endl;
    auto temp = dbase_temp.getDatasets(vname_temp, tstep, reflevel);
    std::cout << "*** temperature done " << std::endl;
    auto ye = dbase_ye.getDatasets(vname_ye, tstep, reflevel);
    std::cout << "*** ye done " << std::endl;
    auto velx = dbase_vel.getDatasets(vname_velx, tstep, reflevel);
    std::cout << "*** velx done " << std::endl;
    auto vely = dbase_vel.getDatasets(vname_vely, tstep, reflevel);
    std::cout << "*** vely done " << std::endl;
    auto velz = dbase_vel.getDatasets(vname_velz, tstep, reflevel);
    std::cout << "*** velz done " << std::endl;
    auto lapse = dbase_lapse.getDatasets(vname_lapse, tstep, reflevel);
    std::cout << "*** lapse done " << std::endl;
    auto gxx   = dbase_metric.getDatasets(vname_gxx, tstep, reflevel);
    std::cout << "*** gxx done " << std::endl;
    auto gyy   = dbase_metric.getDatasets(vname_gyy, tstep, reflevel);
    std::cout << "*** gyy done " << std::endl;
    auto gzz   = dbase_metric.getDatasets(vname_gzz, tstep, reflevel);
    std::cout << "*** gzz done " << std::endl;
    auto heatcool  = dbase_heatcool.getDatasets(vname_heatcool, tstep, reflevel);
    std::cout << "*** heatcool done " << std::endl;

    std::cout << "*** Read Datasets complete " << (int) tstep << std::endl;

    LorentzFactor<3> wlorentz(rho[0],true);
    wlorentz.compute_w_lorentz(gxx[0],gyy[0],gzz[0],velx[0],vely[0],velz[0]);

    // we need this to have the rest-mass density integrand for later
    // interpolation to the spherical grid
    GravBaryMass<3> barymass(rho[0],true);
    barymass.compute_bary_mass(gxx[0],gyy[0],
			       gzz[0],wlorentz,rho[0],1.0);

    // grid spacing dx on the current refinement level
    auto mydelta = rho[0].getDelta();
    
    // NOTE THAT NETGAIN IS ALREADY MULTIPLIED BY ***COARSE GRID*** dx*dy*dz in ZelmaniM1!!!
    // Here we must correct for this
    double volume_factor = 1.0 /  pow(2,(reflevel)*3);

    // we need to multiply the netgain with the sqrt(detg) in order to get the
    // integral right
    Densitized<3> dens_heatcool(heatcool[0],false,gxx[0],gyy[0],gzz[0]);
    
    /*********************** Compute Radial Velocity *****************************/
    RadialVelocity<3> velr(velx[0],true,velx[0],vely[0],velz[0]);

    std::cout << "*** GF-level operations done " << (int) tstep << std::endl;

    /*********************** Start work on angular grid *****************************/

    std::vector<double> extent = rho[0].getMaxExtent();
    auto mxel = std::max_element(extent.begin(), extent.end(),
				 [](double x, double y){ return std::abs(x)<std::abs(y); });

    double rmax = *mxel;
    // set up radius array
    for (int i=0; i<nrad; ++i)
      radii[i] = r0 + (rmax-r0)*i/((double)nrad-1.0);
    
    // set up coordinates
    int idx=0;
    for (double mu : mus) {
      for (double phi : phis) {
	for (double radius : radii) {
	  const double x = sqrt(1.0 - mu*mu) * cos(phi) * radius;
	  const double y = sqrt(1.0 - mu*mu) * sin(phi) * radius;
	  const double z = mu*radius;
	  xarr[idx] = {x, y, z};
	  ++idx;
	}
      }
    }

    auto entropy_interp = entropy[0].interpolate(xarr);
    auto rho_interp = barymass.interpolate(xarr);
    auto temp_interp = temp[0].interpolate(xarr);
    auto ye_interp = ye[0].interpolate(xarr);
    auto heatcool_interp = dens_heatcool.interpolate(xarr);
    auto velr_interp = velr.interpolate(xarr);
    auto w_interp = wlorentz.interpolate(xarr);
    const int n = mus.size()*phis.size()*nrad;

    std::vector<double> av_heatcool(nrad);
    std::vector<double> av_entropy(nrad);
    std::vector<double> av_rho(nrad);
    std::vector<double> av_temp(nrad);
    std::vector<double> av_ye(nrad);
    std::vector<double> av_velr(nrad);
    std::vector<double> av_w(nrad);

    // shock analysis:
    double shock_rad = 0.0;
    double shock_sq = 0.0;
    double shock_max = 0.0;
    double shock_min = 1.e10;
    int imax = 0;
    int jmax = 0;
    int kmax = 0;
    double acc_inner = 0.0;
    for (int i=0; i < mus.size(); ++i) {
      for (int j=0; j < phis.size(); ++j) {
	double shock_c;
	double sedge = entropy_interp[radii.size()*(1 + j + phis.size()*i) - 1];
	for (int k=radii.size()-1; k>-1; --k) {
	  int idx = k + radii.size()*(j + phis.size()*i);
	  if (entropy_interp[idx] > fmax(4.0, 1.1*sedge)) {
	    shock_c = radii[k];
	    if(k > kmax) kmax = k;
	    //int idx2 = k-1 + radii.size()*(j + phis.size()*i);
	    //acc_inner += radii[k-1]*radii[k-1]*std::abs(velr_interp[idx2])*rho_interp[idx2];
	    break;
	  }
	}
	shock_sphere[i][j] = shock_c;
	shock_rad += shock_c;
	shock_sq  += shock_c*shock_c;
	if (shock_c > shock_max) {
	  imax = i;
	  jmax = j;
	}
	shock_max  = fmax(shock_max, shock_c);
	shock_min  = fmin(shock_min, shock_c);
      }
    }

    shock_rad /= (double) mus.size()*phis.size();

    // now let's fill our mask
    for (int i=0; i < mus.size(); ++i) {
      for (int j=0; j < phis.size(); ++j) {
	for (int k=0; k < radii.size(); ++k) {
	  if (radii[k] <= shock_sphere[i][j]) {
	    int idx = k + radii.size()*(j + phis.size()*i);
	    mshock[idx] = 1;
	  } else {
	    mshock[idx] = 0;
	  }
	}
      }
    }

    double mdot_rmax = 0.0;
    double rho_rmax = 0.0;
    double velr_rmax = 0.0;
    {
      int k = kmax;
      if(kmax < radii.size() - 2) {
	k += 2;
      } else if (k < radii.size() - 1) {
	k += 1;
      } 
      for (int i=0; i < mus.size(); ++i) {
	for (int j=0; j < phis.size(); ++j) {
	  int idx = k + radii.size()*(j + phis.size()*i);
	  mdot_rmax += radii[k]*radii[k]*rho_interp[idx] * std::abs(velr_interp[idx]);
	  velr_rmax += std::abs(velr_interp[idx]);
	  rho_rmax += rho_interp[idx];
	}
      }
      mdot_rmax  = 4.0 * PI * mdot_rmax / ( (double) mus.size()*phis.size() ) * 2.0304e5;
      rho_rmax = rho_rmax / ( (double) mus.size()*phis.size() );
      velr_rmax = velr_rmax / ( (double) mus.size()*phis.size() );
    }

    std::cout << "shock_max: " << shock_max << std::endl;
    std::cout << "velr_rmax: " << velr_rmax << std::endl;
    std::cout << "mdot_rmax: " << mdot_rmax << std::endl;

    // average quantities
    for (int i=0; i < mus.size(); ++i)
      for (int j=0; j < phis.size(); ++j)
        for (int k=0; k < radii.size(); ++k) {
          int idx = k + radii.size()*(j + phis.size()*i);
          av_heatcool[k] += heatcool_interp[idx];
          av_entropy[k]  += entropy_interp[idx];
	  av_rho[k]      += rho_interp[idx];
	  av_temp[k]     += temp_interp[idx];
	  av_ye[k]       += ye_interp[idx];
	  av_velr[k]     += velr_interp[idx];
	  av_w[k]        += w_interp[idx];
        }

    const double ifac = 1.0 / ((double) mus.size()*phis.size());
    for (int k=0;k<nrad;k++) {
      av_heatcool[k] *= ifac / pow(32.0,3);
      // * msun * clite * clite * time_gf / pow(32.0,3) * pow(length_gf,3);
      av_entropy[k] *= ifac;
      av_rho[k] *= ifac;
      av_temp[k] *= ifac;
      av_ye[k] *= ifac;
      av_velr[k] *= ifac;
      av_w[k] *= ifac;
    }

    // shock radius on averaged grid
    int kavmax = -1;
    {
      int k=radii.size()-1;
      while(av_entropy[k] < 4.0) {
	--k;
      }
      kavmax = k;
    }

    auto it = max_element(std::begin(av_heatcool), std::end(av_heatcool));
    const double maxheat = *it;

    double mgain = 0.0;
    double intheat = 0.0;
    double intheat2 = 0.0;
    const double dr = radii[1]-radii[0];
    for (int k=0;k < kavmax; ++k) {
      if (av_heatcool[k] > 0.0) {
	mgain += 4.0 * PI * radii[k]*radii[k] * av_rho[k]*dr;
	intheat2 += 4.0 * PI * radii[k]*radii[k] * av_heatcool[k] * dr;
      }
      if (av_heatcool[k] > 0.0) {
	intheat += 4.0 * PI * radii[k]*radii[k] * av_heatcool[k] * dr;
      }
    }

    intheat *= msun * clite * clite * time_gf;
    intheat2 *= msun * clite * clite * time_gf;

    std::cout << "mgain: " << mgain << std::endl;
    std::cout << "intheat: " << intheat << std::endl;
    std::cout << "intheat2: " << intheat2 << std::endl;
    const double tau_adv = mgain/mdot_rmax;
    std::cout << "tau_adv: " << tau_adv << std::endl;

    int kgain = 0;
    {
      int k = 0;
      while(av_heatcool[k] <= 0.0) {
      ++k;
      }
      kgain = k;
    }


    // compute diagnostic energy; first need enclosed mass
    const double m25 = barymass.maskedSum(0.0, 25.0) * pow(mydelta[0],3);
    std::vector<double> menc(nrad);
    menc[0] = m25;
    double egain = 0.0;
    for (int k=1;k < radii.size(); ++k) {
      menc[k] = menc[k-1] + dr * 4.0*PI*radii[k]*radii[k]*av_rho[k];
      
      if (k >= kgain && k < kavmax) {

	double press = 0.0;
	double eps = 0.0;
	double abar = 0.0;
	EOSDriver::GetPressureEpsAbarFromTemperature(av_rho[k]*INV_RHO_GF,
						     av_ye[k],av_temp[k],
						     &press,&eps,&abar);

	const double eps_thermal = 
	  epsThermal::GetThermalEnergy(av_rho[k]*INV_RHO_GF,av_temp[k],
				       av_ye[k],abar);

	double v2 = 1.0 - 1.0/(av_w[k]*av_w[k]);

	egain += 4.0*PI*radii[k]*radii[k] * av_rho[k] *
	  ( eps_thermal*EPS_GF + 0.5*v2 - menc[k]/radii[k]) * dr;  
	
      }
    }
  

////      //      if (av_heatcool[k] > 0.1*maxheat) {
////      if (k >= kgain && k < kavmax) {
////	double press = 0.0;
////	double eps = 0.0;
////	EOSDriver::GetPressureEpsFromTemperature(av_rho[k]*INV_RHO_GF,
////						 av_ye[k],av_temp[k],
////						 &press,&eps);
////
////#if 0
////	// free nucleons
////	double eps2 = 1.5 * av_temp[k] * 1.0218e-6 / 1.66054294e-24;
////	// photon gas
////	eps2 += 7.5657e-15 * pow(av_temp[k]*1.1604447522806e10,4) / (av_rho[k]*INV_RHO_GF);
////	// units
////	eps2 *= EPS_GF;
////#endif
////	// first renormalize SFHo internal energy
////	const double mev_to_erg = 1.60218e-6;
////	const double amu_mev = 931.49432e0;
////	const double amu_cgs = amu_mev / clite / clite * mev_to_erg;
////	const double mn_mev = 939.5654e0;
////	const double mp_mev = 938.2721e0;
////
////	const double eps_temp1 = eps * amu_cgs / mev_to_erg;
////	const double eps_temp2 = eps_temp1 + amu_mev - av_ye[k]*mp_mev 
////	  - (1.0-av_ye[k])*mn_mev;
////	//	const double mass_unit_cgs = (av_ye[k]*mp_mev + (1.0-av_ye[k])*mn_mev) 
////	//  * clite * clite / mev_to_erg;
////	double eps_fix = eps_temp2 * mev_to_erg / amu_cgs;
////	//const double eps_fix = eps_temp2 * mev_to_erg / mass_unit_cgs;
////	//	const double rho_fx = av_rho[k] / amu_cgs * mass_unit_cgs;
////
////	eps = eps*EPS_GF;
////	eps_fix = eps_fix*EPS_GF;
////	double v2 = 1.0 - 1.0/(av_w[k]*av_w[k]); 
////	fprintf(stderr,"%5d %15.6E %15.6E %15.6E %15.6E %15.6E %15.6E\n",k,radii[k],
////		0.5*v2,eps,eps_fix,-menc[k]/radii[k],egain);
////	egain += 4.0*PI*radii[k]*radii[k] * av_rho[k] * 
////	  ( eps_fix + 0.5*v2 ) * dr;
////
////	//	egain += 4.0*PI*radii[k]*radii[k] * av_rho[k] * 
////	//  ( eps_fix + 0.5*v2 - menc[k-1]/radii[k]) * dr;
////
////      }
////

    
    egain *= msun * clite * clite;

    std::cout << "Egain: " << egain << std::endl;
    const double tau_heat = std::abs(egain) / intheat2;
    std::cout << "tau_heat: " << tau_heat << std::endl;
    const double ratio = std::abs(tau_adv / tau_heat);
    std::cout << "ratio: " << ratio << std::endl;

#if 0
    std::ofstream ofile2("profile.dat", std::ofstream::out);
    int i = 0;
    int j = 0;
    for (int k = 0; k < radii.size(); ++k) {
      ofile2 << boost::format("% 15.6e ") % radii[k];
      ofile2 << boost::format("% 15.6e ") % av_entropy[k];
      ofile2 << boost::format("% 15.6e ") % av_rho[k];
      ofile2 << boost::format("% 15.6e ") % av_temp[k];
      ofile2 << boost::format("% 15.6e ") % av_ye[k];
      ofile2 << boost::format("% 15.6e ") % av_velr[k];
      ofile2 << boost::format("% 15.6e ") % av_heatcool[k];
      ofile2 << std::endl;
    }
    ofile2.close();                                                                              
#endif


    ofile << boost::format("% 15.6E ") % rho[0].getTime();
    ofile << boost::format("% 15.6E ") % mgain;
    ofile << boost::format("% 15.6E ") % intheat2;
    ofile << boost::format("% 15.6E ") % egain;
    ofile << boost::format("% 15.6E ") % mdot_rmax;
    ofile << boost::format("% 15.6E ") % tau_adv;
    ofile << boost::format("% 15.6E ") % tau_heat;
    ofile << std::endl;
    
  }
  
  ofile.close();


  return 0;
}
