#include <iostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 

#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename,N", po::value<std::string>(), "Regular expression describing the base name")
      ("outfile,O", po::value<std::string>()->default_value("out.h5"), "Output filename")
      ("reflevel,R", po::value<int>()->default_value(-1), "Specify refinement level (by default all are written to file)");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  if (!vm.count("basename")) {
    std::cerr << "basename is required but none was provided. Exiting." << std::endl;
    return 1;
  }
  std::cout << std::endl; 
  std::cout << "Base: "; 
  std::cout << vm["basename"].as<std::string>() << std::endl; 
  
  // Find out what files and datasets are available 
  CactusDatabase<3> dbase(vm["basepath"].as<std::string>(), 
      vm["basename"].as<std::string>(), vm["directory"].as<std::string>()); 
  
  // Open output hdf5 file
  H5::H5File h5Out;
  std::string outfile(vm["outfile"].as<std::string>());
  if (!boost::filesystem::exists(outfile)) {
    h5Out = H5::H5File(outfile, H5F_ACC_TRUNC); 
  } else { 
    h5Out = H5::H5File(outfile, H5F_ACC_RDWR); 
  }

  // Write out the combined dataset to file
  for (auto tstep : dbase.getTsteps()) {
    for (auto name : dbase.getVariables()) {
      std::cout << "TStep: " << tstep << " Name: " << name << std::endl;
      auto ds = dbase.getDatasets(name, tstep, vm["reflevel"].as<int>()); 
      auto full = CactusDataset<3>(ds); 
      try {
        full.writeToH5(h5Out);
      } catch(H5::Exception& e) {
        std::cerr << "H5 write failed with message : " << e.getDetailMsg() 
            << std::endl;
      }
    }
  }
  std::cout << "recombination done." << std::endl;
  h5Out.close();
  
  return 0;
}  
