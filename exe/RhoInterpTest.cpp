#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
//#include "RealSphericalHarmonic.hpp" 
//#include "DiagnosticEnergy.hpp"
#include <H5Cpp.h> 


namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename-rho,N", po::value<std::string>()->default_value("guenni_rho.*\.h5"), "Regular expression describing the base name for rho")
      ("outfile,O", po::value<std::string>()->default_value("RhoInterpTest.out"), "Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(), 
      vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  std::cout << "Starting rho test interpolation ..." << std::endl;

  std::string vname_rho   = "HYDROBASE::rho";

  // prepare outfile
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[1] x (Msun) " << std::endl; 
  ofile << "#[2] density  " << std::endl; 

  auto tsteps = dbase_rho.getTsteps();
  auto tstep = *(tsteps.begin());

  std::cout << "*** Working on iteration " << (int) tstep << std::endl;
  auto rho   = dbase_rho.getDatasets(vname_rho, tstep, reflevel); 

  const int nx = 600;
  const double xmin = 25.0;
  const double xmax = 580;
  std::vector<double> xx(nx);
  std::vector<std::array<double, 3>> xarr(nx);
  for(int i=0;i<xx.size();++i) {
    xx[i] = xmin + (xmax-xmin)*i/((double)nx-1.0);
    xarr[i] = { xx[i], 0.0, 0.0 };
  }

  auto rho_interp = rho[0].interpolate(xarr);

  for(int i=0;i<xx.size();++i) {
    ofile << boost::format("% 15.6E ") % xx[i];
    ofile << boost::format("% 15.6E ") % rho_interp[i];
    ofile << std::endl;
  }
  
  ofile.close(); 

  std::cout << "Done! Check output in output file!" << std::endl;
   
  return 0;
}  
