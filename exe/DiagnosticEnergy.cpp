#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include "DiagnosticEnergy.hpp"
#include <H5Cpp.h> 


namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename-rho,N", po::value<std::string>()->default_value("guenni_rho.*\.h5"), "Regular expression describing the base name for rho")
      ("basename-ye,N", po::value<std::string>()->default_value("guenni_ye.*\.h5"), "Regular expression describing the base name for Ye")
      ("basename-temp,N", po::value<std::string>()->default_value("guenni_temperature.*\.h5"), "Regular expression describing the base name for temperature")
      ("basename-vel,N", po::value<std::string>()->default_value("guenni_vel.*\.h5"), "Regular expression describing the base name for velocity")
      ("basename-lapse,N", po::value<std::string>()->default_value("guenni_lapse.*\.h5"), "Regular expression describing the base name for lapse")
      ("basename-metric,N", po::value<std::string>()->default_value("guenni_metric.*\.h5"), "Regular expression describing the base name for lapse")
      ("outfile,O", po::value<std::string>()->default_value("DiagnosticEnergy.out"), "Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level")
      ("skip-iteration,S", po::value<std::vector<int> >()->multitoken(), "Skip this iteration [can be used multiple times]");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 

  // by specifying -S <iteration> -S <iteration> -S <iteration> and so forth,
  // one can skip iterations one doesn't want to analyze. That's useful if
  // some data are missing.
  std::vector<int> skipits(0);
  if(vm.count("skip-iteration")) {
    skipits = vm["skip-iteration"].as<std::vector<int>>();
  }


  // first read the EOS table, table is currently hardcoded for simplicity
  EOSDriver::ReadTable("/work/00386/ux455321/stampede2/tables/SFHo.h5");
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(), 
      vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_ye(vm["basepath"].as<std::string>(), 
      vm["basename-ye"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_temp(vm["basepath"].as<std::string>(), 
      vm["basename-temp"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_vel(vm["basepath"].as<std::string>(), 
      vm["basename-vel"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_lapse(vm["basepath"].as<std::string>(), 
      vm["basename-lapse"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_metric(vm["basepath"].as<std::string>(), 
      vm["basename-metric"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);
   
  std::cout << "Starting diagnostic energy calculation ..." << std::endl;

  std::string vname_rho   = "HYDROBASE::rho";
  std::string vname_temp  = "HYDROBASE::temperature";
  std::string vname_ye    = "HYDROBASE::Y_e";
  std::string vname_velx  = "HYDROBASE::vel[0]";
  std::string vname_vely  = "HYDROBASE::vel[1]";
  std::string vname_velz  = "HYDROBASE::vel[2]";
  std::string vname_lapse = "ADMBASE::alp";
  std::string vname_gxx   = "ADMBASE::gxx";
  std::string vname_gyy   = "ADMBASE::gyy";
  std::string vname_gzz   = "ADMBASE::gzz";

  // prepare outfile
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[1] Time (Msun) " << std::endl; 
  ofile << "#[2] Int. Diag. Enr. (50) " << std::endl; 
  ofile << "#[3] Int. Diag. Enr. (100) " << std::endl; 
  ofile << "#[3] Int. Diag. Enr. (200) " << std::endl; 
  ofile << "#[4] Int. Diag. Enr. (Full)" << std::endl; 


  // Iterate over timesteps 
  for (auto tstep : dbase_rho.getTsteps()) {
    std::cout << "*** Working on iteration " << (int) tstep << std::endl;
    // std::cout << "Reading datasets!" << std::endl;

    // check if we need to skip the iteration                                                                                             
    bool found = false;
    for(std::vector<int>::const_iterator it = skipits.begin(); it < skipits.end(); ++it) {
      if( tstep == * it ) {
	std::cout << "Skipping iteration " << *it << " !" << std::endl;
        found = true;
      }
    }
    if (found) continue;

    auto rho   = dbase_rho.getDatasets(vname_rho, tstep, reflevel); 
    auto temp  = dbase_temp.getDatasets(vname_temp, tstep, reflevel); 
    auto ye    = dbase_ye.getDatasets(vname_ye, tstep, reflevel); 
    auto velx  = dbase_vel.getDatasets(vname_velx, tstep, reflevel); 
    auto vely  = dbase_vel.getDatasets(vname_vely, tstep, reflevel); 
    auto velz  = dbase_vel.getDatasets(vname_velz, tstep, reflevel); 
    auto lapse = dbase_lapse.getDatasets(vname_lapse, tstep, reflevel); 
    auto gxx   = dbase_metric.getDatasets(vname_gxx, tstep, reflevel); 
    auto gyy   = dbase_metric.getDatasets(vname_gyy, tstep, reflevel); 
    auto gzz   = dbase_metric.getDatasets(vname_gzz, tstep, reflevel); 

    //    std::cout << "Done reading datasets!" << std::endl;


    LorentzFactor<3> wlorentz(rho[0],true);

    wlorentz.compute_w_lorentz(gxx[0],gyy[0],gzz[0],velx[0],vely[0],velz[0]);

    // std::cerr << "computing diagnostic energy" << std::endl;
    DiagnosticEnergy<3> diagenr(rho[0],true);
    diagenr.compute_diag_enr(lapse[0],rho[0],temp[0],ye[0],
			     wlorentz,gxx[0],gyy[0],gzz[0]);

    //    std::cerr << "diag: " << diagenr(10000) << std::endl;

    // std::cerr << "computing sums" << std::endl;
    double diag50 = diagenr.maskedSumPos(25.0, 2000.0); 
    double diag100 = diagenr.maskedSumPos(50.0, 2000.0); 
    double diag200 = diagenr.maskedSumPos(75.0, 2000.0); 
    double diag_all = diagenr.maskedSumPos(0.0, 100000.0); 
    // std::cerr << "done computing sums" << std::endl;    

    // Note on units:
    // The sums do not multiply by the Cartesian coordinate volume element.
    // One needs to take the result and multiply it with the
    // Cartesian coordinate volume element for the reflevel on which
    // the analysis was carried out.
    // Then multiple by solar mass in cgs times c**2 in cgs. This should
    // give energy.

    // Example: dx = 2, diag_all = 5.15668e-5
    // E_diag(cgs) = 2.0**3 * 5.15668e-5 * 1.99e33 * 9e20

    std::cout << diagenr.getTime() << " " << diag50 << " " << diag_all << std::endl; 
    ofile << boost::format("% 12.5e ") % diagenr.getTime(); 
    ofile << boost::format("% 12.5e ") % diag50;
    ofile << boost::format("% 12.5e ") % diag100; 
    ofile << boost::format("% 12.5e ") % diag200; 
    ofile << boost::format("% 12.5e ") % diag_all; 
    ofile << std::endl; 

  }  
  ofile.close();



#if 0
  // Set up the angular grid and build spherical harmonics
  
  std::string vname = "ZELMANIM1::netheat";  
  //for (auto name : dbase.getVariables()) vname = name; 
   
  
  // Iterate over timesteps 
  for (auto tstep : dbase.getTsteps()) {
    auto darr = dbase.getDatasets(vname, tstep, reflevel); 
    double netheat25 = darr[0].maskedSum(25.0, 400.0); 
    double netheat50 = darr[0].maskedSum(50.0, 400.0); 
    double netheat75 = darr[0].maskedSum(75.0, 400.0); 
    double netheat_all = darr[0].maskedSum(0.0, 100000.0); 
    
    std::cout << darr[0].getTime() << " " << netheat50 << " " << netheat_all << std::endl; 
    ofile << boost::format("% 12.5e ") % darr[0].getTime(); 
    ofile << boost::format("% 12.5e ") % netheat25; 
    ofile << boost::format("% 12.5e ") % netheat50; 
    ofile << boost::format("% 12.5e ") % netheat75; 
    ofile << boost::format("% 12.5e ") % netheat_all; 
    ofile << std::endl; 
     
  } 
  ofile.close(); 
#endif
   
  return 0;
}  
