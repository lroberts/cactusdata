#ifndef DIAGNOSTICENERGY_HPP_
#define DIAGNOSTICENERGY_HPP_

#include <iostream>
#include <memory>
#include <vector>
#include <array>
#include <utility>
#include <H5Cpp.h>
#include <exception>
#include <NDArray.hpp>
#include <CactusDataset.hpp>
#include "EOSDriver/EOSDriver.hpp"
#include "epsThermal/epsThermal.hpp"

#define INV_RHO_GF 6.18735016707159e17
#define PRESS_GF 1.80123683248503E-39
#define EPS_GF 1.11265005605362e-21

template <int Nd>
class LorentzFactor: public CactusDataset<Nd> {
  
public:
  //    LorentzFactor(CactusDataset<Nd> velx, 
  //		CactusDataset<Nd> vely, 
  //		CactusDataset<Nd> velz);

  LorentzFactor(const CactusDataset<Nd>  & CDS, const bool make_zero) 
    : CactusDataset<Nd>(CDS,make_zero) {

    int npoints = 1;
    for(int i=0; i<Nd;i++) {
      npoints *= this->mData.dimSize(i);
    }
    for (int i=0; i < npoints; ++i) {
      this->mData[i] = 1.0;
    } 
    this->mName = "LorentzFactor";
  }

  void compute_w_lorentz(const CactusDataset<Nd> &gxx,
			 const CactusDataset<Nd> &gyy,
			 const CactusDataset<Nd> &gzz,
			 const CactusDataset<Nd> &velx,
			 const CactusDataset<Nd> &vely,
			 const CactusDataset<Nd> &velz);

  void compute_w_lorentz_full(const CactusDataset<Nd> &gxx,
			      const CactusDataset<Nd> &gxy,
			      const CactusDataset<Nd> &gxz,
			      const CactusDataset<Nd> &gyy,
			      const CactusDataset<Nd> &gyz,
			      const CactusDataset<Nd> &gzz,
			      const CactusDataset<Nd> &velx,
			      const CactusDataset<Nd> &vely,
			      const CactusDataset<Nd> &velz);



  
};

template <int Nd>
void LorentzFactor<Nd>::compute_w_lorentz(const CactusDataset<Nd> &gxx,
					  const CactusDataset<Nd> &gyy,
					  const CactusDataset<Nd> &gzz,
					  const CactusDataset<Nd> &velx,
					  const CactusDataset<Nd> &vely,
					  const CactusDataset<Nd> &velz) {
  

  // check these are all of the same size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    //    assert(velx.mData.dimSize(i) == vely.mData.dimSize(i) &&
    //	   vely.mData.dimSize(i) == velz.mData.dimSize(i) &&
    //	   velz.mData.dimSize(i) == this->mData.dimSize(i));
    npoints *= this->mData.dimSize(i);
    //std::cerr << this->mData.dimSize(i) << std::endl;
  }

  
  // assignment loop
  //  std::cerr << "computing Lorentz Factor! " << npoints << std::endl;
#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {
    const double vx2 = gxx(i)*velx(i)*velx(i);
    const double vy2 = gyy(i)*vely(i)*vely(i);
    const double vz2 = gzz(i)*velz(i)*velz(i);
    
    this->mData[i] = 1.0 / sqrt(1.0 - vx2 - vy2 - vz2);
  }
  //std::cerr << "done computing Lorentz Factor!" << std::endl;

}


template <int Nd>
void LorentzFactor<Nd>::compute_w_lorentz_full(const CactusDataset<Nd> &gxx,
					  const CactusDataset<Nd> &gxy,
					  const CactusDataset<Nd> &gxz,
					  const CactusDataset<Nd> &gyy,
					  const CactusDataset<Nd> &gyz,
					  const CactusDataset<Nd> &gzz,
					  const CactusDataset<Nd> &velx,
					  const CactusDataset<Nd> &vely,
					  const CactusDataset<Nd> &velz) {
  

  // check these are all of the same size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    //    assert(velx.mData.dimSize(i) == vely.mData.dimSize(i) &&
    //	   vely.mData.dimSize(i) == velz.mData.dimSize(i) &&
    //	   velz.mData.dimSize(i) == this->mData.dimSize(i));
    npoints *= this->mData.dimSize(i);
    //std::cerr << this->mData.dimSize(i) << std::endl;
  }

  
  // assignment loop
  //  std::cerr << "computing Lorentz Factor! " << npoints << std::endl;
#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {
    const double vx2 = (gxx(i)*velx(i) + gxy(i)*vely(i) + gxz(i)*velz(i)) *velx(i);
    const double vy2 = (gxy(i)*velx(i) + gyy(i)*vely(i) + gyz(i)*velz(i)) *vely(i);
    const double vz2 = (gxz(i)*velx(i) + gyz(i)*vely(i) + gzz(i)*velz(i)) *velz(i);
    
    this->mData[i] = 1.0 / sqrt(1.0 - vx2 - vy2 - vz2);
  }
  //std::cerr << "done computing Lorentz Factor!" << std::endl;

}


template <int Nd>
class DiagnosticEnergy: public CactusDataset<Nd> {
  
public:

  DiagnosticEnergy(const CactusDataset<Nd>  & CDS, const bool make_zero) 
    : CactusDataset<Nd>(CDS,make_zero) {
    this->mName = "DiagnosticEnergy";
  }

  void compute_diag_enr(const CactusDataset<Nd> &lapse,
			const CactusDataset<Nd> &rho,
			const CactusDataset<Nd> &temp,
			const CactusDataset<Nd> &ye,
			const CactusDataset<Nd> &wlorentz,
			const CactusDataset<Nd> &gxx,
			const CactusDataset<Nd> &gyy,
			const CactusDataset<Nd> &gzz);

  void compute_egain(const CactusDataset<Nd> &lapse,
		     const CactusDataset<Nd> &rho,
		     const CactusDataset<Nd> &temp,
		     const CactusDataset<Nd> &ye,
		     const CactusDataset<Nd> &wlorentz,
		     const CactusDataset<Nd> &gxx,
		     const CactusDataset<Nd> &gyy,
		     const CactusDataset<Nd> &gzz);

  void compute_ethermal(const CactusDataset<Nd> &rho,
			const CactusDataset<Nd> &temp,
			const CactusDataset<Nd> &ye,
			const CactusDataset<Nd> &wlorentz,
			const CactusDataset<Nd> &gxx,
			const CactusDataset<Nd> &gyy,
			const CactusDataset<Nd> &gzz);
  
};

template <int Nd>
void DiagnosticEnergy<Nd>::compute_diag_enr(const CactusDataset<Nd> &lapse,
					    const CactusDataset<Nd> &rho,
					    const CactusDataset<Nd> &temp,
					    const CactusDataset<Nd> &ye,
					    const CactusDataset<Nd> &wlorentz,
					    const CactusDataset<Nd> &gxx,
					    const CactusDataset<Nd> &gyy,
					    const CactusDataset<Nd> &gzz) {
  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  const double rho_min = 1.0e4;
  const double ye_min = 0.035;
  const double temp_min = 0.05;
  
  //  std::cout << "start main loop" << std::endl;

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {

    // approximate metric determinant
    const double w = wlorentz(i);
    const double r = rho(i);
    const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
    double press = 0.0;
    double eps = 0.0;
    EOSDriver::GetPressureEpsFromTemperature(std::max(r*INV_RHO_GF,rho_min),
					     std::max(ye(i),ye_min),
					     std::max(temp(i),temp_min),
					     &press,&eps);

    press *= PRESS_GF;
    eps *= EPS_GF;

#if 0
    double eps0;
    double bogus;
    EOSDriver::GetPressureEpsFromTemperature(std::max(r*INV_RHO_GF,rho_min),
					     std::max(ye(i),ye_min),
					     temp_min,
					     &bogus,&eps0);

    eps0 *= EPS_GF;
    eps = eps - eps0;
#endif

    const double h = 1.0 + eps + press/r;

    // diagnostic energy, equation 2 of Mueller et al. 2012a
    // multiplied with sqrt of the det of the 3 metric to account
    // for physical volume element in subsequent integration

    //    this->mData[i] = lapse(i)*(r*h*w*w - press) - r*w;

    //    this->mData[i] = lapse(i)*(r*w*w) - r*w;

    //    this->mData[i] = pow(sdetg,5.0/6.0) * (r*h*w*w - press) - sdetg*w*r;
    //    this->mData[i] = lapse(i) * pow(sdetg,5.0/6.0) * (r*h*w*w - press) - sdetg*w*r;
    this->mData[i] = lapse(i)*(r*h*w*w - press) - w*r;
    this->mData[i] *= sdetg;
      //    this->mData[i] *= sdetg;
      
  }
  //  std::cout << "end main loop" << std::endl;

}


template <int Nd>
void DiagnosticEnergy<Nd>::compute_ethermal(const CactusDataset<Nd> &rho,
					    const CactusDataset<Nd> &temp,
					    const CactusDataset<Nd> &ye,
					    const CactusDataset<Nd> &wlorentz,
					    const CactusDataset<Nd> &gxx,
					    const CactusDataset<Nd> &gyy,
					    const CactusDataset<Nd> &gzz) {

  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  const double rho_min = 1.0e4;
  const double ye_min = 0.035;
  const double temp_min = 0.05;
  
  //  std::cout << "start main loop" << std::endl;

#pragma omp parallel for shared(rho,temp,ye,wlorentz,gxx,gyy,gzz,ye_min,rho_min,temp_min)
  for (std::size_t i=0; i < npoints; ++i) {

    // approximate metric determinant
    const double w = wlorentz(i);
    const double r = rho(i);
    const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
    double press = 0.0;
    double eps = 0.0;
    double abar = 0.0;
    EOSDriver::GetPressureEpsAbarFromTemperature(std::max(r*INV_RHO_GF,rho_min),
						 std::max(ye(i),ye_min),
						 std::max(temp(i),temp_min),
						 &press,&eps,&abar);

    press *= PRESS_GF;

    double eps_thermal =
      epsThermal::GetThermalEnergy(std::max(r*INV_RHO_GF,rho_min),
				   std::max(temp(i),temp_min),
				   std::max(ye(i),ye_min),abar);

    eps_thermal *= EPS_GF;

    this->mData[i] = w*r*eps_thermal;
    this->mData[i] *= sdetg;
      
  }
  //  std::cout << "end main loop" << std::endl;

}

template <int Nd>
void DiagnosticEnergy<Nd>::compute_egain(const CactusDataset<Nd> &lapse,
					 const CactusDataset<Nd> &rho,
					 const CactusDataset<Nd> &temp,
					 const CactusDataset<Nd> &ye,
					 const CactusDataset<Nd> &wlorentz,
					 const CactusDataset<Nd> &gxx,
					 const CactusDataset<Nd> &gyy,
					 const CactusDataset<Nd> &gzz) {
  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  const double rho_min = 1.0e4;
  const double ye_min = 0.035;
  const double temp_min = 0.05;
  
  //  std::cout << "start main loop" << std::endl;

#pragma omp parallel for shared(lapse,rho,temp,ye,wlorentz,gxx,gyy,gzz,ye_min,rho_min,temp_min)
  for (std::size_t i=0; i < npoints; ++i) {

    // approximate metric determinant
    const double w = wlorentz(i);
    const double r = rho(i);
    const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
    double press = 0.0;
    double eps = 0.0;
    double abar = 0.0;
    EOSDriver::GetPressureEpsAbarFromTemperature(std::max(r*INV_RHO_GF,rho_min),
						 std::max(ye(i),ye_min),
						 std::max(temp(i),temp_min),
						 &press,&eps,&abar);

    press *= PRESS_GF;

    double eps_thermal =
      epsThermal::GetThermalEnergy(std::max(r*INV_RHO_GF,rho_min),
				   std::max(temp(i),temp_min),
				   std::max(ye(i),ye_min),abar);

    eps_thermal *= EPS_GF;

    const double h = 1.0 + eps_thermal + press/r;

    this->mData[i] = lapse(i) * (r*h*w*w - press) - w*r;
    this->mData[i] *= sdetg;
      
  }
  //  std::cout << "end main loop" << std::endl;

}




#endif // DIAGNOSTICENERGY_HPP_
