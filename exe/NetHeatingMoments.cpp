#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename,N", po::value<std::string>()->default_value("zelmanim1::heatcoolanalysis.*\.h5"), "Regular expression describing the base name")
      ("outfile,O", po::value<std::string>()->default_value("NetHeating.out"), "Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase(vm["basepath"].as<std::string>(), 
      vm["basename"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);
   
  std::cout << "Starting heating rate extraction..." << std::endl;
  
  // Set up the spherical grid
  std::vector<double> phis, mus;
  int nmu = 100;
  int nphi = 200;
  double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);
  
  int nrad = 600; 
  std::vector<double> radii(nrad);
  double r0 = 50.0;
  double rmax = 400.0; 
  for (int i=0; i<nrad; ++i) 
    radii[i] = r0 + (rmax-r0)*i/((double)nrad-1.0);
  
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size()*nrad);
  int idx=0;
  for (double mu : mus) {
    for (double phi : phis) { 
      for (double radius : radii) {
        double x = sqrt(1.0 - mu*mu) * cos(phi) * radius; 
        double y = sqrt(1.0 - mu*mu) * sin(phi) * radius; 
        double z = mu*radius; 
        xarr[idx] = {x, y, z};
        ++idx;
      }
    }
  }
  
  // Build spherical harmonics
  int lmax = 60;
  std::vector<std::vector<RealSphericalHarmonic>> Ylm(lmax+1); 
  for (int l=0; l<=lmax; ++l) {
    for (int m=-l; m<=l; ++m) {
      Ylm[l].push_back(RealSphericalHarmonic(l,m,mus,phis));
    }
  }
  
  std::string vname = "ZELMANIM1::netheat";  
  //for (auto name : dbase.getVariables()) vname = name; 
   
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[1] Time (Msun) " << std::endl; 
  ofile << "#[2] Q0 " << std::endl; 
  ofile << "#[3] Q1 " << std::endl; 
  ofile << "#[4] Q2 " << std::endl; 
  ofile << "#[5] Q3 " << std::endl; 
  ofile << "#[6] Q4 " << std::endl; 
  ofile << "#[7] Q5 " << std::endl; 
  
  // Iterate over timesteps 
  for (auto tstep : dbase.getTsteps()) {
    auto darr = dbase.getDatasets(vname, tstep, reflevel); 
    auto heating_interp = darr[0].interpolate(xarr); 
    
    std::vector<std::vector<double>> 
        heating_strips(mus.size(),std::vector<double>(phis.size()));
    for (int i=0; i < mus.size(); ++i) { 
      for (int j=0; j<phis.size(); ++j) { 
        heating_strips[i][j] = 0.0; 
        for (int k=0; k<radii.size(); ++k) { 
          int idx = k + radii.size()*(j + phis.size()*i);
          heating_strips[i][j] += heating_interp[idx] * radii[k] * radii[k];  
        } 
      }
    }
    
    std::vector<std::vector<double>> alm(lmax+1);
    std::vector<double> Al(lmax+1);
    for (int l=0; l<=lmax; ++l) { 
      Al[l] = 0.0;
      for (int m=-l; m<=l; ++m) { 
        double y = Ylm[l][m+l].Integrate(heating_strips);
        Al[l] += y*y; 
        alm[l].push_back(y);
      }
      Al[l] = sqrt(Al[l]);
    }
    
    std::cout << darr[0].getTime() << " " << Al[0] << " " << Al[1] << std::endl;
    ofile << boost::format("% 12.5e ") % darr[0].getTime(); 
    ofile << boost::format("% 12.5e ") % Al[0]; 
    ofile << boost::format("% 12.5e ") % Al[1]; 
    ofile << boost::format("% 12.5e ") % Al[2]; 
    ofile << boost::format("% 12.5e ") % Al[3]; 
    ofile << boost::format("% 12.5e ") % Al[4]; 
    ofile << boost::format("% 12.5e ") % Al[5]; 
    ofile << std::endl; 
     
  } 
  ofile.close(); 
   
  return 0;
}  
