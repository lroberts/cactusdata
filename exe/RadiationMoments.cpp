#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>
#include <string> 

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include <H5Cpp.h> 

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename,N", po::value<std::string>()->default_value("zelmanim1::enu.*\.h5"), "Regular expression describing the base name")
      ("outfile,O", po::value<std::string>()->default_value("Shock.out"), "Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase(vm["basepath"].as<std::string>(), 
      vm["basename"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);
   
  std::cout << "Starting radiation extraction..." << std::endl;
  
  // Set up the angular grid and build spherical harmonics
  std::vector<double> phis, mus;
  int nmu = 100;
  int nphi = 200;
  double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);
  int lmax = 1;
  std::vector<std::vector<RealSphericalHarmonic>> Ylm(lmax+1); 
  for (int l=0; l<=lmax; ++l) {
    for (int m=-l; m<=l; ++m) {
      Ylm[l].push_back(RealSphericalHarmonic(l,m,mus,phis));
    }
  }

  // Get variable names and sort by number  
  std::vector<std::string> vnames;  
  for (auto name : dbase.getVariables()) vnames.push_back(name); 
  std::sort(vnames.begin(), vnames.end(), [](std::string a, std::string b) {
    a.erase(std::remove_if(a.begin(), a.end(), [](char x) {
        return ! static_cast<bool> (std::isdigit(x));}), a.end()); 
    b.erase(std::remove_if(b.begin(), b.end(), [](char x) {
        return ! static_cast<bool> (std::isdigit(x));}), b.end()); 
    return std::stoi(a) < std::stoi(b);
  });
  
  for (auto name : vnames) std::cout << name << std::endl;

  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[1] Time (Msun) " << std::endl; 
  
  double r0 = 300.0;
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size());
   
  // Iterate over timesteps 
  for (auto tstep : dbase.getTsteps()) {
    ofile << boost::format("% 12.5e ") % (double) tstep; 
    for (auto vname : vnames) {
      std::cout << "Name: " << vname << " TStep: " << tstep << std::endl;
      auto darr = dbase.getDatasets(vname, tstep, reflevel); 
      std::vector<std::vector<double>> 
          radiation_sphere(mus.size(),std::vector<double>(phis.size()));
  
      // Build radial grid that goes to the maximum radius
      std::vector<double> extent = darr[0].getMaxExtent();
      auto mxel = std::max_element(extent.begin(), extent.end(),
         [](double x, double y){ return std::abs(x)<std::abs(y); });
      
      double radius = r0;
      if (*mxel < radius) 
        double radius = *mxel;
        
      int idx=0;
      for (double mu : mus) {
        for (double phi : phis) { 
          double x = sqrt(1.0 - mu*mu) * cos(phi) * radius; 
          double y = sqrt(1.0 - mu*mu) * sin(phi) * radius; 
          double z = mu*radius; 
          xarr[idx] = {x, y, z};
          ++idx;
        }
      }
       
      auto data = darr[0].interpolate(xarr);
      for (int i=0; i < mus.size(); ++i) {
        for (int j=0; j < phis.size(); ++j) {
          radiation_sphere[i][j] = data[j + i*phis.size()];
        }
      }

      std::vector<std::vector<double>> alm(lmax+1);
      std::vector<double> Al(lmax+1); 
      for (int l=0; l<=lmax; ++l) {
        Al[l] = 0.0;
        for (int m=-l; m<=l; ++m) { 
          double y = Ylm[l][m+l].Integrate(radiation_sphere);
          Al[l] += y*y;
          alm[l].push_back(y);
        }
        Al[l] = sqrt(Al[l]);
      } 
      
      ofile << boost::format("% 12.5e ") % alm[0][0]; 
      ofile << boost::format("% 12.5e ") % alm[1][0]; 
      ofile << boost::format("% 12.5e ") % alm[1][1]; 
      ofile << boost::format("% 12.5e ") % alm[1][2]; 
    }
    ofile << std::endl;
  } 
  ofile.close(); 
   
  return 0;
}  
