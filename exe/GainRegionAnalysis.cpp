#include <iostream>
#include <fstream>
#include <ostream>
#include <math.h>
#include <vector>
#include <memory>
#include <stdio.h>
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp"

#include "NDArray.hpp"
#include "CactusDataset.hpp"
#include "CactusDatabase.hpp"
#include "RealSphericalHarmonic.hpp"
#include "GainRegionAnalysis.hpp"
#include "DiagnosticEnergy.hpp"
#include "GravBaryMass.hpp"
#include <H5Cpp.h>

namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {

  // constants
  const double clite = 2.99792458e10;
  const double msun  =  1.9884e33;
  const double inv_time_gf = 4.92513293223396E-06;
  const double time_gf = 1.0/inv_time_gf;
  const double length_gf = 6.77269222552442E-06;
  const double inv_length_gf = 1.0/length_gf;
  const double inv_rho_gf = 6.17714470405638E+17;
  const double inv_press_gf = 5.55174079257738E+38;
  
  // Set up the cl interface
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
    ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
    ("basename-entropy,NE", po::value<std::string>()->default_value("hydrobase-entropy.*\.h5"), "Regular expression describing the base name for entropy")
    ("basename-rho,NR", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for rho")
    ("basename-temp,NR", po::value<std::string>()->default_value("hydrobase-temperature.*\.h5"), "Regular expression describing the base name for temperature")
    ("basename-ye,NY", po::value<std::string>()->default_value("hydrobase-rho.*\.h5"), "Regular expression describing the base name for Y_e")
    ("basename-vel,NV", po::value<std::string>()->default_value("hydrobase-vel.*\.h5"), "Regular expression describing the base name velocity")
    ("basename-lapse,NL", po::value<std::string>()->default_value("admbase-lapse.*\.h5"), "Regular expression describing the base name lapse")
    ("basename-metric,NM", po::value<std::string>()->default_value("admbase-metric.*\.h5"), "Regular expression describing the base name metric")
    ("basename-heatcool,NH", po::value<std::string>()->default_value("zelmanim1-heatcoolanalysis.*\.h5"), "Regular expression describing the base name heatcool data")
    ("outfile,O", po::value<std::string>()->default_value("GainRegionAnalysis.out"), "Output filename")
    ("outfile-thermo,T", po::value<std::string>()->default_value("GainRegionAnalysis-Thermo.out"), "Thermo Output filename")
    ("octant", "Assume the data posesses reflecting octant symmetry")
    ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level")
    ("startit,I", po::value<int>()->default_value(-1), "Refinement level")
    ("skip-iteration,S", po::value<std::vector<int> >()->multitoken(), "Skip this iteration [can be used multiple times]");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }

  int reflevel = vm["reflevel"].as<int>();
  int startit = vm["startit"].as<int>();

  // by specifying -S <iteration> -S <iteration> -S <iteration> and so forth,
  // one can skip iterations one doesn't want to analyze. That's useful if
  // some data are missing.
  std::vector<int> skipits(0);
  if(vm.count("skip-iteration")) {
    skipits = vm["skip-iteration"].as<std::vector<int>>();
  }

  //Build the database
  std::vector<bool> symmetry(3, false);
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true);

  CactusDatabase<3> dbase_entropy(vm["basepath"].as<std::string>(),
			  vm["basename-entropy"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(),
			  vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_temp(vm["basepath"].as<std::string>(),
			       vm["basename-temp"].as<std::string>(), vm["directory"].as<std::string>(),
			       symmetry);

  CactusDatabase<3> dbase_ye(vm["basepath"].as<std::string>(),
			  vm["basename-ye"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_vel(vm["basepath"].as<std::string>(),
			  vm["basename-vel"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_lapse(vm["basepath"].as<std::string>(),
			  vm["basename-lapse"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_metric(vm["basepath"].as<std::string>(),
			  vm["basename-metric"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  CactusDatabase<3> dbase_heatcool(vm["basepath"].as<std::string>(),
			  vm["basename-heatcool"].as<std::string>(), vm["directory"].as<std::string>(),
			  symmetry);

  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "WARNING! Some constants are hardcoded! Read the code!!!" << std::endl;
  std::cout << "Path to EOS table is hardcoded!!!                      " << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "*******************************************************" << std::endl;
  std::cout << "Done reading data structures!" << std::endl;

  // first read the EOS table, table is currently hardcoded for simplicity
  EOSDriver::ReadTable("/work/00386/ux455321/stampede2/tables/SFHo.h5");
  epsThermal::ReadTable("/work/00386/ux455321/stampede2/cactusdata/electron_table.h5");

  std::string vname_rho   = "HYDROBASE::rho";
  std::string vname_entropy  = "HYDROBASE::entropy";
  std::string vname_temp  = "HYDROBASE::temperature";
  std::string vname_ye    = "HYDROBASE::Y_e";
  std::string vname_velx  = "HYDROBASE::vel[0]";
  std::string vname_vely  = "HYDROBASE::vel[1]";
  std::string vname_velz  = "HYDROBASE::vel[2]";
  std::string vname_lapse = "ADMBASE::alp";
  std::string vname_gxx   = "ADMBASE::gxx";
  std::string vname_gyy   = "ADMBASE::gyy";
  std::string vname_gzz   = "ADMBASE::gzz";
  std::string vname_heat  = "ZELMANIM1::netheat";
  std::string vname_heatcool  = "ZELMANIM1::heatcool";

  // Set up the spherical grid                                                                                                                     
  std::vector<double> phis, mus;
  const int nmu = 200;
  const int nphi = 400;
  const double PI = 3.14159;
  for (int i=0; i<nmu; ++i)  mus.push_back(-1.0 + 2.0*(i+1)/(double)(nmu+1));
  for (int i=0; i<nphi; ++i) phis.push_back((i+1)/((double)(nphi+1))*2.0*PI);

  const int nrad = 600;
  std::vector<double> radii(nrad);

  // change this!!!
  const double r0 = 25.0;

  // coordinates
  std::vector<std::array<double, 3>> xarr(mus.size()*phis.size()*nrad);
  // integer shock mask
  std::vector<int> mshock(mus.size()*phis.size()*nrad);

  // shock radius as a function of angle
  std::vector<std::vector<double>>
    shock_sphere(mus.size(),std::vector<double>(phis.size()));

  // open output file and write header
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);
  ofile << "#[1] Time (Msun) " << std::endl;
  ofile << "#[2] Baryonic Mass in Gain Region [Msun]" << std::endl;
  ofile << "#[3] Bound baryonic Mass inside shock [Msun]" << std::endl;
  ofile << "#[4] Baryonic Mass inside shock [Msun]" << std::endl;
  ofile << "#[5] Qheat Net Heating (masked: r>25M,inside shock) [erg/s]" << std::endl;
  ofile << "#[6] Egain (masked: r>25, inside gain & shock) [erg]" << std::endl;
  ofile << "#[7] Ediag (masked: r>25, >0, in shock) [erg]" << std::endl;
  ofile << "#[8] Accretion rate through shock [Msun/s]" << std::endl;
  ofile << "#[9] tau_adv = Mgain/Mdot [ms]" << std::endl;
  ofile << "#[10] tau_heat = |Egain|/Qheat [ms]" << std::endl;
  ofile << "#[11] Eturb,r [erg]" << std::endl;
  ofile << "#[12] Eturb,t+p [erg]" << std::endl;
  ofile << "#[13] Ekin,r [erg]" << std::endl;
  ofile << "#[14] <Rgain> [km]" << std::endl;
  ofile << "#[15] MD P term [cgs]" << std::endl;
  ofile << "#[16] MDR TrR term [cgs]" << std::endl;
  ofile << "#[17] P(<Rgain>) [cgs]" << std::endl;
  ofile << "#[18] Etherm in gain [cgs]" << std::endl;

  std::ofstream ofile2(vm["outfile-thermo"].as<std::string>(), std::ofstream::out);
  ofile2 << "#[1] Time (Msun) " << std::endl;
  ofile2 << "#[2] Baryonic Mass in Gain Region [Msun]" << std::endl;
  ofile2 << "#[3] <Rgain> [km]" << std::endl;
  ofile2 << "#[4] <Rshock> [km]" << std::endl;
  ofile2 << "#[5] P(Rgain) [cgs]" << std::endl;
  ofile2 << "#[6] s(Rgain) [k_B/bar]" << std::endl;
  ofile2 << "#[7] T(Rgain) [MeV]" << std::endl;
  ofile2 << "#[8] rho(Rgain) [cgs]" << std::endl;
  ofile2 << "#[9] ye(Rgain)" << std::endl;
  ofile2 << "#[10] <s> in gain [k_B/bar]" << std::endl;
  ofile2 << "#[11] <T> in gain [MeV]" << std::endl;
  ofile2 << "#[12] <rho> in gain [cgs]" << std::endl;
  ofile2 << "#[13] <ye> in gain" << std::endl;


  // Iterate over timesteps
    
  for (auto tstep : dbase_rho.getTsteps()) {  

    // check if we need to skip the iteration
    bool found = false;
    for(std::vector<int>::const_iterator it = skipits.begin(); it < skipits.end(); ++it) {
      if( tstep == * it ) {
	std::cout << "Skipping iteration " << *it << " !" << std::endl;
	found = true;
      }
    }
    if (found) continue;

    if (tstep < startit) continue;

    std::cout << "*** Working on iteration " << (int) tstep << std::endl;
    auto rho = dbase_rho.getDatasets(vname_rho, tstep, reflevel);
    std::cout << "*** rho done " << std::endl;
    auto entropy = dbase_entropy.getDatasets(vname_entropy, tstep, reflevel);
    std::cout << "*** entropy done " << std::endl;
    auto temp = dbase_temp.getDatasets(vname_temp, tstep, reflevel);
    std::cout << "*** temperature done " << std::endl;
    auto ye = dbase_ye.getDatasets(vname_ye, tstep, reflevel);
    std::cout << "*** ye done " << std::endl;
    auto velx = dbase_vel.getDatasets(vname_velx, tstep, reflevel);
    std::cout << "*** velx done " << std::endl;
    auto vely = dbase_vel.getDatasets(vname_vely, tstep, reflevel);
    std::cout << "*** vely done " << std::endl;
    auto velz = dbase_vel.getDatasets(vname_velz, tstep, reflevel);
    std::cout << "*** velz done " << std::endl;
    auto lapse = dbase_lapse.getDatasets(vname_lapse, tstep, reflevel);
    std::cout << "*** lapse done " << std::endl;
    auto gxx   = dbase_metric.getDatasets(vname_gxx, tstep, reflevel);
    std::cout << "*** gxx done " << std::endl;
    auto gyy   = dbase_metric.getDatasets(vname_gyy, tstep, reflevel);
    std::cout << "*** gyy done " << std::endl;
    auto gzz   = dbase_metric.getDatasets(vname_gzz, tstep, reflevel);
    std::cout << "*** gzz done " << std::endl;
    auto netheat  = dbase_heatcool.getDatasets(vname_heat, tstep, reflevel);
    std::cout << "*** netheat done " << std::endl;
    auto heatcool  = dbase_heatcool.getDatasets(vname_heatcool, tstep, reflevel);
    std::cout << "*** heatcool done " << std::endl;

    std::cout << "*** Read Datasets complete " << (int) tstep << std::endl;

    LorentzFactor<3> wlorentz(rho[0],true);
    wlorentz.compute_w_lorentz(gxx[0],gyy[0],gzz[0],velx[0],vely[0],velz[0]);

    // mask that marks the region inside the shock, including the PNS.
    // the way this is works that it uses an entropy criterion and if
    // the entropy is higher than that value (4 by default), then the
    // mask is set to 1. It's also by default set to one inside r0.
    DoubleMask<3> shock_mask(rho[0],true);
    shock_mask.set_shock_mask(entropy[0],r0,4.0);

    // mask that makrs the gain region, excluding the PNS (r0). Inside
    // r0 it is set to 0.
    DoubleMask<3> shock_heating_mask(rho[0],true);
    shock_heating_mask.set_shock_and_heating_mask(entropy[0],netheat[0],r0,0.0,4.0,0.0);
    
    // baryonic mass integrant in the gain region
    GravBaryMass<3> barymass_in_gain(rho[0],true);
    barymass_in_gain.compute_bary_mass_with_mask(shock_heating_mask,gxx[0],
						  gyy[0],gzz[0],wlorentz,
						  rho[0],1.0);

    // we need this to have the rest-mass density integrand for later
    // interpolation to the spherical grid
    GravBaryMass<3> barymass(rho[0],true);
    barymass.compute_bary_mass(gxx[0],gyy[0],
			       gzz[0],wlorentz,rho[0],1.0);

    // grid spacing dx on the current refinement level
    auto mydelta = rho[0].getDelta();
    
    double bm_gain = barymass_in_gain.maskedSum(r0, 100000.0) * pow(mydelta[0],3);

    // NOTE THAT NETGAIN IS ALREADY MULTIPLIED BY ***COARSE GRID*** dx*dy*dz in ZelmaniM1!!!
    // Here we must correct for this
    double volume_factor = 1.0 /  pow(2,(reflevel)*3);

    // we need to multiply the netgain with the sqrt(detg) in order to get the
    // integral right
    Densitized<3> densitized_netgain(netheat[0],false,gxx[0],gyy[0],gzz[0]);
    double dens_intnetgain = densitized_netgain.maskedSumMultMask(shock_mask,r0,100000.0);
    dens_intnetgain *= msun * clite * clite * time_gf * volume_factor;

    std::cout << "bary mass in gain: " << bm_gain << std::endl;
    std::cout << "masked integrated densitized net heating " << dens_intnetgain << std::endl;
    
    // *********************** Energy in the gain region
    // using Eqs. 6 and 2. of Mueller et al. 2012a

    Timer guenni;
    DiagnosticEnergy<3> diag_energy(rho[0],true);
    DiagnosticEnergy<3> etherm(rho[0],true);
    diag_energy.compute_egain(lapse[0],rho[0],temp[0],ye[0],
			      wlorentz,gxx[0],gyy[0],gzz[0]);
    etherm.compute_ethermal(rho[0],temp[0],ye[0],wlorentz,gxx[0],gyy[0],gzz[0]);

    double t = guenni.elapsed();
    std::cout << "compute_egain: " << t << " seconds" << std::endl;

    double Egain = diag_energy.maskedSumMultMask(shock_heating_mask,r0,100000.0);
    Egain *= msun * clite * clite *  pow(mydelta[0],3);

    double Ediag = diag_energy.maskedSumMultMaskPos(shock_mask,r0,100000.0);
    Ediag *= msun * clite * clite *  pow(mydelta[0],3);

    double Etherm = etherm.maskedSumMultMaskPos(shock_heating_mask,r0,100000.0);
    Etherm *= msun * clite * clite *  pow(mydelta[0],3);

    std::cout << "Egain:  " << Egain << std::endl;
    std::cout << "Etherm:  " << Etherm << std::endl;
    std::cout << "Diagnostic:  " << Ediag << std::endl;

    DoubleMask<3> shock_bound_mask(rho[0],true);
    shock_bound_mask.set_shock_and_bound_mask(entropy[0],diag_energy,r0,1.0,4.0);

    double Mbound = barymass.maskedSumMultMask(shock_bound_mask,0.0,1000000.0);
    Mbound *= pow(mydelta[0],3);
    std::cout << "Mbound: " << Mbound << std::endl;

    double Mshock = barymass.maskedSumMultMask(shock_mask,0.0,1000000.0);
    Mshock *= pow(mydelta[0],3);
    std::cout << "Mshock: " << Mshock << std::endl;

    /*********************** Compute Radial Velocity *****************************/
    RadialVelocity<3> velr(velx[0],true,velx[0],vely[0],velz[0]);

    /*********************** Compute the pressure *****************************/
    Pressure<3> press(rho[0],true,rho[0],temp[0],ye[0]);

    /*********************** Start work on angular grid *****************************/

    std::vector<double> extent = rho[0].getMaxExtent();
    auto mxel = std::max_element(extent.begin(), extent.end(),
				 [](double x, double y){ return std::abs(x)<std::abs(y); });

    double rmax = *mxel;
    // set up radius array
    for (int i=0; i<nrad; ++i)
      radii[i] = r0 + (rmax-r0)*i/((double)nrad-1.0);
    
    // set up coordinates
    int idx=0;
    for (double mu : mus) {
      for (double phi : phis) {
	for (double radius : radii) {
	  const double x = sqrt(1.0 - mu*mu) * cos(phi) * radius;
	  const double y = sqrt(1.0 - mu*mu) * sin(phi) * radius;
	  const double z = mu*radius;
	  xarr[idx] = {x, y, z};
	  ++idx;
	}
      }
    }

    auto entropy_interp = entropy[0].interpolate(xarr);
    auto temp_interp = temp[0].interpolate(xarr);
    auto ye_interp = ye[0].interpolate(xarr);
    auto rho_interp = barymass.interpolate(xarr);
    auto press_interp = press.interpolate(xarr);
    auto velr_interp = velr.interpolate(xarr);
    auto velx_interp = velx[0].interpolate(xarr);
    auto vely_interp = vely[0].interpolate(xarr);
    auto velz_interp = velz[0].interpolate(xarr);
    auto heatcool_interp = netheat[0].interpolate(xarr);
    auto heatcool2_interp = heatcool[0].interpolate(xarr);
    const int n = mus.size()*phis.size()*nrad;
 
    std::vector<double> av_heatcool(nrad);
    std::vector<double> av_press(nrad);
    std::vector<double> av_ent(nrad);
    std::vector<double> av_temp(nrad);
    std::vector<double> av_ye(nrad);
    std::vector<double> av_rho(nrad);
    std::vector<double> av_velr(nrad);
    std::vector<double> av_trR(nrad);
    std::vector<double> xvelr(n);
    std::vector<double> velt(n);
    std::vector<double> velp(n);


    for (int k=0;k<nrad;k++) {
      av_velr[k] = 0.0;
      av_heatcool[k] = 0.0;
      av_press[k] = 0.0;
      av_temp[k] = 0.0;
      av_ent[k] = 0.0;
      av_ye[k] = 0.0;
      av_rho[k] = 0.0;
      av_trR[k] = 0.0;
    }

    // shock analysis:
    double shock_rad = 0.0;
    double shock_sq = 0.0;
    double shock_max = 0.0;
    double shock_min = 1.e10;
    int imax = 0;
    int jmax = 0;
    int kmax = 0;
    //    double acc_inner = 0.0;
    for (int i=0; i < mus.size(); ++i) {
      for (int j=0; j < phis.size(); ++j) {
	double shock_c;
	double sedge = entropy_interp[radii.size()*(1 + j + phis.size()*i) - 1];
	for (int k=radii.size()-1; k>-1; --k) {
	  int idx = k + radii.size()*(j + phis.size()*i);
	  if (entropy_interp[idx] > fmax(4.0, 1.1*sedge)) {
	    shock_c = radii[k];
	    if(k > kmax) kmax = k;
	    //int idx2 = k-1 + radii.size()*(j + phis.size()*i);
	    //	    acc_inner += radii[k-1]*radii[k-1]*std::abs(velr_interp[idx2])*rho_interp[idx2];
	    break;
	  }
	}
	shock_sphere[i][j] = shock_c;
	shock_rad += shock_c;
	shock_sq  += shock_c*shock_c;
	if (shock_c > shock_max) {
	  imax = i;
	  jmax = j;
	}
	shock_max  = fmax(shock_max, shock_c);
	shock_min  = fmin(shock_min, shock_c);
      }
    }

    shock_rad /= (double) mus.size()*phis.size();
    //    const double mdot_inner = 4.0 * PI * acc_inner / ((double) mus.size()*phis.size())
    //  * 2.0304e5;

    // now let's fill our mask
#pragma omp parallel for
    for (int i=0; i < mus.size(); ++i) {
      for (int j=0; j < phis.size(); ++j) {
	for (int k=0; k < radii.size(); ++k) {
	  if (radii[k] <= shock_sphere[i][j]) {
	    int idx = k + radii.size()*(j + phis.size()*i);
	    mshock[idx] = 1;
	  } else {
	    mshock[idx] = 0;
	  }
	}
      }
    }

    // set up r,t,p velocities                                                                              
#pragma omp parallel for
    for (int i=0; i < mus.size(); ++i)
      for (int j=0; j < phis.size(); ++j)
        for (int k=0; k < radii.size(); ++k) {

          int idx = k + radii.size()*(j + phis.size()*i);
          const double x = xarr[idx][0];
          const double y = xarr[idx][1];
          const double z = xarr[idx][2];
          //const double r = sqrt(x*x + y*y + z*z);
          const double st = sqrt(1.0 - mus[i]*mus[i]);
          const double ct = mus[i];
          const double sp = sin(phis[j]);
          const double cp = cos(phis[j]);
          xvelr[idx] = st*cp*velx_interp[idx] + st*sp*vely_interp[idx] + ct*velz_interp[idx];
          velt[idx] = ct*cp*velx_interp[idx] + ct*sp*vely_interp[idx] - st*velz_interp[idx];
          velp[idx] = -sp*velx_interp[idx]   + cp*vely_interp[idx];
        }

    // get the angle averaged radial velocity and other
    // angle averages  
    const double third = 1.0/3.0;
    for (int i=0; i < mus.size(); ++i)
      for (int j=0; j < phis.size(); ++j)
        for (int k=0; k < radii.size(); ++k) {
          int idx = k + radii.size()*(j + phis.size()*i);
          av_velr[k] += xvelr[idx];
	  av_ent[k] += entropy_interp[idx];
	  av_temp[k] += temp_interp[idx];
	  av_ye[k] += ye_interp[idx];
	  av_heatcool[k] += heatcool2_interp[idx];
	  av_press[k] += press_interp[idx];
	  av_rho[k] += rho_interp[idx];
       }
    const double ifac = 1.0 / ((double) mus.size()*phis.size());
    for (int k=0;k<nrad;k++) {
      av_velr[k] *= ifac;
      av_ent[k] *= ifac;
      av_temp[k] *= ifac;
      av_ye[k] *= ifac;
      av_press[k] *= ifac * inv_press_gf;
      av_heatcool[k] *= ifac / pow(32.0,3);
      av_rho[k] *= ifac * inv_rho_gf;
    }

    for (int i=0; i < mus.size(); ++i)
      for (int j=0; j < phis.size(); ++j)
        for (int k=0; k < radii.size(); ++k) {
          int idx = k + radii.size()*(j + phis.size()*i);
	  double rr = pow(xvelr[idx] - av_velr[k],2);
	  double tt = pow(velt[idx],2);
	  double pp = pow(velp[idx],2);
	  av_trR[k] += rho_interp[idx] * ( rr + tt + pp );
	}

    for (int k=0;k<nrad;k++) {
      av_trR[k] *= ifac*clite*clite*inv_rho_gf*third;
    }

    // find gain radius
    double Rgain = 0.0;
    int igain = -1;
    {
      int i = 0;
      while(av_heatcool[i] <= 0.0 && i < nrad) 
	i++;
      Rgain = radii[i] * inv_length_gf;
      igain = i;
    }


    // compute integrals                                                                                    
    double Ekin_r = 0.0e0;
    double Ekin_r_background = 0.0e0;
    double Ekin_tp = 0.0e0;
    double volume = 0.0e0;
    {
      const double dphi = phis[1]-phis[0];
      const double dmu = mus[1]-mus[0];
      const double dr = radii[1]-radii[0];
#pragma omp parallel for reduction(+:volume,Ekin_r_background,Ekin_r,\
				   Ekin_tp)
      for (int i=0; i < mus.size(); ++i)
        for (int j=0; j < phis.size(); ++j)
          for (int k=0; k < radii.size();++k) {
            int idx = k + radii.size()*(j + phis.size()*i);
	    const double dV = radii[k]*radii[k] * dr * dmu * dphi;
            if (heatcool_interp[idx] > 0.0 && mshock[idx] == 1) {
              Ekin_r += 0.5 * rho_interp[idx]*pow(xvelr[idx]-av_velr[k],2) * dV;
              Ekin_tp += 0.5 * rho_interp[idx] * (velt[idx]*velt[idx] +
                                                  velp[idx]*velp[idx]) * dV;
	      Ekin_r_background += 0.5 * rho_interp[idx] * pow(av_velr[k],2) * dV;

            }
            volume += dV;
          }
    }
    const double dir_volume = 4.0/3.0 * PI * ( pow(radii[radii.size()-1],3)
					       - pow(radii[0],3) );
    std::cout << "test int: " << volume << std::endl;
    std::cout << "truth:    " << dir_volume << std::endl;

    Ekin_r_background *= pow(inv_length_gf,3) * inv_rho_gf * pow(clite,2);
    Ekin_r *= pow(inv_length_gf,3) * inv_rho_gf * pow(clite,2);
    Ekin_tp *= pow(inv_length_gf,3) * inv_rho_gf * pow(clite,2);
    
    std::cout << "Ekin_r : " << Ekin_r << std::endl;
    std::cout << "Ekin_tp: " << Ekin_tp << std::endl;

    double MD_press_term = 0.0;
    double MD_turb_term = 0.0;
    double mean_ent = 0.0;
    double mean_ye = 0.0;
    double mean_rho = 0.0;
    double mean_temp = 0.0;
    {
      const double dr = radii[1]-radii[0];
      
      for (int k=0; k < radii.size();++k) {
	if(av_heatcool[k] > 0.0 && radii[k] <= shock_rad) {
	  double r2 = radii[k]*radii[k];
	  MD_press_term += dr * 2.0 * radii[k] * av_press[k];
	  MD_turb_term += dr * 2.0 * radii[k] * av_trR[k];
	  mean_ent += r2*dr*av_ent[k]*av_rho[k];
	  mean_ye += r2*dr*av_ye[k]*av_rho[k];
	  mean_temp += r2*dr*av_temp[k]*av_rho[k];
	}
      }
    }

    MD_press_term *= inv_length_gf*inv_length_gf;
    MD_turb_term *= inv_length_gf*inv_length_gf;

    mean_ent *= pow(inv_length_gf,3) * 4.0 * PI / (bm_gain*msun);
    mean_ye *= pow(inv_length_gf,3) * 4.0 * PI / (bm_gain*msun);
    mean_temp *= pow(inv_length_gf,3) * 4.0 * PI / (bm_gain*msun);
    double vol_gain = pow(shock_rad*inv_length_gf,3.0) 
      - pow(radii[igain]*inv_length_gf,3);
    vol_gain *= 4.0/3.0 * PI;
    mean_rho = bm_gain/vol_gain * msun;


    double Pgain = av_press[igain];
    double rhoGain = av_rho[igain];
    double tempGain = av_temp[igain];
    double yeGain = av_ye[igain];
    double entGain = av_ent[igain];
				  

    std::cout << "MD_press_term: " << MD_press_term << std::endl;
    std::cout << "MD_turb_term: " << MD_turb_term << std::endl;


    double mdot_rmax = 0.0;
    double rho_rmax = 0.0;
    double velr_rmax = 0.0;
    {
      int k = kmax;
      if(kmax < radii.size() - 2) {
	k += 2;
      } else if (k < radii.size() - 1) {
	k += 1;
      } 
      for (int i=0; i < mus.size(); ++i) {
	for (int j=0; j < phis.size(); ++j) {
	  int idx = k + radii.size()*(j + phis.size()*i);
	  mdot_rmax += radii[k]*radii[k]*rho_interp[idx] * std::abs(velr_interp[idx]);
	  velr_rmax += std::abs(velr_interp[idx]);
	  rho_rmax += rho_interp[idx];
	}
      }
      mdot_rmax  = 4.0 * PI * mdot_rmax / ( (double) mus.size()*phis.size() ) * 2.0304e5;
      rho_rmax = rho_rmax / ( (double) mus.size()*phis.size() );
      velr_rmax = velr_rmax / ( (double) mus.size()*phis.size() );
    }

    std::cout << "shock_max: " << shock_max << std::endl;
    std::cout << "velr_rmax: " << velr_rmax << std::endl;

    // next implement mdot outside the maximum shock radius averaged over all angles

    const double tau_adv1 = bm_gain/mdot_rmax * 1000;
    const double tau_heat = std::abs(Egain) / dens_intnetgain * 1000;
    const double ratio1 = tau_adv1 / tau_heat;
    std::cout << "av Mdot outside rmax shock: " << mdot_rmax << std::endl;
    //    std::cout << "av Mdot justinside shock: " << mdot_inner << std::endl;
    std::cout << "tau_adv1 = Mgain/Mdot: " << tau_adv1 << std::endl;
    std::cout << "tau_heat = Egain/Qheat: " << tau_heat << std::endl;
    std::cout << "ratio1: " << ratio1 << std::endl;

    shock_rad *= inv_length_gf;

    ofile << boost::format("% 15.6E ") % rho[0].getTime();
    ofile << boost::format("% 15.6E ") % bm_gain;
    ofile << boost::format("% 15.6E ") % Mbound;
    ofile << boost::format("% 15.6E ") % Mshock;
    ofile << boost::format("% 15.6E ") % dens_intnetgain;
    ofile << boost::format("% 15.6E ") % Egain;
    ofile << boost::format("% 15.6E ") % Ediag;
    ofile << boost::format("% 15.6E ") % mdot_rmax;
    ofile << boost::format("% 15.6E ") % tau_adv1;
    ofile << boost::format("% 15.6E ") % tau_heat;
    ofile << boost::format("% 15.6E ") % Ekin_r;
    ofile << boost::format("% 15.6E ") % Ekin_tp;
    ofile << boost::format("% 15.6E ") % Ekin_r_background;
    ofile << boost::format("% 15.6E ") % Rgain;
    ofile << boost::format("% 15.6E ") % MD_press_term;
    ofile << boost::format("% 15.6E ") % MD_turb_term;
    ofile << boost::format("% 15.6E ") % Pgain;
    ofile << boost::format("% 15.6E ") % Etherm;
    ofile << std::endl;

    ofile2 << boost::format("% 15.6E ") % rho[0].getTime();
    ofile2 << boost::format("% 15.6E ") % bm_gain;
    ofile2 << boost::format("% 15.6E ") % Rgain;
    ofile2 << boost::format("% 15.6E ") % shock_rad;
    ofile2 << boost::format("% 15.6E ") % Pgain;
    ofile2 << boost::format("% 15.6E ") % entGain;
    ofile2 << boost::format("% 15.6E ") % tempGain;
    ofile2 << boost::format("% 15.6E ") % rhoGain;
    ofile2 << boost::format("% 15.6E ") % yeGain;
    ofile2 << boost::format("% 15.6E ") % mean_ent;
    ofile2 << boost::format("% 15.6E ") % mean_temp;
    ofile2 << boost::format("% 15.6E ") % mean_rho;
    ofile2 << boost::format("% 15.6E ") % mean_ye;
    ofile2 << std::endl;

#if 0
    std::ofstream ofile("test2.dat", std::ofstream::out);
    int i = 0;
    int j = 0;
    for (int k = 0; k < radii.size(); ++k) {
      //int idx = k + radii.size()*(j + phis.size()*i);                                                                                                                                                 
      ofile << boost::format("% 15.6E ") % radii[k];
      ofile << boost::format("% 15.6E ") % av_heatcool[k];
      ofile << std::endl;
    }
    ofile.close();
    abort();
#endif


  }
  
  ofile.close();
  ofile2.close();


  return 0;
}
