#ifndef GainRegionAnalysis_HPP_
#define GainRegionAnalysis_HPP_

#include <iostream>
#include <memory>
#include <vector>
#include <array>
#include <utility>
#include <chrono>
#include <H5Cpp.h>
#include <exception>
#include <NDArray.hpp>
#include <CactusDataset.hpp>
#include "EOSDriver/EOSDriver.hpp"

#define INV_RHO_GF 6.18735016707159e17
#define PRESS_GF 1.80123683248503E-39
#define EPS_GF 1.11265005605362e-21

class Timer
{
public:
  Timer() : beg_(clock_::now()) {}
  void reset() { beg_ = clock_::now(); }
  double elapsed() const { 
    return std::chrono::duration_cast<second_>
      (clock_::now() - beg_).count(); }

private:
  typedef std::chrono::high_resolution_clock clock_;
  typedef std::chrono::duration<double, std::ratio<1> > second_;
  std::chrono::time_point<clock_> beg_;
};


template <int Nd>
class Densitized: public CactusDataset<Nd> {

public:
  
  Densitized(const CactusDataset<Nd>  & CDS, const bool make_zero,
	     const CactusDataset<Nd>  & gxx,
	     const CactusDataset<Nd>  & gyy,
	     const CactusDataset<Nd>  & gzz)
    : CactusDataset<Nd>(CDS,make_zero) {
    this->mName = "Densitized";
   
    // get size
    std::size_t npoints = 1;
    for(int i=0; i<Nd;i++) {
      npoints *= this->mData.dimSize(i);
    }

    assert(Nd == 3); // this will work only in 3D 
    #pragma omp parallel for
    for (std::size_t i=0; i < npoints; ++i) {
      const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
      this->mData[i] *= sdetg;
    }

  }

};

template <int Nd>
class RadialVelocity: public CactusDataset<Nd> {

public:
  
  RadialVelocity(const CactusDataset<Nd>  & CDS, const bool make_zero,
		 const CactusDataset<Nd>  & velx,
		 const CactusDataset<Nd>  & vely,
		 const CactusDataset<Nd>  & velz)
    : CactusDataset<Nd>(CDS,make_zero) {
    this->mName = "RadialVelcoity";
    
    // get size
    std::size_t npoints = 1;
    for(int i=0; i<Nd;i++) {
      npoints *= this->mData.dimSize(i);
    }

    assert(Nd == 3); // this will work only in 3D 
#pragma omp parallel for
    for (std::size_t i=0; i < npoints; ++i) {
      std::array<std::size_t, 3> idx = this->mData.GetIdxInverseCactus(i);
      const double x = this->mOrigin[0] + idx[0]*this->mDelta[0];
      const double y = this->mOrigin[1] + idx[1]*this->mDelta[1];
      const double z = this->mOrigin[2] + idx[2]*this->mDelta[2];
      const double rad = sqrt(x*x+y*y+z*z);
      
      this->mData[i] = (velx(i)*x + vely(i)*y + velz(i)*z)/(rad+1.0e-10);

    }

  }

};

template <int Nd>
class Pressure: public CactusDataset<Nd> {

public:
  
  Pressure(const CactusDataset<Nd>  & CDS, const bool make_zero,
	   const CactusDataset<Nd>  & rho,
	   const CactusDataset<Nd>  & temp,
	   const CactusDataset<Nd>  & ye)
    : CactusDataset<Nd>(CDS,make_zero) {

    this->mName = "Pressure";
   
    // get size
    std::size_t npoints = 1;
    for(int i=0; i<Nd;i++) {
      npoints *= this->mData.dimSize(i);
    }

    assert(Nd == 3); // this will work only in 3D 
    const double rho_min = 1.0e4;
    const double ye_min = 0.035;
    const double temp_min = 0.05;
    
#pragma omp parallel for
    for (std::size_t i=0; i < npoints; ++i) {
      std::array<std::size_t, 3> idx = this->mData.GetIdxInverseCactus(i);
      double press = 0.0;
      press = EOSDriver::GetPressureFromTemperature(std::max(rho(i)*INV_RHO_GF,rho_min),
					    std::max(ye(i),ye_min),
					    std::max(temp(i),temp_min));
    
      this->mData[i] = press*PRESS_GF;

    }
    
  }

};


template <int Nd>
class DoubleMask: public CactusDataset<Nd> {
  
public:

  DoubleMask(const CactusDataset<Nd>  & CDS, const bool make_zero) 
    : CactusDataset<Nd>(CDS,make_zero) {
    this->mName = "DoubleMask";
  }

  void set_shock_mask(const CactusDataset<Nd> &entropy,
		      const double rmin,
		      const double entropy_min);

  void set_shock_and_heating_mask(const CactusDataset<Nd> &entropy,
				  const CactusDataset<Nd> &netheat,
				  const double rmin,
				  // mask value inside r_min
				  const double rmin_value, 
				  const double entropy_min,
				  const double heat_min);

  void set_shock_and_bound_mask(const CactusDataset<Nd> &entropy,
				const CactusDataset<Nd> &diag_energy,
				const double rmin,
				// mask value inside r_min
				const double rmin_value, 
				const double entropy_min);


};


template <int Nd>
void DoubleMask<Nd>::set_shock_mask(const CactusDataset<Nd> &entropy,
				    const double rmin,
				    const double entropy_min)
{

  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  assert(Nd == 3); // this will work only in 3D

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {
  // find closest r,mu,phi
      std::array<std::size_t, 3> idx = this->mData.GetIdxInverseCactus(i);
      const double x = this->mOrigin[0] + idx[0]*this->mDelta[0];
      const double y = this->mOrigin[1] + idx[1]*this->mDelta[1];
      const double z = this->mOrigin[2] + idx[2]*this->mDelta[2];
      const double rad = sqrt(x*x+y*y+z*z);

      if(entropy(i) >= entropy_min) {
	this->mData[i] = 1.0;
      } else {
	this->mData[i] = 0.0;
      }
      if(rad < rmin) {
	this->mData[i] = 1.0;
      }
  }


#if 0
#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {

    const double r = rho(i);

    if(r*INV_RHO_GF >= rho_cut_cgs) {
      // approximate metric determinant
      const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
      const double w = wlorentz(i);
      this->mData[i] = sdetg*w*r;
    } else {
      this->mData[i] = 0.0;
    }
      
  }
#endif

}

template <int Nd>
void DoubleMask<Nd>::set_shock_and_heating_mask(const CactusDataset<Nd> &entropy,
						const CactusDataset<Nd> &heating,
						const double rmin,
						const double rmin_value,
						const double entropy_min,
						const double heat_min)
{

  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  assert(Nd == 3); // this will work only in 3D

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {
  // find closest r,mu,phi
      std::array<std::size_t, 3> idx = this->mData.GetIdxInverseCactus(i);
      const double x = this->mOrigin[0] + ((double)idx[0])*this->mDelta[0];
      const double y = this->mOrigin[1] + ((double)idx[1])*this->mDelta[1];
      const double z = this->mOrigin[2] + ((double)idx[2])*this->mDelta[2];
      const double rad = sqrt(x*x+y*y+z*z);

      if( (entropy(i) >= entropy_min) && (heating(i) > heat_min) ) {
	this->mData[i] = 1.0;
      } else {
	this->mData[i] = 0.0;
      }
      if(rad <= rmin) {
	this->mData[i] = rmin_value;
      }

  }

}

template <int Nd>
void DoubleMask<Nd>::set_shock_and_bound_mask(const CactusDataset<Nd> &entropy,
					      const CactusDataset<Nd> &gain_energy,
					      const double rmin,
					      const double rmin_value,
					      const double entropy_min)
{

  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  assert(Nd == 3); // this will work only in 3D

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {
  // find closest r,mu,phi
      std::array<std::size_t, 3> idx = this->mData.GetIdxInverseCactus(i);
      const double x = this->mOrigin[0] + ((double)idx[0])*this->mDelta[0];
      const double y = this->mOrigin[1] + ((double)idx[1])*this->mDelta[1];
      const double z = this->mOrigin[2] + ((double)idx[2])*this->mDelta[2];
      const double rad = sqrt(x*x+y*y+z*z);

      if( (entropy(i) >= entropy_min) && (gain_energy(i) <= 0.0e0) ) {
	this->mData[i] = 1.0;
      } else {
	this->mData[i] = 0.0;
      }

      if(rad <= rmin) {
	this->mData[i] = rmin_value;
      }

  }

}

#endif // GainRegionAnalysis_HPP_
