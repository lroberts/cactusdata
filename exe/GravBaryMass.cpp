#include <iostream> 
#include <fstream> 
#include <ostream> 
#include <math.h> 
#include <vector>
#include <memory>
#include <stdio.h> 
#include <set>
#include <algorithm>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"
#include "boost/program_options.hpp"
#include "boost/math/special_functions/spherical_harmonic.hpp"
#include "boost/format.hpp" 

#include "NDArray.hpp"
#include "CactusDataset.hpp" 
#include "CactusDatabase.hpp" 
#include "RealSphericalHarmonic.hpp" 
#include "DiagnosticEnergy.hpp"
#include "GravBaryMass.hpp"
#include <H5Cpp.h> 


namespace po = boost::program_options;
namespace bfs = boost::filesystem;

int main(int argc, char** argv) {
  
  // Set up the cl interface 
  po::options_description desc("Allowed options"); 
  desc.add_options()
      ("help", "produce help message")
      ("basepath,P", po::value<std::string>()->default_value("\./"), "String describing the base path")
      ("directory,D", po::value<std::string>()->default_value(""), "Regular expression describing the directories")
      ("basename-rho,N", po::value<std::string>()->default_value("guenni_rho.*\.h5"), "Regular expression describing the base name for rho")
      ("basename-ye,N", po::value<std::string>()->default_value("guenni_ye.*\.h5"), "Regular expression describing the base name for Ye")
      ("basename-temp,N", po::value<std::string>()->default_value("guenni_temperature.*\.h5"), "Regular expression describing the base name for temperature")
      ("basename-vel,N", po::value<std::string>()->default_value("guenni_vel.*\.h5"), "Regular expression describing the base name for velocity")
      ("basename-lapse,N", po::value<std::string>()->default_value("guenni_lapse.*\.h5"), "Regular expression describing the base name for lapse")
      ("basename-metric,N", po::value<std::string>()->default_value("guenni_metric.*\.h5"), "Regular expression describing the base name for lapse")
      ("outfile,O", po::value<std::string>()->default_value("GravyBaryMass.out"), "Output filename")
      ("octant", "Assume the data posesses reflecting octant symmetry")
      ("reflevel,R", po::value<int>()->default_value(-1), "Refinement level");
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help") || vm.size()==0) {
    std::cout << std::endl << desc << std::endl;
    return 1;
  }
  
  int reflevel = vm["reflevel"].as<int>(); 

  // first read the EOS table, table is currently hardcoded for simplicity
  EOSDriver::ReadTable("/work/00386/ux455321/stampede2/tables/SFHo.h5");
     
  //Build the database
  std::vector<bool> symmetry(3, false); 
  if (vm.count("octant")) symmetry = std::vector<bool>(3, true); 

  CactusDatabase<3> dbase_rho(vm["basepath"].as<std::string>(), 
      vm["basename-rho"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_ye(vm["basepath"].as<std::string>(), 
      vm["basename-ye"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_temp(vm["basepath"].as<std::string>(), 
      vm["basename-temp"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_vel(vm["basepath"].as<std::string>(), 
      vm["basename-vel"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  CactusDatabase<3> dbase_lapse(vm["basepath"].as<std::string>(), 
      vm["basename-lapse"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);

  std::cerr << "Guenni!" << std::endl;

  CactusDatabase<3> dbase_metric(vm["basepath"].as<std::string>(), 
      vm["basename-metric"].as<std::string>(), vm["directory"].as<std::string>(),
      symmetry);
  

 
  std::cout << "Starting Grav & Bary Mass calculation ..." << std::endl;

  std::string vname_rho   = "HYDROBASE::rho";
  std::string vname_temp  = "HYDROBASE::temperature";
  std::string vname_ye    = "HYDROBASE::Y_e";
  std::string vname_velx  = "HYDROBASE::vel[0]";
  std::string vname_vely  = "HYDROBASE::vel[1]";
  std::string vname_velz  = "HYDROBASE::vel[2]";
  std::string vname_lapse = "ADMBASE::alp";
  std::string vname_gxx   = "ADMBASE::gxx";
  std::string vname_gyy   = "ADMBASE::gyy";
  std::string vname_gzz   = "ADMBASE::gzz";

  // prepare outfile
  std::ofstream ofile(vm["outfile"].as<std::string>(), std::ofstream::out);  
  ofile << "#[0] Time (Msun) " << std::endl; 
  ofile << "#[1] BaryMass (in 1.0e10) " << std::endl; 
  ofile << "#[2] BaryMass (in 1.0e11) " << std::endl; 
  ofile << "#[3] BaryMass (in 1.0e12) " << std::endl; 
  ofile << "#[4] GravMass (in 1.0e10) " << std::endl; 
  ofile << "#[5] GravMass (in 1.0e11) " << std::endl; 
  ofile << "#[6] GravMass (in 1.0e12) " << std::endl; 

  

  // Iterate over timesteps 
  for (auto tstep : dbase_rho.getTsteps()) {
    std::cout << "*** Working on iteration " << (int) tstep << std::endl;
    // std::cout << "Reading datasets!" << std::endl;

    auto rho   = dbase_rho.getDatasets(vname_rho, tstep, reflevel); 
    std::cerr << "Done rho!" << std::endl;
    auto temp  = dbase_temp.getDatasets(vname_temp, tstep, reflevel); 
    std::cerr << "Done temp!" << std::endl;
    auto ye    = dbase_ye.getDatasets(vname_ye, tstep, reflevel); 
    std::cerr << "Done ye!" << std::endl;
    auto velx  = dbase_vel.getDatasets(vname_velx, tstep, reflevel); 
    std::cerr << "Done velx!" << std::endl;
    auto vely  = dbase_vel.getDatasets(vname_vely, tstep, reflevel); 
    std::cerr << "Done vely!" << std::endl;
    auto velz  = dbase_vel.getDatasets(vname_velz, tstep, reflevel); 
    std::cerr << "Done velz!" << std::endl;
    auto lapse = dbase_lapse.getDatasets(vname_lapse, tstep, reflevel); 
    std::cerr << "Done lapse!" << std::endl;
    auto gxx   = dbase_metric.getDatasets(vname_gxx, tstep, reflevel); 
    std::cerr << "Done gxx!" << std::endl;
    auto gyy   = dbase_metric.getDatasets(vname_gyy, tstep, reflevel); 
    std::cerr << "Done gyy!" << std::endl;
    auto gzz   = dbase_metric.getDatasets(vname_gzz, tstep, reflevel); 
    std::cerr << "Done gzz!" << std::endl;
    //    std::cout << "Done reading datasets!" << std::endl;


    LorentzFactor<3> wlorentz(rho[0],true);

    wlorentz.compute_w_lorentz(gxx[0],gyy[0],gzz[0],velx[0],vely[0],velz[0]);

    GravBaryMass<3> barymass_in_1e10(rho[0],true);
    barymass_in_1e10.compute_bary_mass(gxx[0],gyy[0],gzz[0],wlorentz,
				       rho[0],1.0e10);

    GravBaryMass<3> barymass_in_1e11(rho[0],true);
    barymass_in_1e11.compute_bary_mass(gxx[0],gyy[0],gzz[0],wlorentz,
				       rho[0],1.0e11);

    GravBaryMass<3> barymass_in_1e12(rho[0],true);
    barymass_in_1e12.compute_bary_mass(gxx[0],gyy[0],gzz[0],wlorentz,
				       rho[0],1.0e12);


    double bm10 = barymass_in_1e10.maskedSumPos(0.0, 100000.0); 
    double bm11 = barymass_in_1e11.maskedSumPos(0.0, 100000.0); 
    double bm12 = barymass_in_1e12.maskedSumPos(0.0, 100000.0); 

    GravBaryMass<3> gravmass_in_1e10(rho[0],true);
    gravmass_in_1e10.compute_grav_mass(rho[0],temp[0],ye[0],
				       wlorentz,gxx[0],gyy[0],gzz[0],
				       1.0e10);

    GravBaryMass<3> gravmass_in_1e11(rho[0],true);
    gravmass_in_1e11.compute_grav_mass(rho[0],temp[0],ye[0],
				       wlorentz,gxx[0],gyy[0],gzz[0],
				       1.0e11);

    GravBaryMass<3> gravmass_in_1e12(rho[0],true);
    gravmass_in_1e12.compute_grav_mass(rho[0],temp[0],ye[0],
				       wlorentz,gxx[0],gyy[0],gzz[0],
				       1.0e12);

    double gm10 = gravmass_in_1e10.maskedSumPos(0.0, 100000.0); 
    double gm11 = gravmass_in_1e11.maskedSumPos(0.0, 100000.0); 
    double gm12 = gravmass_in_1e12.maskedSumPos(0.0, 100000.0); 

    // std::cerr << "done computing sums" << std::endl;    

    // Note on units:
    // The sums do not multiply by the Cartesian coordinate volume element.
    // One needs to take the result and multiply it with the
    // Cartesian coordinate volume element for the reflevel on which
    // the analysis was carried out.
    // Then multiple by solar mass in cgs times c**2 in cgs. This should
    // give energy.

    // Example: dx = 2, diag_all = 5.15668e-5
    // E_diag(cgs) = 2.0**3 * 5.15668e-5 * 1.99e33 * 9e20

    std::cout << barymass_in_1e10.getTime() << " " << bm11 << " " << gm11 << std::endl; 
    ofile << boost::format("% 12.5e ") % barymass_in_1e10.getTime(); 
    ofile << boost::format("% 12.5e ") % bm10;
    ofile << boost::format("% 12.5e ") % bm11; 
    ofile << boost::format("% 12.5e ") % bm12; 
    ofile << boost::format("% 12.5e ") % gm10;
    ofile << boost::format("% 12.5e ") % gm11; 
    ofile << boost::format("% 12.5e ") % gm12; 
    ofile << std::endl; 

  }  
  ofile.close();

  return 0;
}  
