#ifndef GravBaryMass_HPP_
#define GravBaryMass_HPP_

#include <iostream>
#include <memory>
#include <vector>
#include <array>
#include <utility>
#include <H5Cpp.h>
#include <exception>
#include <NDArray.hpp>
#include <CactusDataset.hpp>
#include "EOSDriver/EOSDriver.hpp"

#define INV_RHO_GF 6.18735016707159e17
#define PRESS_GF 1.80123683248503E-39
#define EPS_GF 1.11265005605362e-21

template <int Nd>
class GravBaryMass: public CactusDataset<Nd> {
  
public:

  GravBaryMass(const CactusDataset<Nd>  & CDS, const bool make_zero) 
    : CactusDataset<Nd>(CDS,make_zero) {
    this->mName = "GravBaryMass";
  }

  void compute_bary_mass(const CactusDataset<Nd> &gxx,
			 const CactusDataset<Nd> &gyy,
			 const CactusDataset<Nd> &gzz,
			 const CactusDataset<Nd> &wlorentz,
			 const CactusDataset<Nd> &rho,
			 const double rho_cut_cgs);

  void compute_bary_mass_with_mask(const CactusDataset<Nd> &mask,
				   const CactusDataset<Nd> &gxx,
				   const CactusDataset<Nd> &gyy,
				   const CactusDataset<Nd> &gzz,
				   const CactusDataset<Nd> &wlorentz,
				   const CactusDataset<Nd> &rho,
				   const double rho_cut_cgs);

  void compute_grav_mass(const CactusDataset<Nd> &rho,
			 const CactusDataset<Nd> &temp,
			 const CactusDataset<Nd> &ye,
			 const CactusDataset<Nd> &wlorentz,
			 const CactusDataset<Nd> &gxx,
			 const CactusDataset<Nd> &gyy,
			 const CactusDataset<Nd> &gzz,
			 const double rho_cut_cgs); 

};

template <int Nd>
void GravBaryMass<Nd>::compute_bary_mass(const CactusDataset<Nd> &gxx,
					 const CactusDataset<Nd> &gyy,
					 const CactusDataset<Nd> &gzz,
					 const CactusDataset<Nd> &wlorentz,
					 const CactusDataset<Nd> &rho,
					 const double rho_cut_cgs) {
  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {

    const double r = rho(i);

    if(r*INV_RHO_GF >= rho_cut_cgs) {
      // approximate metric determinant
      const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
      const double w = wlorentz(i);
      this->mData[i] = sdetg*w*r;
    } else {
      this->mData[i] = 0.0;
    }
      
  }

}

template <int Nd>
void GravBaryMass<Nd>::compute_bary_mass_with_mask(const CactusDataset<Nd> &mask,
						   const CactusDataset<Nd> &gxx,
						   const CactusDataset<Nd> &gyy,
						   const CactusDataset<Nd> &gzz,
						   const CactusDataset<Nd> &wlorentz,
						   const CactusDataset<Nd> &rho,
						   const double rho_cut_cgs) {

  // get size
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {

    const double r = rho(i);

    if(r*INV_RHO_GF >= rho_cut_cgs) {
      // approximate metric determinant
      const double sdetg = sqrt(gxx(i)*gyy(i)*gzz(i));
      const double w = wlorentz(i);
      this->mData[i] = sdetg*w*r*mask(i);
    } else {
      this->mData[i] = 0.0;
    }
      
  }

}



template <int Nd>
void GravBaryMass<Nd>::compute_grav_mass(const CactusDataset<Nd> &rho,
					 const CactusDataset<Nd> &temp,
					 const CactusDataset<Nd> &ye,
					 const CactusDataset<Nd> &wlorentz,
					 const CactusDataset<Nd> &gxx,
					 const CactusDataset<Nd> &gyy,
					 const CactusDataset<Nd> &gzz,
					 const double rho_cut_cgs) {
  std::size_t npoints = 1;
  for(int i=0; i<Nd;i++) {
    npoints *= this->mData.dimSize(i);
  }

  const double rho_min = 1.0e4;
  const double ye_min = 0.035;
  const double temp_min = 0.05;

#pragma omp parallel for
  for (std::size_t i=0; i < npoints; ++i) {

    const double r = rho(i);

    if(r*INV_RHO_GF >= rho_cut_cgs) {

      const double w = wlorentz(i);
      const double detg = (gxx(i)*gyy(i)*gzz(i));
      double press = 0.0;
      double eps = 0.0;
      EOSDriver::GetPressureEpsFromTemperature(std::max(r*INV_RHO_GF,rho_min),
					       std::max(ye(i),ye_min),
					       std::max(temp(i),temp_min),
					       &press,&eps);
      press *= PRESS_GF;
      eps *= EPS_GF;

      const double h = 1.0 + eps + press/r;
      
      this->mData[i] = r*h*w*w - press;
      this->mData[i] *= pow(detg,5.0/12.0);
    } else {
      this->mData[i] = 0.0;
    }

  }

}



#endif // GravBaryMass_HPP_
